#!/usr/bin/env perl
# -*- coding: utf-8 -*-
# perl
use warnings;
use strict;
# Allow to use true and false as named constants
use constant false => 0;
use constant true  => 1;

# Allow to use functions with parameter signatures
use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);

use File::Path qw(make_path rmtree);
use File::Copy;
use File::Spec;
use Getopt::Long;

sub writeFile($file, $string) {
    open (FH, ">", $file);
    print FH $string;
    close FH;
}

sub command_exists($comm) {
    return `which $comm 2> /dev/null` ne "";
}

sub get_directory($path) {
    my ($vol, $dir, $file) = File::Spec->splitpath($path);
    return $dir;
}

my $quiet_run;
my $fake_run_command;
my $run_heavy_commands;
my $debug_commands;
GetOptions(
    "quiet" => \$quiet_run,
    "fake-command" => \$fake_run_command,
    "run-heavy" => \$run_heavy_commands,
    "debug" => \$debug_commands
);

$quiet_run          ||= false;
$fake_run_command   ||= false;
$run_heavy_commands ||= false;


# Pretty message printing
my $pretty_msg = command_exists("colecho");
sub log1($message)  {
    if ($pretty_msg) { system("colecho -- \"$message\"");
    } else { say "  - $message"; } }
sub err1($message)  {
    if ($pretty_msg) { system("colecho -e:2 -- \"$message\"");
    } else { say "!!! $message"; } }
sub info1($message) {
    if ($pretty_msg) { system("colecho -i:1 -- \"$message\"");
    } else { say "--> $message"; } }
sub warn1($message) {
    if ($pretty_msg) { system("colecho -w:1 -- \"$message\"");
    } else { say "=>> $message"; } }

my @tools = (
    ["colecho", "Colorful messages in scripts", "~/workspace/hax-nim/build-all.sh hax-nim"],
    ["kitty", "Terminal emulator", "pacman --noconfirm -S kitty"],
    ["awesome", "Window manager", "pacman --noconfirm -S awesome"],
    ["startx", "Xorg startup utility", "pacman --noconfirm xorg-xinit"],
    ["zathura", "pdf preview tool", "pacman -S --noconfirm zathura zathura-pdf-poppler"],
    ["emacs", "main text editor", "pacman -S --noconfirm emacs"],
    ["nvim", "CLI text editor", "pacman -S --noconfirm neovim"],
    ["qutebrowser", "Web browser", "pacman -S --noconfirm qutebrowser"],
    ["yay", "AUR helper", "#todo"],
    ["git", "git", "pacman -S git"],
    ["ranger", "terminal file manager", "pacman -S --noconfirm ranger"],
    ["rofi", "dmenu-like selector", "pacman -S --noconfirm rofi"],
    ["xbindkeys", "WM-independent key combinations", "pacman -S --noconfirm xbindkeys"],
    ["sxiv", "image preview software", "pacman -S --noconfirm sxiv"],
    ["nim", "nim compiler", "curl https://nim-lang.org/choosenim/init.sh -sSf | sh"],
    ["fzf", "fuzzy selector", "pacman -S --noconfirm fzf"],
    ["exa", "colorful ls alternative", "pacman -S --noconfirm exa"],
    ["tcc", "C compiler", "pacman -S --noconfirm tcc"],
    ["mega-sync", "MEGA sync client", "yay megacmd-bin"],
    ["zip", "archiver", "ddd"],
    ["unzip", "archive opener", "ddd"],
    ["iw", "wireless network configuration tool", "pacman -S --noconfirm iw"],
    ["highlight", "ranger syntax highlighting", "pacman -S --noconfirm highlight"],
);

for my $tool (@tools) {
    if (command_exists($tool->[0])) {
        info1 "Found $tool->[0]" unless $quiet_run;
    } else {
        err1 "Missing $tool->[0]: $tool->[1].\n To install it $tool->[2]";
    }
}

mkdir glob "~/workspace";

sub check_files(%conf) {
    my $test_file = $conf{'test-file'};
    my $always_update = $conf{'always-update'};
    my $update_command = $conf{'command'};
    my $is_heavy = $conf{'is-heavy'};

    my $do_command;

    if ($debug_commands) {
        log1("$test_file -->");
        log1("$update_command");
        say "";
    }

    if (-f $test_file or -d $test_file or -l $test_file) {
        info1("Found $test_file") unless $quiet_run;
        if ($always_update) {
            log1 "$update_command" unless $quiet_run;
            $do_command = true;
        }
    } else {
        err1 "Could not find $test_file";
        $do_command =
            defined $is_heavy ? ($is_heavy and $run_heavy_commands) : true;
    }


    if ($do_command) {
        system "$update_command" unless $fake_run_command;
    }
}

my @config = (
    {
        'test-file' => glob ("~/.emacs.d/spacemacs.mk"),
        'command' => "git clone -b develop https://github.com/syl20bnr/spacemacs ~/.emacs.d",
        'is-heavy' => true
    },
    {
        'test-file' => glob ("~/workspace/hax-nim"),
        'command' => "git clone -b develop https://gitlab.com/haxscramper/hax-nim ~/workspace/hax-nim"
    },
    {
        'test-file' => glob ("~/.config/hax-config"),
        'command' => "git clone -b desktop https://gitlab.com/haxscramper/settings-public ~/.config/hax-config"
    },
    {
        'test-file' => glob ("~/.local/share/nvim/site/autoload/plug.vim"),
        'command' => "curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
    },
    {
        'test-file' => glob ("~/.config/awesome/themes/hax"),
        'command' => "mkdir -p ~/.config/awesome/themes && cp -r ~/.config/hax-config/desktop/awesome_wm_theme ~/.config/awesome/themes/hax",
        'always-update' => true
    },
    {
        'test-file' => glob ("~/.config/hax-local/"),
        'command' => "mkdir -p ~/.config/hax-local"
    },
    {
        'test-file' => glob("~/.config/hax-local/log"),
        'command' => "mkdir -p ~/.config/hax-local/log"
    },
    {
        'test-file' => glob("~/.config/awesome/awesome-wm-widgets"),
        'command' => "git clone https://github.com/streetturtle/awesome-wm-widgets ~/.config/awesome/awesome-wm-widgets"
    },
    {
        'test-file' => glob("~/.config/awesome/scratchdrop"),
        'command' => "mkdir -p ~/.config/awesome && cp -r ~/.config/hax-config/desktop/awesome_scratchdrop ~/.config/awesome/scratchdrop"
    },
    {
        'test-file' => glob("~/.nimble"),
        'command' => "wget https://nim-lang.org/choosenim/init.sh && chmod +x init.sh && ./init.sh -y && rm init.sh",
        'is-heavy' => true
    },
    {
        'test-file' => glob("~/workspace/hax-nim/bin/colecho"),
        'command' => "(nim --version || (echo 'Missing nim compiler' && false)) && (nimble -y install moustachu) && (nimble -y install parsetoml) && cd ~/workspace/hax-nim && ./build-all.sh"
    },
    {
        'test-file' => glob("~/workspace/hax-nim/bin/fsm-build"),
        'command' => "(nim --version || (echo 'Missing nim compiler' && false)) && (nimble -y install moustachu) && (nimble -y install parsetoml) && cd ~/workspace/hax-nim && ./build-all.sh"
    },
    {
        'test-file' => glob("~/.config/awesome/lain"),
        'command' => "git clone https://github.com/lcpz/lain ~/.config/awesome/lain"
    },
    {
        'test-file' => glob("~/.config/awesome/freedesktop"),
        'command' => "git clone https://github.com/lcpz/awesome-freedesktop ~/.config/awesome/freedesktop"
    }
);



check_files(%$_) for (@config);

my $conf_dir = glob "~/.config/hax-config";
my $home = glob "~";

# TODO if 'to' is symbolic link but it points to wrong direction,
# remove it and replace with correct one.
sub symlink_settings($from, $to, $message) {
    $from = "$conf_dir/$from";
    $to = "$home/$to";
    my $target = readlink($to);
    unless (defined $target) {
        unless (-d get_directory ($to)) {
            info1("Target directory for $to is missing, create new one")
                unless $quiet_run;
            make_path (get_directory($to));
        }

        my $res = symlink ($from, $to) == 1;
        info1("Link $message from [ $from ] to [ $to ]: " .
                  ($res ? "success" : "fail"));
        unless ($res) {
            if (-f $from or -d $from) {
                info1("Source file exists");
            } else {
                warn1("'from' ($from) is missing");
            }

           if ((-f $to or -d $to) and (not -l $to)) {
               warn1("'to' ($to) already exists and not a symlink");
           }
        }
    } else {
        info1 ("$message is OK") unless $quiet_run;
    }
}

system("grep -q load_hax_bash_config ~/.bashrc || echo 'source ~/.config/hax-config/bash_config.sh' >> ~/.bashrc");

$ENV{'PATH'} = `bash -c 'source ~/.config/hax-config/bash_config.sh && echo \$PATH'`;

my @settings_links = (
    ["config.fish",
     ".config/fish/config.fish",
     'fish shell'],
    ["cli/fish_user_key_bindings.fish",
     ".config/fish/functions/fish_user_key_bindings.fish",
     'fish key bidings'],
    ["cli/neovim_init.vim",
     ".config/nvim/init.vim",
     "neovim"],
    ["cli/ranger.conf",
     ".config/ranger/rc.conf",
     "ranger"],
    ["browser/qutebrowser_config.py",
     ".config/qutebrowser/config.py",
     "qutebrowser"],
    ["cli/kitty.conf",
     ".config/kitty/kitty.conf",
     "kitty terminal"],
    ["desktop/rofi",
     ".config/rofi/config",
     "rofi launcher"],
    ["other/sxiv_key-handler.sh",
     ".config/sxiv/exec/key-handler",
     "sxiv key handler"],
    ["emacs/dotspacemacs.el",
     ".spacemacs",
     "spacemacs"],
    ["desktop/.xbindkeysrc",
     ".xbindkeysrc",
     "xbindkeys"],
    ["desktop/xinitrc.sh",
     ".xinitrc",
     "xinitrc"],
    ["git/commit_template.adoc",
     ".gitmessage",
     "git commit template"],
    ["desktop/awesome_wm.lua",
     ".config/awesome/rc.lua",
     "awesome wm config"],
    ["desktop/zathurarc.conf",
     ".config/zathura/zathurarc",
     "Zathura config"]
);

symlink_settings ($_->[0], $_->[1], $_->[2]) for (@settings_links);

my $default_ignore = <<'EOF';
**/*.bin
**/*.tmp
**/*.out
**/*.so
**/*.pdf
**/*.html
EOF

writeFile(glob ("~/.gitignore_global"), $default_ignore);
system "git config --global core.excludesfile ~/.gitignore_global";
system "bash ~/.config/hax-config/scripts/ensure_dir_structure.sh" .
    ($quiet_run ? " > /dev/null" : "");

info1("Done");
