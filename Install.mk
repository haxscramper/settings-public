## Run this makefile to correctly check all dependencies for the
## configuration, provide list of necessary installations and git
## clones. *No actions will be carried automatically, only list of
## missing/necessary things will be generated.

SHELL=/bin/bash

#######################  nim language installation  #######################

.ONESHELL:
check-nim-installation:
	@true
	## Check whether or not nim language compiler is installed.

###########################  required software  ###########################

.ONESHELL:
check-required-software:
	@true
	## Check if all necesary software is installed

.ONESHELL:
check-desktop-software:
	@true
	## Check if software for desktop is installed (window manager,
	## keybind manager, rofi etc).

.ONESHELL:
check-utility-gui-software:
	@true
	## Check if utility gui sofware is installed (pdf viewer with
	## necessary plugins, image viewer).

.ONESHELL:
check-cli-scripting-tools:
	@true
	## Check

#####################  required cloned repositories  ######################

.ONESHELL:
check-required-clones:
	@true:
	## Check if all required repositories has been cloned
