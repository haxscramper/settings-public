#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

t="test-requirements.pl"

# ./$t --quiet
# ./$t --quiet --fake-command
./$t --quiet --debug --run-heavy
