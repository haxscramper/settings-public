#!/usr/bin/env bash

port="12070"
file="$HOME/.config/hax-config/browser/surfingkeys-settings.js"
msg="$HOME/.nimble/bin/colecho -L -p:surfingkeys" # TODO use message printer


function init_runner () {
    script="$0"
    $msg "Starting runner script"
    echo "$script" | entr -r "$script" start
}

function main () {
    $msg "Starting surfingkeys proivider at port $port"
    while true; do
        { echo -ne "HTTP/1.0 200 OK\r\nContent-Length: $(wc -c < "$file")\r\n\r\n"; \
          cat "$file"; \
          } | nc -l -p "$port" > /dev/null
        $msg "Served file from '$file'"
    done
}

case ${1:-""} in
    "start")
        main
        ;;
    *)
        init_runner
esac
