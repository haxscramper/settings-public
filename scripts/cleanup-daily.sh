#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

xclip -out | perl -pe 's/^(\d{2})(\d{2})$/*** \@time:$1:$2;/; s/^(\d{4}-\d{2}-\d{2})$/** \@date:$1;\n/' | fmt | xclip -sel cli
