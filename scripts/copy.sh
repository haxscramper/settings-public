#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit

if [[ "$1" = "-h" ]]; then
    cat << EOF | groff -man -Tascii
.TH copy.sh 1 '2019-04' '1.0'
.SH NAME
copy.sh
.SH SYNOPSIS
Copy first argument to the second and display fancy message
.SH DESCRIPTION
Identical to
.SB "cp \$1 \$2"
but with nice formatted message
.SS Options
.TP
-h
display this message and exit
EOF

    exit 1
fi


if [ "$#" -ne 2 ]; then
    colecho -e2 "copy.sh requires exactly two arguments: source and destination."
fi

what="$1"
where="$2"

echo -e "  |   $what\n  +-\033[32m>\033[0m $where"

cp -- "$what" "$where"
