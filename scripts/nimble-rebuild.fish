#!/usr/bin/env fish


function find-match-in-parent-dir -a "find_match"
    set -l path "$PWD"
    while [ '/' != "$path" ]
        set result (find "$path" -maxdepth 1 -mindepth 1 -name "$find_match")
        if [ '' != "$nimble_conf" ]
            echo "$result"
            break
        else
            echo "eee"
            set path (readlink -f "$path/..")
        end
        # break
    end
end

set -l nimble_conf (find-match-in-parent-dir "*.nimble")
set -l original_dir $PWD

if [ '' != "$nimble_conf" ]
    echo "Found nimble file: $nimble_conf"
    cd (dirname $nimble_conf)
else
    echo "No nimble file found, exiting"
    exit 1
end

set -l suits (/home/test/.config/hax-config/scripts/find_nimble_tests.nim)

if [ 'true' != "$IN_ESHELL" ]
    echo "fzf"
    set -g selected_suit (printf '%s\n' $suits | cat -n | fzf -i)
    set -l nimble_name (basename $nimble_conf)
    kitty @ set-window-title "nimble-rebuild $nimble_name"
else
    echo "ivy"
    set -g selected_suit (printf '%s\n' $suits | cat -n | emacsclient_ivy)
end

if [ '' != "$selected_suit" ]
    # echo "Press `q` to cancel"
    # echo "Select suit number"
    set -l selected (echo "$selected_suit" | awk '{print $1}')
    # echo "selected $selected"
    if string match -qr '^[0-9]+$' $selected;
        set -l suit $suits[$selected]
        set -l cmd "time nim c -r $suit && echo -l:1 'Done tests \"$suit\"'"
        # echo $cmd
        fd -e nim -e nims -e sh -e nimble -e cfg | entr -rc sh -c "$cmd"
        # cd $original_dir # FUCK SHELL
    else if string match 'q' $selected
        echo -w:1 "Cancelled `nd`"
    else
        echo -e:1 "Expected integer but found $selected"
    end
end
