#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

echo "[$(date -Is)][log] reloading xbindkeys" >> ~/.config/hax-local/log/$(date -I)
killall -9 xbindkeys
xbindkeys
echo "[$(date -Is)][log] xbindkeys reloaded" >> ~/.config/hax-local/log/$(date -I)


