#!/usr/bin/env bash
set -x

IFS=+ read -r resolution x_pos y_pos <<< $(slop)
out=$(mktemp "/tmp/recording.XXXXXXXXXXXX")
# qpipe="/tmp/ffmpeg_screencast_q_pipe"
# rm $qpipe
# mkfifo $qpipe

ext="mp4"

notify-send -t 2000 "Started recording ${x_pos}x${y_pos}, $resolution"
ffmpeg -y \
       -video_size $resolution \
       -framerate 25 \
       -f x11grab \
       -i :0.0+$x_pos,$y_pos \
       "$out.$ext" &

pid=$!

nc -l 12000 -q0 > /dev/null
notify-send -t 2000 "Finished recording to $out.$ext"
echo "q\n" > /proc/$pid/fd/0
kill -2 $pid
notify-send -t 2000 "Saved recording to file '$out.$ext'"
echo -n "$out.mp4" | xclip -sel cli
mpv "$out.$ext"
# sleep 1

# ffmpeg -i "$out.mkv" "$out.mp4"
# mpv "$out.mp4"
