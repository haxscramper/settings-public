#!/usr/bin/env bash

# Change order of pdf pages
# Swap even and odd pages in pdf

#https://unix.stackexchange.com/questions/496716/rearrange-pdf-pages-swap-odd-pages-and-even-pages

pdfseparate "$1" piece-%04d.pdf
reordered=()
set -- piece*.pdf
while (($#)); do
  if [ $# -ge 2 ]; then
    reordered+=("$2" "$1")
    shift 2
  else
    reordered+=("$1")
    shift
  fi
done
pdfunite "${reordered[@]}" "${2:-swapped.pdf}"
rm -f piece-*.pdf
unset reordered
