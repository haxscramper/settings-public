import hmisc/macros/matching
import hmisc/other/[hshell, oswrap]
import strutils, strformat

let
  target = parseFile(paramStrs()[0])
  dir = cwd()

target.assertExists()

let (path, name, ext) = target.splitFile()

case ext:
  of "zip":
    echo "unzipping"
  of "tar.gz":
    echo "untarring"
  of "gz":
    echo "ungz"
