import std/[strutils, os, sequtils, strformat, shell]
import hmisc/helpers

var noprint: set[DebugOutputKind]

let inlines = toSeq(stdin.lines())
let
  list = inlines.joinq(wrap = "\\\"").wrap(("'(", ")"))
  command = fmt("-e \"(ivy-read \\\"Select\\\" {list})\"")

let (res, err, code) = shellVerboseErr noprint:
  emacsclient ($command)

echo res.strip(chars = {'"'})
