#!/usr/bin/env bash

# DOCS:
# Download single folder from github repository.
# First argument is user name, second one is repository name.
# Third argument specifies concrete directory and can be omitted 
# (in this case whole repository will be downloaded)

# NOTE: downloaded is performed using wget as regular files. In other
# words no git metadata is preserved: you only get content of thedirectory

# EXAMPLES:
# > github-download-folder.sh cirosantilli test-many-commits-1m
# download whole repository from github
# > github-download-folder.sh openframeworks openFrameworks examples/gl/billboardExample
# donwload specific folder

# DEPS: jq, curl, wget

# repo_user="openframeworks"
# repo_name="openFrameworks"
# repo_dir="examples/gl/billboardExample"

if [[ -z "$1" ]]; then
    echo "missing repo user (first argument)"
    exit 1
fi

if [[ -z "$2" ]]; then
    echo "missing repo name (second argument)"
    exit 1
fi

if [[ -z "$3" ]]; then 
    echo "downloading whole repository"
fi

repo_user=$1
repo_name=$2
repo_dir=$3

repo_dir=$(echo $repo_dir | sed 's!/$!!')

echo "downloading from $repo_user/$repo_name/$repo_dir"

function api_get () {
    curl --silent "https://api.github.com/$1"
}

tree_sha=$(api_get repos/$repo_user/$repo_name/branches/master |
    jq -r '.commit.commit.tree.sha')

api_get repos/$repo_user/$repo_name/git/trees/$tree_sha?recursive=1 |
    jq -r '.tree | .[] | select(.type == "blob") | .path' |
    grep "$repo_dir" |
    sed "s!$repo_dir/!!" |
    while read file
    do
        url=https://raw.githubusercontent.com/$repo_user/$repo_name/master/$repo_dir/$file
        echo "$url -> $file"
        mkdir -p $(dirname $file)
        wget --force-directories -O $file $url
    done
