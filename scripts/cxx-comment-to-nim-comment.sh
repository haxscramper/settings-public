#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit

intext=$(xclip -out)
notify-send --urgency=low -- "$intext"
outtext=$(echo "$intext" | sed -E 's/^\s\*?\s?(.*)/  ## \1/')
notify-send --urgency=low -- "$outtext"

echo "$outtext" | xclip -sel cli
