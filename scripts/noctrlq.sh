#!/usr/bin/env bash

# Just want to say hello to shitheads @ mozilla who decided that putting Ctrl+Q
# (aka fuck-up-everything shortcut) next to Ctrl+W (prolly second most used one)
# was such a good idea.

W="$(xdotool getactivewindow)"
S1="$(xprop -id "$W" | awk -F '"' '/WM_CLASS/{print $4}')"
if [ "$S1" != "Firefox" ] &&
       [ "$S1" != "Firefox Developer Edition" ] &&
       [ "$S1" != "firefox" ]; then
    xdotool key --clearmodifiers --window "$W" "ctrl+q"
else
    notify-send "Fuck Ctrl-q"
fi
