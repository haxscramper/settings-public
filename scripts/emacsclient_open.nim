#!/usr/bin/env -S nim r
import hmisc/preludes/cli_app

let args = paramStr(0).dropPrefix("file://").split(":")

let msg = &"(progn (find-file \"{args[0]}\") (goto-line {args[1]}))"

execShell shellCmd("emacsclient").withIt do:
  it.opt "e", " ", msg
