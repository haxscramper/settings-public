#!/usr/bin/env bash

## DOCS: List extensions by file size

find -type f -printf '%k %p\n' |
    sed -r 's/^([0-9]+) .*?\.(\w+)$/\1 \2/' |
    sort -k2 |
    awk '{a[$2] += $1;} END { for (i in a) print a[i], i;}' |
    sort -n
