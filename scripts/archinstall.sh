#!/usr/bin/env sh

install_disk() {
  boot_size=512M
  swap_size=2G

  # umount /dev/sda1
  # umount /dev/sda2
  # umount /dev/sda3
#   # to create the partitions programatically (rather than manually)
#   # https://superuser.com/a/984637
#   sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk /dev/sda
#   o # clear the in memory partition table
#   n # new partition
#   p # primary partition
#   1 # partition number 1
#     # default - start at beginning of disk
#   +$boot_size # boot parttion
#   n # new partition
#   p # primary partition
#   2 # partion number 2
#     # default, start immediately after preceding partition
#   +$swap_size # swap parttion
#   n # new partition
#   p # primary partition
#   3 # partion number 3
#     # default, start immediately after preceding partition
#     # default, extend partition to end of disk
#   a # make a partition bootable
#   1 # bootable partition is partition 1 -- /dev/sda1
#   p # print the in-memory partition table
#   w # write the partition table
#   q # and we're done
# EOF

  # Format the partitions
  mkfs.ext4 -F /dev/sda3 # -F to force overwrite (for repeated install tries)
  mkfs.fat -F32 /dev/sda1

  # Set up time
  timedatectl set-ntp true

  # echo "keyserver hkp://keyserver.ubuntu.com" >> /etc/pacman/gnupg/gpg.conf
  # Initate pacman keyring
  # pacman-key --init
  # pacman-key --populate archlinux
  # pacman-key --refresh-keys

  # Mount the partitions
  mount /dev/sda3 /mnt
  mkdir -pv /mnt/boot/efi
  mount /dev/sda1 /mnt/boot/efi
  mkswap /dev/sda2
  swapon /dev/sda2

  # Install Arch Linux
  # pacstrap /mnt base base-devel grub os-prober intel-ucode efibootmgr dosfstools xorg xorg-server xorg-xinit mesa xf86-video-intel fish bash mkinitcpio


  # Generate fstab
  mkdir -p /mnt/etc
  genfstab -U /mnt >> /mnt/etc/fstab

  cp "$0" /mnt
  echo "Disk setup done. "
  echo "Installing packages"
  pacstrap /mnt base linux grub efibootmgr iwd fish bash sudo neovim
  echo "Copy installer script to /mnt"
  cp "$0" /mnt
  echo "To continue installation 'arch-chroot /mnt'"
}

install_user() {
    # Set date time
    ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
    hwclock --systohc

    # Set locale to en_US.UTF-8 UTF-8
    sed -i '/en_US.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
    sed -i '/ru_RU.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
    sed -i '/ru_RU ISO-8859-5/s/^#//g' /etc/locale.gen
    sed -i '/ru_RU.CP1251 CP1251/s/^#//g' /etc/locale.gen

    locale-gen
    echo "LANG=en_US.UTF-8" >> /etc/locale.conf


    # Set hostname
    name="haxscramper-laptop"
    echo $name >> /etc/hostname
    echo "127.0.1.1 $name.localdomain  $name" >> /etc/hosts

    # Generate initramfs
    # mkinitcpio -P

    # Set root password
    echo -e "Enter \e[31mroot password\e[39m"
    passwd

    # Install bootloader
    cat << EOF > /etc/default/grub
GRUB_CMDLINE_LINUX_DEFAULT=""
GRUB_SAVEDEFAULT=true
GRUB_DEFAULT=saved
EOF

    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=arch
    grub-mkconfig -o /boot/grub/grub.cfg

    # Create new user
    useradd -m -G wheel,power,input,storage,uucp,network \
            -s /usr/bin/fish haxscramper
    sed --in-place 's/^#\s*\(%wheel\s\+ALL=(ALL)\s\+NOPASSWD:\s\+ALL\)/\1/' /etc/sudoers

    echo -e "Enter \e[33muser password\e[39m"
    passwd haxscramper

    echo "Setting up network configuration"
    cat << EOF > /etc/systemd/network/20-wired.network
[Match]
Name=enp*

[Network]
DHCP=yes

[DHCP]
RouteMetric=10
EOF


    cat << EOF > /etc/systemd/network/25-wireless.network
[Match]
Name=wl*

[Network]
DHCP=yes

[DHCP]
RouteMetric=20
EOF

    systemctl enable systemd-resolved.service
    systemctl enable systemd-networkd.service


}

if [[ "$1" == disk ]]; then
    install_disk
elif [[ "$1" == user ]]; then
    install_user
else
    echo "Expected '\e[32mdisk\e[39m' or '\e[32muser\e[39m', but found '\e[31m$1\e[39m'"
fi
