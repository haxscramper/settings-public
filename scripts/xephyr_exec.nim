import hmisc/macros/matching
import hmisc/other/[hshell, oswrap]
import strutils, strformat

let ok = ([@command, opt @display or ":1"] ?= paramStrs())

echo paramStrs(), " ", ok

if not ok:
  echo "Expected 'shell command' [x display]"
  quit 1

setEnv("DISPLAY", ":0")

let xephyr = startShell(
  &"Xephyr -resizeable -retro -sw-cursor -softCursor -ac {display}")

withEnv({"DISPLAY" : display}):
  let box = startShell("openbox")
  execShell(command)
