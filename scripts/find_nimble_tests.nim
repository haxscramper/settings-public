#!/usr/bin/env -S nim r

import std/[os, re, strutils, strformat]

setCurrentDir("/mnt/workspace/github/hmisc")

var result: seq[string]

for file in walkDirRec("tests"):
  var suite: string
  let (_, name, ext) = splitFile(file)
  if name.startsWith("t") and file.endsWith(".nim"):
    result.add &"'{file}'"
    for line in lines(file):
      if line =~ re"suite ""(.*?)"":":
        suite = matches[0]
        result.add &"'{file}' '{suite}'::" 

      elif line =~ re"  test ""(.*?)"":":
        result.add &"'{file}' '{suite}'::'{matches[0]}'" 

for item in result:
  echo item
