#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit

intext=$(xclip -out)
notify-send --urgency=low -- "$intext"
outtext=$(
    echo "$intext" |
    sed -E 's/^[a-zA-Z0-9_]+\((\w|\-)+\)# ?//; /^$/d') # |
    # sed -E 's/([. ])([123])0([. ]|$)/\1\22\3/'
      # )

notify-send --urgency=low -- "$outtext"

echo "$outtext" | xclip -sel cli
