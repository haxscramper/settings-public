#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
# set -o errexit

sendOk=false
pids=$(xdotool search --class "kitty")
for pid in $pids; do
    name=$(xdotool getwindowname $pid)
    if [[ $name == "kitty-target"* ]]; then
        notify-send "Sending repeat to window '$name' (id: $pid)"
        xdotool sleep 0.3 key --clearmodifiers --window $pid Up
        xdotool sleep 0.3 key --clearmodifiers --window $pid KP_Enter
        notify-send "Done"
        sendOk=true
    fi
done

if [[ $sendOk != "true" ]]; then
    notify-send "!!! Use `kt` to set window name"
fi
