#!/usr/bin/env bash

copyq eval 'print(read(0))' | sed -r 's/^.*?\[.*? ([0-9]{2}:[0-9]{2})]/** @time:\1;\n/' | fmt
