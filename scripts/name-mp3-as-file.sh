#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit

file="$1"
noext="${file%%.*}"

echo "Changing file: $1"

eyeD3 -a "$noext" -A "$noext" -b "$noext" -t "$noext" \
    -c "none" \
    "$file"
