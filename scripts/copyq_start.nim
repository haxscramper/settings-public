import hmisc/other/[hshell, oswrap]
import fusion/matching
import strutils, times, sequtils, strformat

func awkSplit(str: string, sep: string = " "): seq[string] =
  str.split(sep).filterIt(it.len > 0)

func firstLine(str: string): string = str.split("\n")[0]

let copyq = startShell shellCmd(copyq, toggle)
sleep(100)

[@copyqWid, .._] := shPipe(
  shellCmd(wmctrl, -lp),
  shellCmd(grep, CopyQ)).
  evalShell().stdout.split("\n")[0].awkSplit()

[[_, @mouseX], [_, @mouseY], .._] := shellCmd(
  xdotool, getmouselocation).
  evalShell().stdout.firstLine().awkSplit().
  mapIt(it.split(":"))

let
  mX = mouseX.parseInt() - 100
  mY = mouseY.parseInt() - 100

# let cmd = &"xdotool windowmove {copyqWid} {mX} {mY}"
execShell shellCmd(xdotool, windowmove, $copyqWid, $mX, $mY)
execShell shellCmd(xdotool, windowsize, $copyqWid, 600, 800)
