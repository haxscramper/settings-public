#!/usr/bin/env bash

## DOCS: parse dynalist OPML format into org-mode
## DEPS: xidel,sed,xclip,fmt

xclip -out | tee \
    >(
        {
            echo -ne "\n* Logs\n\n"
            xidel -s - -e '/opml/body/outline/outline[2]/outline/concat(@text, codepoints-to-string(10))' |
                sed -r 's/^([0-9][0-9]):([0-9][0-9])/** @time:\1:\2\n\n/' |
                sed -r 's/^\s*//' |
                fmt
        } |
        sed -r 's/^/%%%%%%%%%%%/'
    ) \
    >(
    xidel -s - -e '/opml/body/outline/outline[1]/outline/concat(@complete, ":::", @text)' |
        sed '/^:::$/d; s/^true:::/** DONE /; s/^:::/** TODO /' |
        sed -r 's/^/%%%%%%%%%%%/'
    ) \
        |
        sed -r '/^%%%%%%%%%%%/!d; s/^%%%%%%%%%%%//' |
        xclip -sel cli
