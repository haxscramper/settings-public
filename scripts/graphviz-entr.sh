#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset

if [ -z "$1" ]; then
    echo "Missing file argument"
fi

if ! [ -e "$1" ]; then
    echo -e "digraph G{\n  <++>\n}" > "$1"
fi


dot -Tpng "$1" > "$1.png"
sxiv "$1.png" &

echo "watching file $1"
inotifywait -e close_write -m "$1" |
    while read -r events filename; do
        echo "File changed"
        if dot -Tpng "$1" > "$1.tmp.png"; then
           cp "$1.tmp.png" "$1.png"
        fi
    done

