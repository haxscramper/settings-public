#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit


if [[ "$1" = "-h" ]]; then
    cat << EOF | groff -man -Tascii
.TH remove.sh 1 '2019-04' '1.0'
.SH NAME
remove.sh
.SH SYNOPSIS
Remove first argument to the second and display fancy message
.SH DESCRIPTION
Identical to
.SB "rm \$1"
but with nice formatted message
.SS Options
.TP
-h
display this message and exit
EOF

    exit 1
fi


if [ "$#" -ne 1 ]; then
    colecho -e2 "remove.sh requires exactly one argument: file to remove."
fi

what="$1"

echo -e "  --\033[31mx\033[0m $what"

rm "$what"
