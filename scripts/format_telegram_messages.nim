import hmisc/other/[hshell, oswrap]
import hmisc/algo/htext_algo
import hmisc/helpers
import sequtils, strutils, times, strformat, std/wordwrap

# startHax()

let clipboard = evalShellStdout(ShellExpr "copyq eval 'print(read(0))'")
# echo clipboard
# let clipboard = "input.txt".readFile()

func getInsideBalanced(
  str, delimOp, delimClose: string, returnUnbalanced: bool = true): string =
  var
    cnt = 0
    idx = 0
    startPos = 0

  while idx < str.len:
    if str.continuesWith(delimOp, idx):
      idx += delimOp.len

      if cnt == 0:
        startPos = idx

      inc cnt
    elif str.continuesWith(delimClose, idx):
      dec cnt
      if cnt == 0:
        return str[startPos ..< idx]
      else:
        idx += delimClose.len
    else:
      inc idx

  return str[startPos .. ^1]


func getInsideBalanced(
  str: string, delim: char, returnUnbalanced: bool = true): string =
  let (delOp, delClose) = case delim:
    of '[', ']': ("[", "]")
    of '{', '}': ("{", "}")
    of '(', ')': ("(", ")")
    else: raiseAssert("Invalid delimiter pair")

  getInsideBalanced(str, delOp, delClose)

assertEq "[[00]]".getInsideBalanced("[", "]"), "[00]"
assertEq "((00))".getInsideBalanced("((", "))"), "00"

let names = ["haxscramper", "𝖍𝖆𝖝𝖘𝖈𝖗𝖆𝖒𝖕𝖊𝖗"]

let spl = clipboard.split("\n")
var idx = 0
while idx < spl.len:
  var next = idx + 1
  if spl[idx][names]:
    let
      date = spl[idx].getInsideBalanced('[')
      time = date.parse("dd'.'mm'.'yy HH:mm")
      formatted2 = time.format("HH:mm")
      formatted = time.format("yyyy-MM-dd ddd HH:mm")
      section = &"""
** @time:{formatted2};
   :PROPERTIES:
   :CREATED:  <{formatted}>
   :END:
"""
    # echo time
    while next < spl.len and (not spl[next][names]):
      inc next

    # echov (idx, next)
    let text = spl[(idx + 1) .. (next - 1)]
    idx = next
    var buf: string = section & "\n"
    for line in text:
      # line.splitText().wrapText(80).joinText() & "\n"
      buf.add line.wrapWords(80) & "\n"

    echo buf
  else:
    inc idx
