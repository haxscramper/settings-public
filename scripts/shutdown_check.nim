#!/usr/bin/env nimcr
#nimcr-args c --verbosity:0 --hints:off --cc:tcc

import parsetoml
import os
import hmisc/shell
import re
import strformat
import colechopkg/lib
import options
import algorithm
import hmisc/halgorithm

const configDir = "~/.config/hax-config/config".expandTilde()
let config = parsetoml.parseFile(os.joinpath(configDir, "files_and_dirs.toml"))



proc checkRepositories(conf: TomlValueRef) =
  for dir in config["git_repositories"].getElems():
    let path = dir.getStr().expandTilde()
    echo path
    var status: seq[tuple[
      idx, wktree, path: string, new: Option[string]
    ]]

    for line in iterstdout("git status --porcelain"):
      if line =~ re"(.)(.) (((.+?) -> (.+))|(.+))":
        let x = matches[0]
        let y = matches[1]
        echo matches
        let pathChange = matches[4].len > 0 and matches[5].len > 0

        if pathChange:
          status.add (x, y, matches[4], some(matches[5]))
        else:
          status.add (x, y, matches[7], none(string))

    status = status.sortedByIt((it.idx, it.wktree))
    echo status
    let merged = status.mergeUniqByIt(it.idx & it.wktree)
    for eql in merged:
      case eql[0].idx & eql[0].wktree:
        of "??": echo "untracked files:"
        of " M": echo "modified:"
        else: (for it in eql: echo it)



checkRepositories(config)
