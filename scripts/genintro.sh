#!/usr/bin/env bash

## DOCS: Add intro picture for mp4 video. Text on picture is 
## taken from video name
## DEPS: colecho,ffmpeg,ffprobe,convert
## USAGE: `genintro.sh file.mp4`

function msg () {
    colecho -i:0 -- $@
}

function ffmp () {
    ffmpeg -hide_banner -loglevel panic -y $@
}

function make_intro() {
    file=$1
    name=$(parse-path --name $file)
    resolution=$(ffprobe -v error -select_streams v:0 \
        -show_entries stream=width,height -of csv=s=x:p=0 $file)

    msg "Creating image for $file, name is $name, resolution $resolution"

    convert -size $resolution xc:white \
        -font "FreeMono" -pointsize 360 \
        -fill black -draw "text 200,300 '$name'" $file.png

    msg "Creating slideshow"

    ffmp \
        -framerate 1.5 -i "$file.png" -c:v \
        libx264 -r 30 -pix_fmt yuv420p intro_$file

    msg "Adding audio"

    ffmp -i "intro_$file" -f lavfi -i \
        aevalsrc=0 -shortest -y "intro1_$file"

    msg "Merging into single video"
    echo -e "file 'intro1_$file'\nfile '$file'" > list.txt
    ffmp -f concat -safe 0 -i list.txt -c copy result_$file
}

make_intro $1

