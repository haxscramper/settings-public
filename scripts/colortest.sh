#!/usr/bin/env bash

echo "formatting string is \e[<attr>;<backg>;<foreg>m"

echo "background"
for clfg in {40..47} {100..107} 49 ; do
    echo -en "\e[${clfg}m${clfg}\e[0m "
done

echo

echo "foreground"
for clfg in {30..37} {90..97} 39 ; do
    echo -en "\e[${clfg}m${clfg}\e[0m "
done

echo

echo "attribute"
for attr in 0 1 2 4 5 7 ; do
    echo -ne "\e[${attr}mtt\e[0m "
done

exit 0
