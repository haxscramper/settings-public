#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit


if [[ "$1" = "-h" ]]; then
    cat << EOF | groff -man -Tascii
.TH move.sh 1 '2019-04' '1.0'
.SH NAME
move.sh
.SH SYNOPSIS
Move first argument to the second and display fancy message
.SH DESCRIPTION
Identical to
.SB "mv \$1 \$2"
but with nice formatted message
.SS Options
.TP
-h
display this message and exit
EOF

    exit 1
fi


if [ "$#" -ne 2 ]; then
    colecho -e2 "move.sh requires exactly two arguments: source and destination."
fi

what="$1"
where="$2"

echo -e "  \033[31mv\033[0m   $what\n  +-\033[32m>\033[0m $where"

mv "$what" "$where"
