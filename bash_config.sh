#!/bin/bash

envsubst=$(which envsubst)
cat=$(which cat)

PATH="/bin"

while read line
do
    PATH="$PATH:$line"
done < <($cat "$HOME/.config/hax-config/config/path_dirs.txt" | $envsubst)
