== User permissions

=== Groups

* `usermod -a -G <group> <user>` - add user to group
* `useradd -m -G <comma-separated group list> -s <shell path> <user name>`
* `passwd <user>` - set password for user


=== ???

`exec su -l $USER` - can be used to reload groups for current user.
Hasn't worked for me though
