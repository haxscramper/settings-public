# Links

+ https://tutos.readthedocs.io/en/latest/source/Arch.html
+ https://blogs.nologin.es/rickyepoderi/index.php?/archives/87-Copy-n-Paste-in-KVM.html

# Errors

## i3 Error: Status_command not found (exit 127)

Install `i3status`


# Notes

Format efi partition using `mkfs.fat -F32 /dev/sdX`

# Step-by step guide

## Pre-installation

### Partition and mount drives

`fdisk /dev/sdX` to partition drive _sdX_

Use `fdisk -l` to list availiable disks. In fstab dialogue `n` will
create new partition, `t` will change type. If you are using UEFI and
plan to use separate partition for _swap_ you should create three
partitions: ~512 or more MB for EFI (To avoid confusion with MB/Mb you
can choose number that is slightly more than 512, let's say 600M). To
create partition of given size using fdisk you can select end using
`+<size>`.

To change partition's type to EFI type `t`, number of your partition
and EFI partition code (`ef` (you can get codes by typing `L`))

In this example I used `/dev/sdX1` for efi partition, `/dev/sdX2` for
swap and `/dev/sdX3` for root.

### Format paritions

```
mkfs.ext4 /dev/sdX3 # Root partition
mkfs.vfat -F32 /dev/sdX1 # EFI partition
mkswap /dev/sdX2 # Swap
swapon /dev/sdX2
```

### If you are using WiFi

NOTE: Assuming you are not chroot.

Enable wifi by using `wifi-menu`, creating new profile of your
wireless network and then starting it using `netctl start <your
profile name>`

To be able to use `wifi-menu` after installing (after you installed
but didnt' connect to the internet) you should also install `dialog`
pacakge (`pacman -S dialog`) or copy profile created by `wifi-menu`
from `/etc/netctl/<profile name>` to `/mnt/etc/netctl/<profile name>`.

We need to install packages for connecting to the internet as we did
on the start of installation. For that, we will need these packages
(which are included on this USB version, but not on installation):
pacman -S dialog wpa_actiond ifplugd wpa_suppicant sudo zsh That
should be sufficient for making Wi-Fi or wired connection in our new
system, when we finish work from here. There are also two useful
packages sudo and zsh. I will cover them in next paragraph.

## Installation

### Install the base packages

```
pacstrap /mnt base
```

### Configure the system

#### Generate fstab

```
genfstab -U /mnt >> /mnt/etc/fstab
```

#### Chroot into the new system

```
arch-chroot /mnt
```

#### Time zone

Set the time zone:

```
ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
```

Run hwclock(8) to generate /etc/adjtime:

```
hwclock --systohc
```

#### Setup users

##### root

Use `passwd` to chage root password

##### Other users

NOTE: This part can be done after installation

Use `useradd -m -s /usr/bin/bash <username>` to create new user. `-m`
option will generate home directory for the user at
`/home/<username>`. `-s` allows to select shell for the user. If you
want to use different shell you might install it with `pacman -S <your
shell package>` and use `/usr/bin/<your shell binary>`

#### Generate `initramfs`

```
mkinitcpio -p linux
```

#### Install bootloader

```
pacman -S grub
# grub-install --target=i386-pc --recheck /dev/sda -v
# Assuming you have changed root to /mnt
grub-install --bootloader-id=GRUB --target=x86_64-efi --efi-directory=/efi -v
grub-mkconfig -o /boot/grub/grub.cfg
```

##### Errors

`grub install error efibootmgr not found` -> `pacman -S efibootmgr`

TODO: What does `--recheck` do?

TODO: `Warning! This MBR partition does not have a unique signature`

TODO: When installing grub without chroot: `failed to get canonical
path to ...`

## Making system usable

### X server and window manager setup

Install `i3-gaps i3status xorg xorg-xinitrc rxvt-unicode
rxvt-unicode-terminfo`

Put

```
#!/usr/bin/env bash
exec i3
```

into your `~/.xinitrc` file (Have to be done for each user?)

TODO: Put link to my xdefaults settings

#### Errors

[source, bash]
----
Failed to load module "intel" (module does not exit)
No drivers available
no screens found
----

##### No screen found

You have to [install
dirvers](https://wiki.archlinux.org/index.php/xorg#Driver_installation)


https://www.reddit.com/r/archlinux/comments/2mvbqw/xorg_no_screens_found_on_amd_suddenly_after/

### Adding users

+ `useradd -m <username> -s /usr/bin/<your shell> -G sudo` to create
  new user with home directory at `/home/<username>`, _<your shell>_
  as default shell that also is in the _sudo_ group
+ `passwd <username>` to set password for user

+ `userdel <username>` to delete user
+ `chsh` to change shell for user
+ `usermode -a -G <group> <user>` to append _<user>_ to _<group>_

#### Allow user to use `sudo`

Login as root or `su` to get root prompt, type `visudo`, an editor
will open find a line says `root ALL=(ALL) ALL` add one with your
username below that `<username> ALL=(ALL) ALL`

### Minimal i3 configuration changes

#### Support for changing brightness using multimedia keys

https://wiki.archlinux.org/index.php/backlight
https://askubuntu.com/questions/823669/volume-sound-and-screen-brightness-controls-not-working

To use `xbacklight` you have to install `xorg-xbacklight` package

Use `acpilight` package (info on arch wiki backlight page)

#### Kitty terminal exits with locale error

Probably missing/broken locale. Check `/etc/locale.gen` file: it
should have necessary locale uncommented. Then run `sudo locale-gen`.
This should fix issue

#### Could not find strip utility

Install `base-devel` packages


### Setting utility packages

`pacman -S emacs neovim exa fish quteborwser`

`git clone -b develop https://github.com/syl20bnr/spacemacs
~/.emacs.d` - isntall spacemacs

`https://gitlab.com/haxscamper-misc/settings-public.git` - install
settings

#### ssh-keygen is not isntalled

ssh-keygen utility is a prt of `openssh` package




https://bbs.archlinux.org/viewtopic.php?id=156064
https://www.dedoimedo.com/computers/grub.html
https://antergos.com/wiki/miscellaneous/how-to-fix-grub-with-efi-boot/
https://www.x.org/wiki/FAQErrorMessages/#index5h2
