use hcheat

testConfig = ~/test.cheat

fn selectKey [map]{
  res = ?(keys $map | each [key]{ echo $key } | fzf)
  if $res {
    put $res
  } else {
    put ""
  }
}

fn selectCheat [file @files]{
  cheatMap = (hcheat:toFzfSelection (hcheat:toCheats $file))
  selected = (selectKey $cheatMap)
  if (==s $selected "") {
    put $false
  } else {
    put $cheatMap[$selected]
  }
}

fn cheat []{
  selected = (selectCheat $testConfig)
  hcheat:currentCheatSheet = $selected
  if $selected {
    put $selected['command']
    edit:current-command = $selected['command']
  }
}

edit:insert:binding[Alt-r] = {
  replaced = (hcheat:replacePrevousPlaceholder \
    $edit:current-command                      \
    $edit:-dot                                 \
    (hcheat:getDefaultReplacements)            \
    &nofzfz=$true                              \
  )

  edit:current-command = $replaced[str]
  edit:-dot = $replaced[pos]
}
