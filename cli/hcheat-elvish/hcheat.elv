#!/usr/bin/env elvish
use re

useDebug = $false

fn msg [@a]{
  if $useDebug {
    colecho_path = (which colecho)
    if (==s $colecho_path "") {
      echo -- $@a > /dev/tty
    } else {
      colecho $@a > /dev/tty
    }
  }
}

fn pmsg [arg]{
  pprint $arg > /dev/tty
}


# TODO add support for placeholder documentation that will be shown
# when cheat-sheet is selected.
fn makeCheat []{
  put [
    &warning= []
    &description= []
    &command= ""
    &replacement= ""
  ]
}

fn append [list item]{
  list = [$@list $item]
  put $list
}


# List of entries that can be specified for a given command
fn makeStages []{
  put [
    &description= $false
    &warning= $false
    &command= $false
    &replacement= $false
  ]
}

fn parseFile [input]{
  cheats = [(makeCheat)]
  curr = 0
  in_multiline = $false
  descStages = (makeStages)

  cat $input | each [line]{
    # Command description
    if (re:match '#\?.*' $line) {
      # If current command already has description and command specified
      # and new line contains description it means that it is a start of
      # new command
      commandCanUpdate = (and $descStages['description'] $descStages['command'])

      if $commandCanUpdate {
        curr = (+ $curr 1)
        cheats[$curr] = (makeCheat)
        descStages = (makeStages)
      }

      cheats[$curr]["description"] = (append $cheats[$curr]["description"] $line)
      descStages['description'] = $true

    # Start of the command
    } elif (re:match '>.*' $line) {
      if (re:match '>.*?\\$' $line) {
        in_multiline = $true
      } else {
        # Command has command specified
        descStages["command"] = $true
      }

      cheats[$curr]["command"] = $line

    # Handling multiline command
    } elif (and $in_multiline (re:match '^\s+.*?(\\)?$' $line)) {
      if (re:match '^\s+.*?[!\\]^' $line) {
        in_multiline = $false
        # Command has command specified
        descStages["command"] = $true
      }

      cheats[$curr]["command"] = $cheats[$curr]["command"]"\n"$line
    } elif (re:match '#!.*?' $line) {
      cheats[$curr]['warning'] = (append $cheats[$curr]['warning'] $line)
      # TODO support multiline replacement strings
    } elif (re:match '\$ [a-zA-Z-_0-9+]+:.*' $line) {
      cheats[$curr]['replacement'] = $line
    }
  }

  put $cheats
}

fn cleanupCheat [cheat]{
  if (> (count $cheat['warning']) 0) {
    cheat['warning'] = (put $cheat['warning'] |
      explode (all) |
      each [line]{
      put (re:replace "^#! " "" $line)
    })
  }

  cheat['command'] = (re:replace '^> ' '' $cheat['command'])

  put $cheat
}

fn toCheats [file]{
  put [(parseFile $file | explode (all) | each [cheat]{
      put (cleanupCheat $cheat)
  })]
}

# Generate map where all description lines mapped to the original
# cheats. It allows to select single line from the description and
# still be able to get result back.
fn toFzfSelection [cheats]{
  res = [&]
  explode $cheats |
  each [cheat]{
    put $cheat['description'] |
    explode (all) |
    each [descr]{
      res[$descr] = $cheat
    }
  }

  put $res
}

placeholderRegex = '%([a-zA-Z_-]+)%'

fn findPreviousPlaceholder [position @lines]{
  text = ""
  if (> (count $lines) 1) {
    text = (joins " " $@lines)
  } else {
    text = $@lines
  }

  placeholders = [(re:find $placeholderRegex $text)]

  closestPlaceholder = $false
  # There is at least one placeholder and it starts before current
  # cursor position
  if (and (> (count $placeholders) 0) \
          (< $placeholders[0][start] $position)) {
    prev = $placeholders[0]
    closestPlaceholder = $prev
    for place $placeholders[1:] {
      # placeholder starts before cursor, but is closer to it than
      # previous one
      if (and (<= $place[start] $position) \
              (< $prev[end] $place[start])) {
        prev = $place
        closestPlaceholder = $prev

        # Found placeholder that is starting after cursor
      } elif (> $place[start] $position) {
        closestPlaceholder = $prev
      }
    } else {
      msg -i "only one placeholder"
    }
  } else {
    msg -e "no placeholders found"
  }

  if $closestPlaceholder {
    put $closestPlaceholder
  } else {
    put $false
  }
}

fn replacePrevousPlaceholder [
  inputString
  position
  replacements
  &nofzfz=$true
]{
  place = (findPreviousPlaceholder $position $inputString)
  outputString = $inputString
  outputPosition = $position

  if $place {
    name = (re:replace $placeholderRegex '${1}' $place[text])
    selected = ""
    if (has-key $replacements $name) {
      command = $replacements[$name]
      if (not $nofzfz) {
        selected = (bash -c $command | fzf --select-1)
      }
    }

    # TODO add support for replacing *only* single previous
    # placeholder and all of them.
    outputString = (replaces $place[text] $selected $inputString)
    outputPosition = $place[start]
  } else {
    msg -e "no matching placeholders"
  }

  put [&str= $outputString &pos= $outputPosition]
}


currentCheatSheet = $false

fn getDefaultReplacements []{
  replacements = [&]
  if (has-key 'replacement' $currentCheatSheet) {
    replacements = $currentCheatSheet[replacement]
  }

  put $replacements
}


# for test ["<sdfd>" "<sdfasdfasdf>"] {
#   pmsg (re:find $placeholderRegex $test)
# }

# pmsg (replacePrevousPlaceholder "<sdfsdf>    " 12 [&dir= "echo 'test'"])


testConfig = ~/.config/hax-config/cli/test.cheat

# pprint (toFzfSelection (toCheats $testConfig))
