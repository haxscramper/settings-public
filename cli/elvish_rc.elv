use epm
use re
use str

epm:install &silent-if-installed github.com/zzamboni/elvish-completions
use github.com/zzamboni/elvish-completions/vcsh
use github.com/zzamboni/elvish-completions/cd
use github.com/zzamboni/elvish-completions/ssh
use github.com/zzamboni/elvish-completions/builtins
use github.com/zzamboni/elvish-completions/git
use github.com/zzamboni/elvish-modules/util
use github.com/zzamboni/elvish-modules/alias


-exports- = (alias:export)

mkdir -p /tmp/hax-trash

edit:insert:binding["Ctrl-["] = $edit:command:start~
edit:command:binding["Ctrl-["] = $edit:command:start~

fn msg [@a]{
  echo -- $@a > /dev/tty
}

E:TERM=xterm
E:LANG = en_US.UTF-8
E:LC_ALL = en_US.UTF-8
E:LC_CTYPE = UTF-8
E:HAX_CONFIG_DIR = ~/.config/hax-config
E:HAX_LOCAL_DIR = ~/.config/hax-local
E:HAX_CONFIG_FILES_DIR = $E:HAX_CONFIG_DIR/config
E:NPM_PACKAGES = $E:HOME"/.npm-packages"

E:PATH = (
    cat $E:HOME/.config/hax-config/config/path_dirs.txt |
    envsubst |
    eawk [path _]{ put $path } |
    str:join ":"
)

fn reload []{
  msg "reloading elvish shell"
  exec elvish
}

sequence = ""
fn cleanseq []{ sequence = "" }
fn addseq [key]{ sequence = $sequence""$key }
fn setseq [key]{ sequence = $key }

edit:insert:binding[Ctrl-t] = {
  try { edit:insert-at-dot (fd | fzf 2> /dev/tty) } except e { }
}

edit:insert:binding[Ctrl-w] = { edit:kill-small-word-left }

edit:insert:binding[Alt-Enter] = { edit:insert-at-dot "\n" }


edit:command:binding["d"] = {
  if (==s $sequence "") {
    setseq "d"
  } elif (==s $sequence "d") {
    edit:kill-small-word-right
    cleanseq
  }
}

edit:insert:binding[Alt-Up] = { edit:move-dot-up }
edit:insert:binding[Alt-Down] = { edit:move-dot-down }
edit:command:binding["B"] = { edit:move-dot-left-word }
edit:command:binding["^"] = { edit:move-dot-sol }
edit:command:binding[";"] = { edit:move-dot-right }
edit:command:binding["l"] = { edit:move-dot-left }

edit:command:binding["A"] = { edit:move-dot-eol ; edit:listing:close }
edit:command:binding["I"] = { edit:move-dot-sol ; edit:listing:close }

edit:command:binding["w"] = {
  if (==s $sequence "d") {
    edit:kill-small-word-right
    cleanseq
  } elif (==s $sequence "W") {
    edit:kill-word-right
    cleanseq
  }
}


edit:prompt = { put (basename (pwd)) " ___\n" }
edit:rprompt = { put '' }

alias:arg-replacer = '<++>'

alias:new ls exa --sort type
alias:new lsl exa --long --header --git --sort type
alias:new lsa exa --all --long --header --git --sort type
alias:new lst exa --tree
alias:new lsta exa --tree --long --header --sort type --git --all
alias:new lstl exa --tree --sort type --git --all
alias:new cp e:cp -r
fn rm [@args]{ e:mv --backup=numbered -vt /tmp/hax-trash $@args }

alias:new ncdu "E:ncdu --color dark -x --exclude .git --exclude node_modules"
alias:new find-lib "ldconfig -p | grep -i"
alias:new vi nvim
alias:new ec emacsclient --nw
alias:new mkdir e:mkdir -p


edit:abbr['paci '] = 'sudo pacman -Sv '
edit:abbr['pacl '] = 'pacman -Qqe '
edit:abbr['pacr '] = 'sudo pacman -Rnsv '
edit:abbr['pacu '] = 'sudo pacman -Syvu '
edit:abbr['yaoi '] = 'yay  --noconfirm '
edit:abbr['gis ']  = 'git status -s ; echo ; git branch -v ; echo ; git stash list '
edit:abbr['gisn ']  = 'git status -s . ; echo ; git branch -v ; echo ; git stash list '
edit:abbr['gicm ']  = 'git commit -m "['
edit:abbr['gil ']  = 'git log --graph --oneline --decorate -n20 '
edit:abbr['gia ']  = 'git add '
edit:abbr['gid ']  = 'git diff '
edit:abbr['gids ']  = 'git diff --staged '
edit:abbr['gipm ']  = 'git push --atomic origin master '
edit:abbr['xcp '] = 'xclip -sel cli '
edit:abbr['xcpi '] = 'xclip -sel cli -t image/png -i '
edit:abbr['xco '] = 'xclip -out '
edit:abbr['xcoi '] = 'xclip -sel cli -t image/png -o '
alias:new xcpp "find -maxdepth 2 -type f -print0 | fzf --read0 --multi --print0 | xargs -r0 realpath | xclip -sel cli "

fn kt {
  e:kitty @ set-window-title kitty-target
}

fn cj {
  target_dir = (cat $E:HAX_CONFIG_FILES_DIR/common_dirs.txt | envsubst | fzf | awk '{print $2}')
  if ?(test -d $target_dir) {
    cd $target_dir
  }
}

fn to-showlist []{
  cat -n |
  eawk [line num item]{
    put [&to-filter=$num &to-accept=$item &to-show=$item]
  }
}

fn select-list [cb caption]{
  edit:listing:start-custom [($cb | to-showlist)] &accept=$put~ &caption=$caption
}

fn anyprefix [str prefixes]{
  for pref $prefixes {
    if (str:has-prefix $str $pref) {
      put true
      return
    }
  }
  put false
}

fn prompt-confirm [prompt]{
  prompt = $prompt" [Y/n] "
  print $prompt > /dev/tty
  resp = (util:readline)
  if (anyprefix $resp [y Y]) {
    true
  } elif (eq $resp '') {
    true
  } else {
    false
  }
}

fn nr_setup [file]{
  if ?(test ! -e $file) {
    if (prompt-confirm "Create file?") {
      has-file = true
      e:echo -ne "echo 1\n" > $file
    } else {
      return
    }
  }

  path = (realpath $file)

  emacsclient --eval "(find-file \""$path"\")"
  echo "Run using build"
  e:kitty @ set-window-title "nimble-rebuild "$file
}

fn nr [file]{
  nr_setup $file
  if (str:compare $file[0] "/") {
    cd (dirname $file)
  }
  result = (str:trim-suffix $file ".nim")
  cache = (str:trim-suffix (basename $file) ".nim")
  e:fd --type f . |
  e:entr -rc sh -c "time nim c -r -o:"$result" --nimcache:/tmp/nimcache/"$cache" "$file


}


fn nr1 [file]{
  nr_setup $file
  echo $file | e:entr -rc sh -c "time nim c -o:"$file".bin "$file" && ./"$file".bin"
}





fn nd []{
  ~/.config/hax-config/scripts/nimble-rebuild.fish
}
