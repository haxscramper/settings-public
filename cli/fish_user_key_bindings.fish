function insert_navi
    commandline -r (navi --print)
end

# FIXME copies right prompt
function copy_history_to_clipboard
    set -l selected ( history | fzf )

    if test -n "$selected"
        xclip -sel cli
    end
end

function fish_user_key_bindings
    bind l backward-char
    bind \; forward-char

    bind -M visual l backward-char
    bind -M visual \; forward-char

    source ~/.config/hax-config/cli/fzf_key_bindings.fish

    bind \cw backward-kill-bigword force-repaint
    bind \ce forward-kill-bigword force-repaint
    bind \cd delete-char


    bind -e \cJ -M insert
    bind -M insert \en ranger
    # bind -M insert \ch copy_history_to_clipboard
    # FIXME cannot type anything after use of this

end

function delete-or-exit
	  set -l cmd (commandline)

    switch "$cmd"
        case '*'
            commandline -f delete-char

    end
end
