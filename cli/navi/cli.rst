Add python file as module to use from different script
=====

- For user-local python installation
  :command: `ln -sfv (realpath %file_to_add.py%) (python -m site
            --user-site)`
- For currently active virtual environment
  :command: `ln -svf (realpath %file_to_add.py%) (pip show numpy | grep
            Location | cut -d' ' -f2)`
  :note: This assumes `numpy` is installed in current virtual
         environment. Any other package can be substituted instead as
         long as it is is installed for this virtual environment
         instance.

Add file as globally available latex package
=====

:command: `mkdir -p ~/texmf/tex/latex/local/ && ln -svg (realpath
          %filename%.tex) ~/texmf/tex/latex/local/%filename%.sty`
:docs: `%filename%` is the name of the file **without** extension
