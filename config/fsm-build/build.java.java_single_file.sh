#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    javac "$input_file"
}

function run {
    java $build_file
}

function get_config {
main_class=$(parse-path --name "$input_file")

    cat << EOF
build_file = "$main_class"
file_globs = [ "\$input_file" ]
EOF
}

function cleanup {
    true
}

function preview {
    true
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
    cleanup)
        cleanup
        ;;
    preview)
        preview
        ;;
esac
