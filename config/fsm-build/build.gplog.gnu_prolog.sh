#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    cp $input_file $input_file.tmp.pl
    gplc -o $build_file $input_file.tmp.pl
    rm $input_file.tmp.pl
}

function run {
    ./"$build_file"
}

function get_config {
    cat << EOF
name = "GNU prolog compile"
build_file = "\$input_file.bin"
EOF
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
esac
