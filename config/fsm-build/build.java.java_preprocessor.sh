#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

run_flags="${additional_run_flags:-''}"

function set_java_11_to_path {
    java_11_bin=$(
        echo $PATH                                        |
            tr ':' '\n'                                   |
            xargs -i find '{}' -name "javac" 2> /dev/null |
            grep 11                                       |
            xargs -i parse-path --dirname '{}'            |
            uniq)

    if [[ -z "$java_11_bin" ]]; then
        $msg -e "Could not find java 11"
    else
        PATH="$java_11_bin:$PATH"
    fi
}

function build {
    set_java_11_to_path

    m4 "$input_file" > $input_file.tmp
    $fsm_dir/java.pl $input_file.tmp
    mv $input_file.tmp .$input_file.tmp.d/

    $msg -i "Done build"
}

function run {
    set_java_11_to_path

    java -jar "$build_file" $run_flags
}

function get_config {
    cat << EOF
build_file = ".\$input_file.tmp.out.d./out.jar"
file_globs = [ "*.java" "*.m4" ]
EOF
}

function cleanup {
    rm -r ".$input_file.tmp.out.d"
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    build_run)
        build
        run
        ;;
    get_config)
        get_config
        ;;
    cleanup)
        cleanup
        ;;
esac
