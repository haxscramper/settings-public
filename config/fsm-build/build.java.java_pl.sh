#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    $fsm_dir/java.pl "$input_file"
}

function run {
    java -jar "$build_file"
}

function get_config {
    cat << EOF
file_globs = [ "*.java" ]
build_file = ".\$input_file.d/out.jar"
EOF
}

function cleanup {
    rm -r ".$input_file.d/"
}

function preview {
    true
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
    cleanup)
        cleanup
        ;;
    preview)
        preview
        ;;
esac
