#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

build_flags="${additional_build_flags:-''}"
run_flags="${additional_run_flags:-''}"
preview_flags="${additional_preview_flags:-''}"
cleanup_flags="${additional_cleanup_flags:-''}"

function build {
    clang++ \
        --std=c++17 \
        -o "$build_file" \
        -g \
        -DDEBUG \
        "$input_file"
}

function run {
    ./"$build_file"
}

function get_config {
    cat << EOF
name = "clang debug build"
file_globs = [ "\$input_file", "*.hpp" ]
build_file = "\$input_file.bin"
EOF
}

function cleanup {
    true
}

function preview {
    true
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
    cleanup)
        cleanup
        ;;
    preview)
        preview
        ;;
esac
