#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    g++ -o "$build_file" \
        -fdiagnostics-format=json \
        "$input_file" 2>&1 | \
        jq .[].message
}

function run {
    $msg 'running'
    ./"$build_file"
}

function get_config {
    cat << EOF
name = "Build template"
type = "compiled"
build_file = "\$input_file.bin"
file_globs = [ "\$input_file" ]
EOF
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
esac
