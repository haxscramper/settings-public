#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    ocamlc -o "$build_file" "$input_file"
}

function run {
    ./"$build_file"
}

function get_config {
    cat << EOF
name = "Ocaml simple"
file_globs = [ "\$input_file" ]
build_file = "\$input_file.bin"
EOF
}

function cleanup {
    true
}

function preview {
    true
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
    cleanup)
        cleanup
        ;;
    preview)
        preview
        ;;
esac
