#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    m4 $input_file > $input_file.tmp
    # Use intermediate file to not crash sxiv when bad png is
    # generated
    dot -Tpng $input_file.tmp -o$build_file.tmp
    cp $build_file.tmp $build_file

    # if 'disable_fulle_clean' is not set remove temporary files
    if [[ ! disable_full_clean || $disable_full_clean = "true" ]]; then
      rm $build_file.tmp
      rm $input_file.tmp
    fi
}

function run {
    true
}

function get_config {
    cat << EOF
build_file = "\$input_file.tmp.png"

[options.disable_full_clean]
descr = "Do not repove temporary files after build"
type = bool
EOF
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
esac
