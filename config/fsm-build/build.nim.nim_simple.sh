#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    nim c \
        -o:"$build_file" \
        "$input_file"
}

function run {
    ./"$build_file"
}

function get_config {
    cat << EOF
name = "Nim simple"
type = compiled
file_globs = [ "*.nim" ]
EOF
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
esac
