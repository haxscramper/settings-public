#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    mypy "$input_file"
}

function run {
    python "$input_file"
}

function get_config {
    cat << EOF
name = "Python mypy"
file_globs = [ "*.py" ]
EOF
}

function cleanup {
    true
}

function preview {
    true
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
    cleanup)
        cleanup
        ;;
    preview)
        preview
        ;;
esac
