#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    nim c \
        --cc:tcc \
        --verbosity:0 \
        --hints:off \
        -o:"$build_file" \
        -d:nimOldCaseObjects  \
        --warning[CaseTransition]:off \
        "$input_file"
}

function run {
    ./$build_file
}

function get_config {
    cat << EOF
name = "Nim legacy"
type = "compiled"
build_file = "\$input_file.bin"
file_globs = [ "\$input_file" ]
EOF
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
esac
