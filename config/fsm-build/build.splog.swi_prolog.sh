#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    true
}

function run {
    swipl -t main -g halt -f "$input_file"
}

function get_config {
    cat << EOF
name = "SWI prolog simple"
EOF
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
esac
