#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    nim c \
        --cc:tcc \
        -o:"$build_file" \
        "$input_file"
}

function run {
    ./$build_file
}

function get_config {
    cat << EOF
name = "Nim tcc"
type = "compiled"
build_file = "\$input_file.bin"
file_globs = [ "*.nim" ]
EOF
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
esac
