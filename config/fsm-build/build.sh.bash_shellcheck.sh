#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    true
}

function run {
    shellcheck \
        --exclude=SC2034 \
        "$input_file"

    ./"$input_file"
}

function get_config {
    cat << EOF
name = "Bash shellcheck"
file_globs = [ "*.sh" ]
EOF
}

function cleanup {
    true
}

function preview {
    true
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
    cleanup)
        cleanup
        ;;
    preview)
        preview
        ;;
esac
