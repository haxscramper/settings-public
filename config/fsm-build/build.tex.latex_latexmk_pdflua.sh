#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    latexmk \
        -latexoption="-shell-escape" \
        -pdflua \
        --interaction=nonstopmode \
        "$input_file"
}

function run {
    true
}

function cleanup {
    latexmk -C "$input_file"
}

function get_config {
    cat << EOF
name = "latexmk pdflua"
file_globs = [ "*.tex" ]
EOF
}

function preview {
    zathura "$build_file" &
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
    cleanup)
        cleanup
        ;;
    preview)
        preview
        ;;
esac
