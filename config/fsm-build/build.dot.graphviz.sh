#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    dot -Tpng "$input_file" -o"$output_file"
}

function run {
    true
}

function get_config {
    cat << EOF
name = "Graphviz simple"
build_file = "\$input_file.bin"
EOF
}

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
esac
