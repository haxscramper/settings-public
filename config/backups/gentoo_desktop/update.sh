#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
cd "$(dirname "$0")"
set -o nounset

colecho -i2 "Copying gentoo desktop configuration"

copy () {
    what="$1"
    where="$2"

    rm "$where"
    copy.sh "$what" "$where"
    echo "# Copied from $what" >> "$where"
}




copy /etc/portage/make.conf etc_portage_make.conf
copy ~/.xinitrc xinirtc.sh
copy /etc/portage/package.use/use.conf portage_package_use.conf
copy /usr/src/linux/.config linux_kernel_config.conf
copy /etc/X11/xorg.conf.d/10-keyboard.conf xorg_keyboard_config.conf
copy /etc/X11/xorg.conf.d/10-monitor.conf xorg_screen_config.conf
copy /var/lib/portage/world portage_world_set

colecho -i1 "Done copying gentoo desktop configuration"
echo

exa -alh --color=always | sed 's/^/    /'
