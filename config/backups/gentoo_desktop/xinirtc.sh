#!/usr/bin/env bash

xset s off
xset -dpms
xset s noblank

xserverauthfile=$XAUTHORITY

exec awesome
# Copied from /home/test/.xinitrc
