#!/usr/bin/env perl
# -*- coding: utf-8 -*-
# perl

use warnings;
use strict;

# Command line argument parsing
use Pod::Usage;
use Getopt::Long;
use Data::Dumper;

# Allow to use true and false as named constants
use constant false => 0;
use constant true  => 1;

# Allow to use functions with parameter signatures
use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);

sub log1 ($message, $ind = 0) { system("colecho -I:$ind -- '$message'"); }
sub err1 ($message, $ind = 0) { system("colecho -I:$ind -e:2 -- '$message'"); }
sub info1($message, $ind = 0) { system("colecho -I:$ind -i:1 -- '$message'"); }
sub warn1($message, $ind = 0) { system("colecho -I:$ind -w:1 -- '$message'"); }

say "Hello world";
