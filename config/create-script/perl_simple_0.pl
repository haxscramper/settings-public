#!/usr/bin/env perl
# -*- coding: utf-8 -*-
# perl
use warnings;
use strict;
# Allow to use true and false as named constants
use constant false => 0;
use constant true  => 1;

# Allow to use functions with parameter signatures
use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);

say "Hello world";
