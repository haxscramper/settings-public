#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit
msg="colecho -b"

function build {
    # Building done using this function
    $msg 'build'
}

function run {
    $msg 'running'

    # If build is successfull this function will be called.
    # <++> 
}

function get_config {
    # Put main configuration options here
    cat << EOF
    # <++>
EOF
}


# !!! default code for calling. Do not modify !!!

case ${1:-""} in
    build)
        build
        ;;
    run)
        run
        ;;
    get_config)
        get_config
        ;;
esac
