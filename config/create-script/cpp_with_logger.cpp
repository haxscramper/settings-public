#include <iostream>
#include <vector>


class Logger
{
  public:
    ~Logger() {
        std::cout << "\b\n";
    }

    static Logger log() {
        return Logger();
    }

    template <class T>
    Logger& operator<<(T t) {
        std::cout << t << " ";
        return *this;
    }
};

#define LOG Logger::log()

int main() {
    LOG << "Logger test";
}
