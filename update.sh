#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit

function msg { colecho -b "$@" > /dev/tty; }
function psdir { pushd "$1" > /dev/null; msg -I:4 "$1"; }
function ppdir { popd > /dev/null; }

function check_for_updates {
    local_path="$1"
    psdir "$local_path"

    ppdir
}

function update_hax {
    path="$1"
    psdir "$path"
    ./update.sh
    ppdir
}

msg -i:1 "Checking status of dependencies"
msg -i "Local submodules ..."

while read -r path
do
    check_for_updates "$path"
done < <(git submodule--helper list | awk '{print $4}')

msg -i "Out-of-tree dependencies ..."

update_hax "$HOME/workspace/hax-nim"
