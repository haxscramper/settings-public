local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

local hotkeys_popup = require("awful.hotkeys_popup").widget
local help_group = "client"

client.connect_signal("manage", function (c)
    get_num_widget(c)
    update_client_numeration()
end)


client.connect_signal("unmanage",
  function (c)
    update_client_numeration()
  end
)

-- function delete_client_numeration(c)
--   -- TODO
-- end

---------------------------  enumerate clients  ---------------------------
-- - IDEA :: Provide order of screen iteration (instead of following order
--   of tags enforced by `tag:clients()`). Prioritise tags positioned on
--   bottom screens (iterate screen grid from right corner in rows and go
--   upwards)

-- Keep track of all client numbering widgets
number_widgets = {} -- Numbering widgets -> clients
client_numbers = {} -- Clients -> client numbers

--- Get numbering widget for client. If no widget were registered
--- create new one and add it to the table.
function get_num_widget(cl)
  if number_widgets[cl] == nil then
    wgt = wibox.widget.textbox()
    wgt:set_text("[test]")

    number_widgets[cl] = wgt
  end

  return number_widgets[cl]
end

--- Update all client numbers
function update_client_numeration()
  num = 1
  -- TODO ignore tags that are not currently visible
  for _, tag in pairs(root.tags()) do
    for _, cl in ipairs(tag:clients()) do
      -- print("setting number " .. num .. " to cl " .. tostring(cl))
      local color = "green"
      if cl == client.focus then color = "red" end

      local text = "<span foreground=\"" ..
        color .. "\">" .. "[- " .. num .. " -]</span>"

      if number_widgets[cl] then
        number_widgets[cl]:set_markup_silently(text)
        client_numbers[cl] = num
        num = num + 1
      end
    end
  end
  -- print("---")
end

function switch_to_client(client_class)
  for _, tag in pairs(root.tags()) do
    for _, cl in ipairs(tag:clients()) do

    end
  end
end

-------------------------  switch to nth client  --------------------------

function move_cursor (x, y)
  local command = "xdotool mousemove " ..
    tostring(x) ..
    " " ..
    tostring(y)

  -- print("running command: ", command)

  awful.spawn(command)
end

function mouse_on_client(c)
  -- log("moving mouse to client")
  if c == nil then print("client is nil") end
  local geom = c:geometry()
  -- aldump("geometry: ", geom)

  move_cursor(
    geom.x + geom.width / 3 + 10,
    geom.y + geom.height / 3 + 10
  )
end

function focus_nth_client(n)
  log("focusing client #" .. n)
  for cl, num in pairs(client_numbers) do
    if num == n then
      local status, err = pcall(mouse_on_client, cl)

      if status then
        client.focus = cl
        cl:raise()
        -- log("focused client")
      else
        print(err)
      end
    end
  end
end


client.connect_signal("focus", function(cl) update_client_numeration() end)
client.connect_signal("unfocus", function(cl) update_client_numeration() end)


--- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
      awful.button({ }, 1, function()
          client.focus = c
          c:raise()
          awful.mouse.client.move(c)
      end),
      awful.button({ }, 3, function()
          client.focus = c
          c:raise()
          awful.mouse.client.resize(c)
      end)
    )

    awful.titlebar(c) : setup {
      { -- Left
        awful.titlebar.widget.iconwidget(c),
        get_num_widget(c), -- get numbering widget for client
        buttons = buttons,
        layout  = wibox.layout.fixed.horizontal
      },
      { -- Middle
        { -- Title
          align  = "center",
          widget = awful.titlebar.widget.titlewidget(c)
        },
        buttons = buttons,
        layout  = wibox.layout.flex.horizontal
      },
      { -- Right
        awful.titlebar.widget.ontopbutton    (c),
        awful.titlebar.widget.closebutton    (c),
        layout = wibox.layout.fixed.horizontal()
      },
      layout = wibox.layout.align.horizontal
    }

    update_client_numeration()
end)


for i = 1, 12 do
  globalkeys = gears.table.join(
    globalkeys,
    awful.key({ modkey }, "F" .. i,
      function () focus_nth_client(i) end,
      {description = "focus client #" .. i, group = "client"})
  )
end
