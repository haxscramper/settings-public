;; TODO Insert multiple cursors into visual block mode instead of vim shit
(global-evil-mc-mode 1)


(defun internal/line-length (n)
  "Length of the Nth line."
  (save-excursion
    (goto-char (point-min))
    (if (zerop (forward-line (- n 1)))
        (- (line-end-position)
           (line-beginning-position)))))

(defun internal/current-line-length ()
  "Length of current line"
  (- (line-end-position)
     (line-beginning-position)))

(defun internal/has-char-at-line-col
    (line column &optional ignore-whitespace)
  "Return t if line `line' has at least `column' characters. If
  `ignore-whitespace' is *t* ignore whitespace caracters: if line
  has whitespace character at `column' return nil")

(defun internal/point-has-character-at(num)
  "If there is a character `num' lines below (above if num is
  negative) from the position of the point return
  position of the character. Otherwise return nil"
  (save-excursion
    (let ((line-now (what-line))
          (column-now (current-column))
          (point-now (point)))
      (forward-line num)
      (if (>= (internal/current-line-length)
              column-now)
          (progn
            (beginning-of-line)
            (+ (point) column-now))
        nil))))

(defun internal/exhaustive-search-char (direction &optional search-limit)
  "Exchaustively search for character above current position of
  the cursor. Return position of the character of found,
  otherwiser return nil. Latter case should only be expected if
  search hits beginning/end of the buffer. direction can either
  be 1, -1 or nil. Nil defaults to 1 (search below). Will be
  performed `search-limit` attempts before stopping. If value is
  negative or nil do not stop"
  (if (equal direction nil) (setq direction 1))
  (save-excursion
    (let ((increment (* direction 1))
          (relative direction))
      (loop
       (let ((char-pos (internal/point-has-character-at relative)))
         (cond
          ( ; Found matching character
           (not (equal char-pos nil))
           (return char-pos))
          ( ; Hit search limit
           (and (>= (/ relative increment) search-limit)
                (not (equal search-limit nil)))
           (return nil))
          ( ; Increment and continue search
           t
           (setq relative (+ relative increment)))))))))



(defun ++mc-any-cursors-at-pos (pos)
  "Check if ther is any cursors located at position `pos'"
  (some
   (lambda (cursor) (equal pos (evil-mc-get-cursor-start cursor)))
   evil-mc-cursor-list))

(defun ++mc-ensure-cursor-here ()
  "If there is a cursor at `pos' do nothing, otherwise create new
  evil-mc-cursor"
  (unless (++mc-any-cursors-at-pos (point))
    (evil-mc-make-cursor-here)
    (progn
      (message "Cursor already exists at position. Ignoring"))))

(defun ++mc-ensure-single-cursor-here ()
  "Ensure that there is either mc-cursor or real one at given
  position, and not two at the same time. If there is two
  cursors, remove evil-mc one"
  (message "Ensure single cursore at %s" (point))
  (if (++mc-any-cursors-at-pos (point))
      (evil-mc-undo-cursor-at-pos (point))))


;; TODO Add version that not only ignores lines that don't have enough
;; characters but also lines that have whitespace character

(defun internal/mc-make-cursors-precise-move-line (num &optional dir)
  "Create evil-mc cursor at current position and move precisely
above. If line does not contain enough characters search for
matching line until found. If called with `num' equal to nil
search for line until found and place cursor there. If num is not
nil perform exactly `num' attempts to create cursor. dir can
either be 1, -1 or nil. Nil defaults to 1 (below)"
  (interactive "P")
  (if (equal dir nil) (setq dir 1))
  (if (equal num nil)
      (progn
        (let ((above-pos (internal/exhaustive-search-char dir 100)))
          (if (equal above-pos nil)
              (message "Cannot create cursor above")
            (progn
              (++mc-ensure-cursor-here)
              (goto-char above-pos)
              (++mc-ensure-single-cursor-here)))))
    (progn
      (dotimes (i (+ num 1))
        (let ((above-pos (internal/point-has-character-at (* i dir))))
          (unless (equal above-pos nil)
            (progn
              (save-excursion
                (goto-char above-pos)
                (++mc-ensure-cursor-here)
                (message "%s" above-pos))
              (++mc-ensure-single-cursor-here))))))))

(defun ++mc-make-cursor-above-move-next-line (count)
  "Make cursor precisely above current position of the cursor."
  (interactive "P")
  (internal/mc-make-cursors-precise-move-line count -1))

(defun ++mc-make-cursor-below-move-prev-line (count)
  (interactive "P")
  (internal/mc-make-cursors-precise-move-line count 1))



(map!
 :n "C-S-k" '++mc-make-cursor-above-move-next-line
 :n "C-S-j" '++mc-make-cursor-below-move-prev-line)

;;;#= ESC support for evil-mc

;; https://github.com/gilbertw1/bmacs/blob/master/bmacs.org#helper-functions
(defun bmacs/escape ()
  "Run the `bmacs-escape-hook'."
  (interactive)
  (cond ((minibuffer-window-active-p (minibuffer-window))
         ;; quit the minibuffer if open.
         (abort-recursive-edit))
        ;; Run all escape hooks. If any returns non-nil, then stop there.
        ((run-hook-with-args-until-success 'bmacs-escape-hook))
        ;; don't abort macros
        ((or defining-kbd-macro executing-kbd-macro) nil)
        ;; Back to the default
        (t (keyboard-quit))))

;; https://github.com/gilbertw1/bmacs/blob/master/bmacs.org#hooks
(defvar bmacs-escape-hook nil
  "A hook run after C-g is pressed (or ESC in normal mode, for
evil users). Both trigger `bmacs/escape'. If any hook returns
non-nil, all hooks after it are ignored.")

(global-set-key [remap keyboard-quit] #'bmacs/escape)

;; https://github.com/gilbertw1/bmacs/blob/master/bmacs.org#configuration-1
(advice-add #'evil-force-normal-state :after #'bmacs/escape)

(defun +evil|escape-multiple-cursors ()
  "Clear evil-mc cursors and restore state."
  (evil-mc-undo-all-cursors)
  t)

(add-hook 'bmacs-escape-hook #'+evil|escape-multiple-cursors)
