Each file's name in each directory will be used to generate 
yasnippet snippet and bind it to the shortcut generated from
the name. First letter after each dash will be used in the 
generated bindings. For example `Edit-surround-vector-bold` 
will generate `Esvb` `SPC`-based shortcut for latex-mode.
