;; (defun my/org-render-latex-fragments ()
;;   (if (org--list-latex-overlays)
;;       (progn (org-toggle-latex-fragment)
;;              (org-toggle-latex-fragment))
;;     (org-toggle-latex-fragment)))

;; (add-hook 'org-mode-hook
;;           (lambda ()
;;             (add-hook
;;              'after-save-hook
;;              'my/org-render-latex-fragments
;;              nil
;;              'make-the-hook-local)))

;; (setq hax/internal::font-lock-fontification-mode-highlights
;;       '(("hello" . font-lock-function-name-face)
;;         ))

(define-derived-mode hax/internal::font-lock-fontification-mode fundamental-mode
  "Helper major mode used when calling `font-lock-fontify-text'"
  ;; (setq font-lock-defaults '(hax/internal::font-lock-fontification-mode-highlights))
  )

(defun font-lock-fontify-from-mode (text mode)
  "Fontify text using `mode' font-lock conifguration"
  (with-temp-buffer
    (erase-buffer)
    (insert text)
    (delay-mode-hooks (funcall mode))
    (font-lock-fontify-buffer)
    (buffer-string)))



(defun font-lock-fontify-text (text font-lock-config)
  "Fontify text using `font-lock-config' for highlighting."

  (font-lock-clear-keywords
   'hax/internal::font-lock-fontification-mode)

  (font-lock-add-keywords
   'hax/internal::font-lock-fontification-mode font-lock-config)

  (font-lock-fontify-from-mode
   text hax/internal::font-lock-fontification-mode))

(cl-defun hax/prompt-string
    (prompt-string
     history-name
     &key
     highlight-config
     (store-exit-history t)
     )
  "Prompt user for string.

- `prompt-string' :: prefix string for prompt
- `history-name' :: name of the history list. All prompts with with
  identical history names share history entries
- `highlight-config' :: font-lock configuration to use for syntax
  highlighting user input.
- `store-exist-history' :: add content of the minibuffer to history
  even if user exited with `ESC'"
  )

(let ((highlight-config
       '(("hello" . font-lock-function-name-face)
         ("zzza" . font-lock-warning-face)
         ("zzz" . font-lock-keyword-face)
         ))
      (tmp/prompt-string "Hello "))
  (read-from-minibuffer
   tmp/prompt-string
   (font-lock-fontify-text "hello zzza" highlight-config)
   '(keymap
     (?a . (lambda ()
             (interactive)
             (let ((prompt-content
                    (concat (buffer-substring (+ (point-min) (length tmp/prompt-string))
                                              (point-max))
                            "a")))
               (delete-minibuffer-contents)
               (insert (font-lock-fontify-text prompt-content highlight-config)))))
     (escape . (lambda () (interactive) (message "hello") (keyboard-escape-quit))))
   )
  )

:W
