:;exec emacs -batch -l "$0" -f "main" "$@"

;; (toggle-debug-on-error)

(add-to-list 'load-path "~/.config/emacs-load")

(require 'ob-nim)
(require 's)
(require 'cl-seq)
(require 'cl-extra)

(defun pprint (form &optional output-stream)
  (princ (with-temp-buffer
           (cl-prettyprint form)
           (buffer-string))
         output-stream))



(defvar hax/org-babel-eval-results nil)

;; (defun assoc-multi-key (path nested-alist)
;;   "Find element in nested alist by path."
;;   (if (equal nested-alist nil)
;;       (error "cannot lookup in empty list"))
;;   (let ((key (car path))
;;         (remainder (cdr path)))
;;     (if (equal remainder nil)
;;         (alist-get key nested-alist)
;;       (assoc-multi-key remainder (alist-get key nested-alist)))))

(defmacro al2-get (key-1 key-2 list)
  `(alist-get ,key-2
              (alist-get ,key-1 ,list nil nil 'equal)
              nil nil 'equal))

(when t
  (let ((tmp '((:a . ((:b . 12))))))
    (when nil
      (print (al2-get :a :b tmp))
      (setf (alist-get :q tmp) '((:z . 99)))
      (setf (alist-get :z (alist-get :q tmp)) 1002)
      (setf (al2-get :a :b tmp) 909)
      (setf (al2-get :a :none tmp) 1202)
      (pprint tmp))
    (when nil
      (setq tmp nil)
      (setf (al2-get "he" 2 tmp) :12)
      (setf (al2-get "he" 3 tmp) :12)
      (pprint tmp)
      )
    )
  )

(defun org-babel-eval (cmd body)
  "Run CMD on BODY.
If CMD succeeds then return its results, otherwise display
STDERR with `org-babel-eval-error-notify'."
  (let ((err-buff (get-buffer-create " *Org-Babel Error result*")) exit-code)
    (with-current-buffer err-buff (erase-buffer))
    (with-temp-buffer
      (insert body)
      (setq exit-code
            (org-babel--shell-command-on-region
             (point-min) (point-max) cmd err-buff))

      (let (result-string)
        (setq result-string
              (if (> exit-code 0)
                  (with-current-buffer err-buff (buffer-string))
                (buffer-string)))
        (add-to-list 'hax/org-babel-eval-results
                     (list
                      (cons :cmd cmd)
                      (cons :res result-string)
                      (cons :code exit-code))
                     t)
        nil))))

(setq hax/org-babel-nosession-langs '("nim"))
;; (print (member "nim" hax/org-babel-nosession-langs))

(setq hax/org-session-langs-blocks-alist nil)



(defun org-babel-assemble-session:nim (blocks-list)
  ;; (pprint "\e[31mAssembling session\e[39m")
  ;; (pprint blocks-list)
  (s-join "\n"
          (mapcar
           (lambda (blc)
             (let ((idx (alist-get :idx blc)))
               (concat
                (format "debugecho \"==== begin-runtime: %s ====\"\n" idx)
                (format "static: debugecho \"==== begin-compiletime: %s ====\"\n" idx)
                (alist-get :body blc)
                "\n"
                (format "debugecho \"==== end-runtime: %s ====\"\n" idx)
                (format "static: debugecho \"==== end-compiletime: %s ====\"\n" idx))))
           blocks-list)))

(defun make-marker-at-point ()
  ;; (message "p: %s" (point))
  (let ((m (make-marker)))
    (set-marker m (point) (current-buffer))))

(defun text-between-regex (re-start re-end)
  (re-search-forward re-start)
  (beginning-of-line 2)
  (condition-case nil
      (buffer-substring
       (make-marker-at-point)
       (progn (re-search-forward re-end)
              (beginning-of-line 1)
              (backward-char)
              (make-marker-at-point)))
    ((message "not found") "")))

;; (with-temp-buffer
;;   (insert "
;; AAA
;; ---
;; BBB
;; ")
;;   (goto-char 0)
;;   (message "\e[35mtext:\e[39m\n%s" (text-between-regex (rx "AAA") (rx "BBB")))
;;   )

(defun text-in-session (full-text name needed-idx)
  (message "\e[32mfull text:\e[39m\n%s" full-text)
  (with-temp-buffer
    (insert full-text)
    (goto-char 0)
    (let ((idx (format "%s" needed-idx)))
      (text-between-regex
       (rx "==== begin-" (eval name) ": " (eval idx) " ====")
       (rx "==== end-" (eval name) ": " (eval idx) " ====")))))

(defun org-babel-disassemble-session:nim (evalres needed-idx)
  ;; TODO translate error lines from absolute position to relative (to start of the session block)

  ;; (print "Dissasembing session")
  ;; (pprint evalres)
  ;; (pprint needed-idx)
  (let ((compile-results (s-chomp (text-in-session
                                   (alist-get :res (nth 0 evalres))
                                   "compiletime" needed-idx)))
        (runtime-results (s-chomp (text-in-session
                                   (alist-get :res (nth 1 evalres))
                                   "runtime" needed-idx))))
    ;; (message "compiletime: %s" compile-results)
    ;; (message "runtime: %s" runtime-results)
    (concat compile-results runtime-results)))

(defun org-babel-insert-src-eval-result (header-args result-string info)
  ;; (print result-string)
  (org-babel-insert-result
   result-string
   (assoc :result-params header-args)
   info))

(defun hax/org-babel-execute-advice (func &rest args)
  (let* ((info (nth 0 args))
         (params (nth 1 args))
         (lang (nth 0 params))
         (body (nth 1 params))
         (header-args (nth 2 params))
         (block-idx (alist-get :idx header-args))
         (result))
    ;; (print "Original arguments")
    ;; (print args)
    ;; (pprint header-args)
    (if (not (member lang hax/org-babel-nosession-langs))
        (funcall func args) ;; Has native support for sessions
      (if (not (assoc :session header-args))
          (funcall func args) ;; No `:session' argument
        (if (not (assoc :idx header-args))
            (funcall func args)
          ;; (error ;; Has `:session' but block is missing `:idx'
          ;;  (concat "Lang '%s' does not have native support for "
          ;;          "sessions - block :idx must be specified") lang)
          )
        (setf ;; Store session block
         (al2-get lang (assoc :idx header-args)
                  hax/org-session-langs-blocks-alist)
         (list
          (cons :header-args header-args)
          (cons :body body)))
        (let ((eval-results)
              (session-result)
              (full-body
               (funcall
                (intern (concat "org-babel-assemble-session:" lang))
                (progn
                  (sort
                   (cdr (assoc lang hax/org-session-langs-blocks-alist))
                   (lambda (x y) (< (alist-get :idx x)
                                    (alist-get :idx y))))))))
          (assq-delete-all :session header-args)
          (assq-delete-all :idx header-args)
          ;; (pprint full-body)
          (setq eval-results
                (funcall (intern (concat "org-babel-execute:" lang))
                         full-body
                         header-args))

          (setq result
                (funcall (intern (concat "org-babel-disassemble-session:" lang))
                         hax/org-babel-eval-results
                         block-idx))
          (setq hax/org-babel-eval-results nil)
          (org-babel-insert-src-eval-result header-args result info))))))

(advice-add
 'org-babel-execute-src-block
 :around
 'hax/org-babel-execute-advice)

(advice-add
 'org-babel-execute:nim
 :around
 (lambda (f &rest args)
   (let ((body (nth 0 args))
         (params (nth 1 args)))
     ;; (print "body")
     ;; (print body)
     ;; (print "params")
     ;; (print params)
     (funcall f body params))
   )
 )

(defun main()
  (setq org-babel-default-header-args:nim
        `((:import . "macros")
          (:flags . "--hints:off --verbosity:0 --cc:tcc")
          (:results . "output")))

  (org-babel-do-load-languages 'org-babel-load-languages '((nim . t)))
  (setq org-confirm-babel-evaluate (lambda (l b) nil))

  (when t
    (with-temp-buffer
      (insert "
#+begin_src nim :session test :idx 1
proc test() = echo 12
echo \"idx 1\"
#+end_src

#+begin_src nim :session test :idx 2
test()
#+end_src
")
      (org-mode)
      (goto-char 0)
      (message (buffer-string))
      (re-search-forward (rx "#+begin_src"))
      (org-ctrl-c-ctrl-c)
      (re-search-forward (rx "#+begin_src"))
      (org-ctrl-c-ctrl-c)
      (message (buffer-string)))))

(provide 'ob-session)

(when nil
  (require 'ob-session)
  )
