(defun ++yas-prompt-confirm()
  "Prompt user to choose between `t' or `nil'"
  (yas-choose-value '(t nil)))

(defun ++yas-prompt-insert-if-positive (string)
  "Prompt user for yes/no input and return string if positive
  answer has been choosen. Otherwise return empty string"
  (if (++yas-prompt-confirm)
      (string)
    ("")))
