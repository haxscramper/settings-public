(require 'ob)

(defvar org-babel-default-header-args:pic
  '((:results . "file") (:exports . "results"))
  "Default arguments to use when evaluating a asymptote source block.")

(defun org-babel-prep-session:pic (_session _params)
  "Return an error because Asymptote does not support sessions."
  (error "Asymptote does not support sessions"))

(defmacro babel-file! (file)
  `(org-babel-process-file-name ,file))

(defun spc-concat (&rest args)
  (mapconcat 'identity args " "))

(defun org-babel-execute:pic (body headers)
  "Pass block BODY to the asy utility creating an image.
Specify the path at which the image should be saved as the first
element of headers, any additional elements of headers will be
passed to the asy utility as command line arguments. The default
output format is pdf, but you can specify any format supported by
Imagemagick convert program with '-f outformat'."
  (message "pic-formatting...")
  (let* ((out-file (cdr (or (assq :file headers)
			                      (error "You need to specify a :file parameter"))))
         (format (file-name-extension out-file))
         (args (cdr (assq :cmdline headers)))
         (pic-in-file (make-temp-file "org-pic"))
         (pdfcrop-in-file (make-temp-file "org-pdfcrop"))
         (gm-in-file (make-temp-file "org-gm-convert")))
    (with-temp-file pic-in-file
      (insert (format ".PS\n%s\n.PE\n" body)))
    (shell-command
     (spc-concat
      "pdfroff -p " (babel-file! pic-in-file)
      " > " (babel-file! pdfcrop-in-file)))
    (shell-command
     (spc-concat
      "pdfcrop " (babel-file! pdfcrop-in-file)
      " " (babel-file! gm-in-file)))
    (shell-command
     (spc-concat
      "gm convert -density 150"
      (babel-file! gm-in-file)
      (babel-file! out-file)))
    nil))

(provide 'ob-pic)
