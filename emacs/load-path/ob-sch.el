(require 'ob)

(defvar org-babel-default-header-args:sch
  '((:results . "file") (:exports . "results"))
  "Default arguments to use when evaluating a asymptote source block.")

(defun org-babel-prep-session:sch (_session _params)
  "Return an error because Asymptote does not support sessions."
  (error "Asymptote does not support sessions"))

(defmacro babel-file! (file)
  `(org-babel-process-file-name ,file))

(defun spc-concat (&rest args)
  (mapconcat 'identity args " "))

;; TODO compile asyncrhonously
(defun org-babel-execute:sch (body headers)
  "Pass block BODY to the asy utility creating an image.
Specify the path at which the image should be saved as the first
element of headers, any additional elements of headers will be
passed to the asy utility as command line arguments. The default
output format is pdf, but you can specify any format supported by
Imagemagick convert program with '-f outformat'."
  (message "sch-formatting...")
  (let* ((out-file (cdr (or (assq :file headers)
			                      (error "You need to specify a :file parameter"))))
         (format (file-name-extension out-file))
         (args (cdr (assq :cmdline headers)))
         (sch-in-file (make-temp-file "org-sch"))
         )
    (with-temp-file sch-in-file (insert body))
    (shell-command
     (spc-concat
      "schtex"
      (babel-file! sch-in-file)
      (babel-file! out-file)))
    nil))

(provide 'ob-sch)
