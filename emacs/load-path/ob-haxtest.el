(require 'ob)

(defun org-babel-prep-session:haxtest (_session _params)
  "Return an error because haxtest does not support sessions."
  (error "Asymptote does not support sessions"))

(defun org-babel-execute:haxtest (body headers)
  (message "haxtest-formatting...")
  (f-filename (buffer-file-name)))

(provide 'ob-haxtest)
