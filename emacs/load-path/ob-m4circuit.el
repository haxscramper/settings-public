(require 'ob)

(defvar org-babel-default-header-args:m4circuit
  '((:results . "file") (:exports . "results"))
  "Default arguments to use when evaluating a asymptote source block.")

(defun org-babel-prep-session:m4circuit (_session _params)
  "Return an error because m4circuit does not support sessions."
  (error "Asymptote does not support sessions"))

(defmacro babel-file! (file)
  `(org-babel-process-file-name ,file))

(defun spc-concat (&rest args)
  (mapconcat 'identity args " "))

;; TODO compile asyncrhonously
(defun org-babel-execute:m4circuit (body headers)
  "Pass block BODY to the asy utility creating an image.
Specify the path at which the image should be saved as the first
element of headers, any additional elements of headers will be
passed to the asy utility as command line arguments. The default
output format is pdf, but you can specify any format supported by
Imagemagick convert program with '-f outformat'."
  (message "m4circuit-formatting...")
  ;; TODO if there is no file use temporary one
  (let* ((file-arg (assq :file headers))
         ;; Volatile directory for temporary files (/tmp)
         (volatile-temp-directory "/tmp/org-babel/m4circuit/")
         ;; Non-volatile temp directory (only resulting file will be saved)
         (non-volatile-temp-directory (f-join (f-expand "~/.cache/org/")
                                              (f-filename (buffer-file-name))))
         ;; Full name of the output file
         (full-out-file (cond
                         (file-arg (cdr file-arg))
                         (t (progn
                              (make-directory volatile-temp-directory t)
                              (make-directory non-volatile-temp-directory t)
                              (make-temp-file non-volatile-temp-directory  nil ".png")))))
         ;; Name of the output file
         (out-file (cond
                    (file-arg (cdr file-arg))
                    ((assq :can-use-temporary headers)
                     (f-filename full-out-file))
                    (t (error "You need to specify a :file parameter or set
            :can-use-temporary"))))
         ;; Output format
         (format (file-name-extension out-file))
         ;; Additional command-line arguments to circuit generator
         (args (cdr (assq :cmdline headers)))
         ;; Save source body to this file
         (in-file (make-temp-file
                   (concat volatile-temp-directory "m4circuit_body")))
         ;; Temporary file to output generated file into.
         (tmp-out-file (make-temp-file volatile-temp-directory nil ".png"))
         ;; Name of the buffer to redirect debug information from
         ;; script execution
         (debug-output-buffer "*M4-circuit output*")
         )
    (with-temp-file in-file (insert (concat body "\n")))
    ;; TODO insert
    (exec-util "circuit.nim"
               (append
                (let ((args (cdr (assq :cmdline headers))))
                  (cond
                   ((stringp args) (list args))
                   ((listp args) args)
                   ((not args) nil)))
                (list "--debug"
                      (babel-file! in-file)
                      (babel-file! tmp-out-file)))
               :output-buffer debug-output-buffer)
    (message-to-buffer debug-output-buffer
                       "Generated file is %s\nTarget file is %s\nOriginal input file was %s"
                       tmp-out-file
                       full-out-file
                       in-file)
    (let ((res (copy-file tmp-out-file full-out-file t)))
      (message "Copy res: %s" res)
      (if file-arg
          res
        (progn
          (make-directory volatile-temp-directory t)
          (f-join volatile-temp-directory full-out-file))))
    ))

(provide 'ob-m4circuit)
