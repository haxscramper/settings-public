;;; Code:
(require 'ob)

(defvar org-babel-default-header-args:asy
  '((:results . "file") (:exports . "results"))
  "Default arguments to use when evaluating a asymptote source block.")

(defun org-babel-prep-session:asy (_session _params)
  "Return an error because Asymptote does not support sessions."
  (error "Asymptote does not support sessions"))

(defun org-babel-execute:asy (body headers)
  "Pass block BODY to the asy utility creating an image.
Specify the path at which the image should be saved as the first
element of headers, any additional elements of headers will be
passed to the asy utility as command line arguments. The default
output format is pdf, but you can specify any format supported by
Imagemagick convert program with '-f outformat'."
  (message "asy-formatting...")
  (let* ((out-file (cdr (or (assq :file headers)
			                      (error "You need to specify a :file parameter"))))
         (format (file-name-extension out-file))
         (args (cdr (assq :cmdline headers)))
         (data-file (make-temp-file "org-asy")))
    (let* ((asy-input-file (org-babel-process-file-name data-file))
           (asy-out-file (org-babel-process-file-name out-file))
           (asy-shell-command (concat
                               "asy -f " format
                               " -o " asy-out-file
                               " " args
                               " " asy-input-file)))
      (with-temp-file data-file (insert body))
      (org-babel-eval asy-shell-command "")
      nil)))

(provide 'ob-asy)
