;; CREDIT: https://github.com/qdot/org-gitbook

;; Utility functions for doing gitbook export with org-mode
;; Requires ox-gfm: https://github.com/larstvei/ox-gfm

(defvar org-gitbook-output-directory "./build"
  "Directory that org-gitbook will export files to")

;; Code adapted from:
;; export headlines to separate files
;; http://emacs.stackexchange.com/questions/2259/how-to-export-top-level-headings-of-org-mode-buffer-to-separate-files
(defun get-export-file-name ()
  (f-join
   org-gitbook-output-directory
   (concat
    (s-dashed-words
     (concat
      (org-format-outline-path (cdr (org-get-outline-path)))
      "/" (nth 4 (org-heading-components))))
    ".md")))

(defun org-export-gitbook ()
  "Export all subtrees that are *not* tagged with :noexport: to
  separate files.

  Subtrees that do not have the :EXPORT_FILE_NAME: property set
  are exported to a filename derived from the headline text."
  (interactive)
  (save-buffer)
  (save-excursion
    (org-map-entries
     (lambda ()
       (let ((heading-level (nth 0 (org-heading-components))))
         (when (>= 4 heading-level 1)
           (org-set-property "EXPORT_FILE_NAME"
                             (get-export-file-name))
           (org-gfm-export-to-markdown nil t)
           (org-delete-property "EXPORT_FILE_NAME"))))
     "-noexport" 'file))
  )


(defun org-build-gitbook-toc ()
  (interactive)
  (save-excursion
    (setq current-export-file "")
    (setq current-toc "")
    (org-map-entries
     (lambda ()
       (let ((heading-level (nth 0 (org-heading-components))))
         (if (> heading-level 1)
             (let ((prefix (make-string (* (- heading-level 2) 2) ? ))
                   (link-format (format
                                 "[%s](%s)"
                                 (nth 4 (org-heading-components))
                                 (get-export-file-name))))
               (setq current-toc
                     (concat current-toc
                             (format "%s* %s\n" prefix link-format)))))))
     "-noexport" 'file))
  current-toc)

(cl-defun org-save-summary (&optional (summary-file "SUMMARY.md"))
  (interactive)
  (let ((toc (org-build-gitbook-toc)))
    (with-temp-buffer (insert toc) (write-file summary-file)))
  )

(defun org-build-gitbook ()
  (interactive)
  (org-export-gitbook)
  (org-save-summary))

(provide 'org-gitbook)
