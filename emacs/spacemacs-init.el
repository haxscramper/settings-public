
(setq-default dotspacemacs-elpa-https t
              dotspacemacs-elpa-timeout 5
              dotspacemacs-check-for-update nil
              ;; If non-nil, a form that evaluates to a package directory. For example, to
              ;; use different package directories for different Emacs versions, set this
              ;; to `emacs-version'.
              dotspacemacs-elpa-subdirectory nil
              dotspacemacs-editing-style 'hybrid
              ;; If non nil output loading progress in `*Messages*' buffer. (default nil)

              dotspacemacs-startup-banner nil
              dotspacemacs-verbose-loading nil
              dotspacemacs-startup-lists '((recents . 5)
                                           (projects . 7))
              ;; True if the home buffer should respond to resize events.
              dotspacemacs-startup-buffer-responsive t
              dotspacemacs-scratch-mode 'text-mode
              dotspacemacs-themes '(spacemacs-dark spacemacs-light)
              ;; If non nil the cursor color matches the state color in GUI Emacs.
              dotspacemacs-colorize-cursor-according-to-state t
              ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
              ;; quickly tweak the mode-line size to make separators look not too crappy.
              dotspacemacs-default-font '(("JetBrains Mono"
                                           :size 18
                                           :weight normal
                                           :width normal
                                           :powerline-scale 1.2)
                                          ;; ("Fira Code"
                                          ;;  :size 18
                                          ;;  :weight normal
                                          ;;  :width normal)
                                          ;; ("Fira Code Symbol"
                                          ;;  :size 18
                                          ;;  :weight normal
                                          ;;  :width normal
                                          ;;  :powerline-scale 1.0)
                                          )
              ;; The leader key
              dotspacemacs-leader-key "SPC"
              ;; The key used for Emacs commands (M-x) (after pressing on the leader key).
              ;; (default "SPC")
              dotspacemacs-emacs-command-key "SPC"
              ;; The key used for Vim Ex commands (default ":")
              dotspacemacs-ex-command-key ":"
              ;; The leader key accessible in `emacs state' and `insert state'
              ;; (default "M-m")
              dotspacemacs-emacs-leader-key "M-m"
              ;; Major mode leader key is a shortcut key which is the equivalent of
              ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
              dotspacemacs-major-mode-leader-key ","
              ;; Major mode leader key accessible in `emacs state' and `insert state'.
              ;; (default "C-M-m")
              dotspacemacs-major-mode-emacs-leader-key "C-M-m"
              ;; These variables control whether separate commands are bound in the GUI to
              ;; the key pairs C-i, TAB and C-m, RET.
              ;; Setting it to a non-nil value, allows for separate commands under <C-i>
              ;; and TAB or <C-m> and RET.
              ;; In the terminal, these pairs are generally indistinguishable, so this only
              ;; works in the GUI. (default nil)
              dotspacemacs-distinguish-gui-tab nil
              ;; If non nil `Y' is remapped to `y$' in Evil states. (default nil)
              dotspacemacs-remap-Y-to-y$ nil
              ;; If non-nil, the shift mappings `<' and `>' retain visual state if used
              ;; there. (default t)
              dotspacemacs-retain-visual-state-on-shift t
              ;; If non-nil, J and K move lines up and down when in visual mode.
              ;; (default nil)
              dotspacemacs-visual-line-move-text nil dotspacemacs-ex-substitute-global nil
              ;; Name of the default layout (default "Default")
              dotspacemacs-default-layout-name "Default" dotspacemacs-display-default-layout nil
              ;; If non nil then the last auto saved layouts are resume automatically upon
              ;; start. (default nil)
              dotspacemacs-auto-resume-layouts nil dotspacemacs-large-file-size 1
              ;; Location where to auto-save files. Possible values are `original' to
              ;; auto-save the file in-place, `cache' to auto-save the file to another
              ;; file stored in the cache directory and `nil' to disable auto-saving.
              ;; (default 'cache)
              dotspacemacs-auto-save-file-location 'cache
              ;; Maximum number of rollback slots to keep in the cache. (default 5)
              dotspacemacs-max-rollback-slots 5
              ;; If non nil, `helm' will try to minimize the space it uses. (default nil)
              dotspacemacs-helm-resize nil
              ;; if non nil, the helm header is hidden when there is only one source.
              ;; (default nil)
              dotspacemacs-helm-no-header nil
              ;; define the position to display `helm', options are `bottom', `top',
              ;; `left', or `right'. (default 'bottom)
              dotspacemacs-helm-position 'bottom dotspacemacs-helm-use-fuzzy 'always
              ;; If non nil the paste micro-state is enabled. When enabled pressing `p`
              ;; several times cycle between the kill ring content. (default nil)
              dotspacemacs-enable-paste-transient-state nil
              ;; Which-key delay in seconds. The which-key buffer is the popup listing
              ;; the commands bound to the current keystroke sequence. (default 0.4)
              dotspacemacs-which-key-delay 0.4
              ;; Which-key frame position. Possible values are `right', `bottom' and
              ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
              ;; right; if there is insufficient space it displays it at the bottom.
              ;; (default 'bottom)
              dotspacemacs-which-key-position 'bottom
              ;; If non nil a progress bar is displayed when spacemacs is loading. This
              ;; may increase the boot time on some systems and emacs builds, set it to
              ;; nil to boost the loading time. (default t)
              dotspacemacs-loading-progress-bar t
              ;; If non nil the frame is fullscreen when Emacs starts up. (default nil)
              ;; (Emacs 24.4+ only)
              dotspacemacs-fullscreen-at-startup nil
              ;; If non nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
              ;; Use to disable fullscreen animations in OSX. (default nil)
              dotspacemacs-fullscreen-use-non-native nil
              ;; If non nil the frame is maximized when Emacs starts up.
              ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
              ;; (default nil) (Emacs 24.4+ only)
              dotspacemacs-maximized-at-startup nil
              ;; A value from the range (0..100), in increasing opacity, which describes
              ;; the transparency level of a frame when it's active or selected.
              ;; Transparency can be toggled through `toggle-transparency'. (default 90)
              dotspacemacs-active-transparency 90
              ;; A value from the range (0..100), in increasing opacity, which describes
              ;; the transparency level of a frame when it's inactive or deselected.
              ;; Transparency can be toggled through `toggle-transparency'. (default 90)
              dotspacemacs-inactive-transparency 90
              ;; If non nil show the titles of transient states. (default t)
              dotspacemacs-show-transient-state-title t
              ;; If non nil show the color guide hint for transient state keys. (default t)
              dotspacemacs-show-transient-state-color-guide t
              ;; If non nil unicode symbols are displayed in the mode line. (default t)
              dotspacemacs-mode-line-unicode-symbols nil
              dotspacemacs-smooth-scrolling t
              dotspacemacs-line-numbers 'visual
              dotspacemacs-folding-method 'origami
              dotspacemacs-smartparens-strict-mode nil
              dotspacemacs-smart-closing-parenthesis nil
              dotspacemacs-highlight-delimiters 'all
              dotspacemacs-persistent-server nil
              dotspacemacs-search-tools '("ag" "pt" "ack" "grep")
              dotspacemacs-default-package-repository nil
              dotspacemacs-whitespace-cleanup 'all
              dotspacemacs-mode-line-theme '(spacemacs :separator wave
                                                       :separator-scale 1.5))

(message "Loaded spacemacs-init.el")
