;;; Display Layer -*- lexical-binding: t; -*-

(setq display-packages
      '(;; Owned packages
        (prettify-utils :location (recipe :fetcher github
                                          :repo "Ilazki/prettify-utils.el"))
        which-key

        ;; Personal display-related packages
        (pretty-code     :location local)
        (pretty-fonts    :location local)
        (pretty-magit    :location local)))

;;;; All-the-icons-ivy

;;;; Prettify-utils

(defun display/init-prettify-utils ()
  (use-package prettify-utils))

;;;; Solarized-theme

(defun display/init-solarized-theme ()
  (use-package solarized-theme))

;;; Unowned Packages
;;;; Which-key

(defun display/post-init-which-key ()
  (when (configuration-layer/package-used-p 'pretty-fonts)
    (setq which-key-separator " ")
    (setq which-key-prefix-prefix "> ")))


;;; Pretty Packages
;;;; Pretty-code

(defun display/init-pretty-code ()
  (use-package pretty-code
    :config
    (progn
      (pretty-code-add-hook 'python-mode-hook     '((:lambda "lambda"))))))

;;;; Pretty-fonts
(defun display/init-pretty-fonts ()
  (use-package pretty-fonts
    :config
    ;; !! This is required to avoid segfault when using emacs as daemon !!
    (spacemacs|do-after-display-system-init
     ;(pretty-fonts-add-hook 'prog-mode-hook pretty-fonts-fira-code-alist)
     ;(pretty-fonts-add-hook 'org-mode-hook  pretty-fonts-fira-code-alist)

     ;; (pretty-fonts-set-fontsets-for-fira-code)
     ;; (pretty-fonts-set-fontsets
     ;;  '())
     )))

;;;; Pretty-magit

(defun display/init-pretty-magit ()
  (use-package pretty-magit
    :config
    (progn
      (pretty-magit-add-leaders
       '(("[FEATURE]"  'term-color-blue)
         ("[REFACTOR]" 'term-color-blue)
         ("[REVIEW]" 'term-color-blue)
         ("[FIX]"     'term-color-yellow)
         ("[SKIP]"     'term-color-red)
         ("[BREAK]"   'term-color-red)
         ("[TEST]"   'term-color-magenta)
         ("[POC]"   'term-color-green)
         ("[REPO]"   'term-color-red)
         ("[CLEAN]"   'term-color-blue)
         ("[STYLE]"   'term-color-blue)
         ("[CHECKPOINT]" 'term-color-yellow)
         ("[WIP]" 'term-color-blue)
         ("[DOC]"    'term-color-blue)))

      (pretty-magit-setup))))
