;; ;;; pretty-fonts.el --- Ligature and fontset setters -*- lexical-binding: t; -*prettify-symbols-alist.-

;; ;;; Commentary:

;; ;; A heavily annotated, cleaned-up version of ligature implementations for Emacs
;; ;; floating around on the web. If you ever looked at the snippets online and
;; ;; thought wtf is going on, this implementation should clear things up.

;; ;;; Code:
;; ;;;; Requires

;; (require 'dash)
;; (require 'dash-functional)

;; ;;;; Configuration
;; ;;;;; Fira-code Ligatures

;; ;; (
;; ;;  (defconst pretty-fonts-fira-code-alist
;; ;;    '(;; OPERATORS
;; ;;      ;; Pipes
;; ;;      ("\\(<|\\)" #Xe14d) ("\\(<>\\)" #Xe15b) ("\\(<|>\\)" #Xe14e) ("\\(|>\\)" #Xe135)

;; ;;      ;; Brackets
;; ;;      ("\\(<\\*\\)" #Xe14b) ("\\(<\\*>\\)" #Xe14c) ("\\(\\*>\\)" #Xe104)
;; ;;      ("\\(<\\$\\)" #Xe14f) ("\\(<\\$>\\)" #Xe150) ("\\(\\$>\\)" #Xe137)
;; ;;      ("\\(<\\+\\)" #Xe155) ("\\(<\\+>\\)" #Xe156) ("\\(\\+>\\)" #Xe13a)

;; ;;      ;; Equality
;; ;;      ("\\(!==\\)"         #Xe10f) ("\\(===\\)"#Xe13d) ("\\(=/=\\)" #Xe143) ("\\(/==\\)"         #Xe12d)
;; ;;      ("\\(!=\\)" #Xe10e) ("\\(/=\\)" #Xe12c)
;; ;;      ("[^!/]\\(==\\)[^>]" #Xe13c)

;; ;;      ;; Equality Special
;; ;;      ("\\(||=\\)"  #Xe133) ("[^|]\\(|=\\)" #Xe134)
;; ;;      ("\\(~=\\)"   #Xe166)
;; ;;      ("\\(\\^=\\)" #Xe136)
;; ;;      ("\\(=:=\\)"  #Xe13b)

;; ;;      ;; Comparisons
;; ;;      ("\\(<=\\)" #Xe141) ("\\(>=\\)" #Xe145)
;; ;;      ("\\(</\\)" #Xe162) ("\\(</>\\)" #Xe163)

;; ;;      ;; Shifts
;; ;;      ("[^-=]\\(>>\\)" #Xe147) ("\\(>>>\\)" #Xe14a)
;; ;;      ("[^-=]\\(<<\\)" #Xe15c) ("\\(<<<\\)" #Xe15f)

;; ;;      ;; Dots
;; ;;      ("\\(\\.-\\)"    #Xe122) ("\\(\\.=\\)" #Xe123)
;; ;;      ("\\(\\.\\.<\\)" #Xe125)

;; ;;      ;; Hashes
;; ;;      ("\\(#{\\)"  #Xe119) ("\\(#(\\)"   #Xe11e) ("\\(#_\\)"   #Xe120)
;; ;;      ("\\(#_(\\)" #Xe121) ("\\(#\\?\\)" #Xe11f) ("\\(#\\[\\)" #Xe11a)

;; ;;      ;; REPEATED CHARACTERS
;; ;;      ;; 2-Repeats
;; ;;      ("\\(||\\)" #Xe132)
;; ;;      ("\\(!!\\)" #Xe10d)
;; ;;      ("\\(%%\\)" #Xe16a)
;; ;;      ("\\(&&\\)" #Xe131)

;; ;;      ;; 2+3-Repeats
;; ;;      ("\\(##\\)"       #Xe11b) ("\\(###\\)"         #Xe11c) ("\\(####\\)" #Xe11d)
;; ;;      ("\\(--\\)"       #Xe111) ("\\(---\\)"         #Xe112)
;; ;;      ("\\({-\\)"       #Xe108) ("\\(-}\\)"          #Xe110)
;; ;;      ("\\(\\\\\\\\\\)" #Xe106) ("\\(\\\\\\\\\\\\\\)" #Xe107)
;; ;;      ("\\(\\.\\.\\)"   #Xe124) ("\\(\\.\\.\\.\\)"   #Xe126)
;; ;;      ("\\(\\+\\+\\)"   #Xe138) ("\\(\\+\\+\\+\\)"   #Xe139)
;; ;;      ("\\(//\\)"       #Xe12f) ("\\(///\\)"         #Xe130)
;; ;;      ("\\(::\\)"       #Xe10a) ("\\(:::\\)"         #Xe10b)

;; ;;      ;; ARROWS
;; ;;      ;; Direct
;; ;;      ("[^-]\\(->\\)" #Xe114) ("[^=]\\(=>\\)" #Xe13f)
;; ;;      ("\\(<-\\)"     #Xe152)
;; ;;      ("\\(-->\\)"    #Xe113) ("\\(->>\\)"    #Xe115)
;; ;;      ("\\(==>\\)"    #Xe13e) ("\\(=>>\\)"    #Xe140)
;; ;;      ("\\(<--\\)"    #Xe153) ("\\(<<-\\)"    #Xe15d)
;; ;;      ("\\(<==\\)"    #Xe158) ("\\(<<=\\)"    #Xe15e)
;; ;;      ("\\(<->\\)"    #Xe154) ("\\(<=>\\)"    #Xe159)

;; ;;      ;; Branches
;; ;;      ("\\(-<\\)"  #Xe116) ("\\(-<<\\)" #Xe117)
;; ;;      ("\\(>-\\)"  #Xe144) ("\\(>>-\\)" #Xe148)
;; ;;      ("\\(=<<\\)" #Xe142) ("\\(>>=\\)" #Xe149)
;; ;;      ("\\(>=>\\)" #Xe146) ("\\(<=<\\)" #Xe15a)

;; ;;      ;; Squiggly
;; ;;      ("\\(<~\\)" #Xe160) ("\\(<~~\\)" #Xe161)
;; ;;      ("\\(~>\\)" #Xe167) ("\\(~~>\\)" #Xe169)
;; ;;      ("\\(-~\\)" #Xe118) ("\\(~-\\)"  #Xe165)

;; ;;      ;; MISC
;; ;;      ("\\(www\\)"                   #Xe100)
;; ;;      ("\\(<!--\\)"                  #Xe151)
;; ;;      ("\\(~@\\)"                    #Xe164)
;; ;;      ("[^<]\\(~~\\)"                #Xe168)
;; ;;      ("\\(\\?=\\)"                  #Xe127)
;; ;;      ("[^=]\\(:=\\)"                #Xe10c)
;; ;;      ("\\(/>\\)"                    #Xe12e)
;; ;;      ("[^\\+<>]\\(\\+\\)[^\\+<>]"   #Xe16d)
;; ;;      ("[^:=]\\(:\\)[^:=]"           #Xe16c)
;; ;;      ("\\(<=\\)"                    #Xe157))
;; ;;    "Fira font ligatures and their regexes")

;; ;; ;;;; Fontsetters

;; ;;  (defun pretty-fonts-set-fontsets (font-codepoints-alist)
;; ;;    "Set CODEPOINTS to use FONT in FONT-CODEPOINTS-ALIST in all situations."
;; ;;    (-each font-codepoints-alist
;; ;;      (-lambda ((font . codepoints))
;; ;;        (-each codepoints
;; ;;          (lambda (codepoint)
;; ;;            (set-fontset-font nil `(,codepoint . ,codepoint) font)
;; ;;            (set-fontset-font t `(,codepoint . ,codepoint) font))))))

;; ;; ;;;; Ligatures

;; ;;  (defun pretty-fonts--pad-codepoint (codepoint)
;; ;;    "Converts CODEPOINT to a string that `compose-region' will know to leftpad.

;; ;; A snippet from it's documentation:

;; ;;   If it is a string, the elements are alternate characters. In this case, TAB
;; ;;   element has a special meaning. If the first character is TAB, the glyphs are
;; ;;   displayed with left padding space so that no pixel overlaps with the previous
;; ;;   column. If the last character is TAB, the glyphs are displayed with right
;; ;;   padding space so that no pixel overlaps with the following column.

;; ;; Also note that prior implementations use `list' instead of `char-to-string',
;; ;; they do the same thing here but `char-to-string' is obviously more descriptive."
;; ;;    (concat "\t" (char-to-string codepoint)))

;; ;;  (defun pretty-fonts--build-keyword (rgx codepoint)
;; ;;    "Builds the font-lock-keyword for RGX to be composed to CODEPOINT.

;; ;; This function may seem obtuse. It can be translated into the spec
;; ;; defined in `font-lock-add-keywords' as follows:

;; ;;   (MATCHER . HIGHLIGHT=MATCH-HIGHLIGHT=(SUBEXP=0 FACENAME=expression))

;; ;; The FACENAME is a form that should evaluate to a face. In the case it returns
;; ;; nil, which we do here, it won't modify the face. If instead it was `(prog1
;; ;; font-lock-function-name-face compose...)', the composition would still be applied
;; ;; but now all ligatures would be highlighted as functions, for example."
;; ;;    `(,rgx (0 (prog1 nil
;; ;;                (compose-region (match-beginning 1)
;; ;;                                (match-end 1)
;; ;;                                ,(pretty-fonts--pad-codepoint codepoint))))))

;; ;;  (defun pretty-fonts-add-kwds (rgx-codepoint-alist)
;; ;;    "Exploits `font-lock-add-keywords' to transform RGXs into CODEPOINTs."
;; ;;    (->> rgx-codepoint-alist
;; ;;       (-map (-applify #'pretty-fonts--build-keyword))
;; ;;       (font-lock-add-keywords nil)))

;; ;;  (defun pretty-fonts-add-hook (hook rgx-codepoint-alist)
;; ;;    "Add `pretty-fonts-add-kwds' as a hook."
;; ;;    (add-hook hook
;; ;;              (lambda () (pretty-fonts-add-kwds rgx-codepoint-alist))))

;; ;;)

;; (defun pretty-fonts-set-fontsets-for-fira-code ()
;;   "Tell Emacs to render Fira Code codepoints using Fira Code Symbol font."
;;   (set-fontset-font t '(#Xe100 . #Xe16f) "Fira Code Symbol"))



;; (defconst fira-code-font-lock-keywords-alist
;;   (mapcar
;;    (lambda (regex-char-pair)
;;      `(,(car regex-char-pair)
;;        (0 (prog1 ()
;;             (compose-region (match-beginning 1)
;;                             (match-end 1)
;;                             ;; The first argument to concat is
;;                             ;; a string containing a literal
;;                             ;; tab
;;                             ,(concat "\t" (list (decode-char 'ucs (cadr regex-char-pair)))))))))
;;    '(("\\(www\\)"                   #Xe100)
;;      ("[^/]\\(\\*\\*\\)[^/]"        #Xe101)
;;      ("\\(\\*\\*\\*\\)"             #Xe102)
;;      ("\\(\\*\\*/\\)"               #Xe103)
;;      ("\\(\\*>\\)"                  #Xe104)
;;      ("[^*]\\(\\*/\\)"              #Xe105)
;;      ("\\(\\\\\\\\\\)"              #Xe106)
;;      ("\\(\\\\\\\\\\\\\\)"          #Xe107)
;;      ("\\({-\\)"                    #Xe108)
;;      ("\\(\\[\\]\\)"                #Xe109)
;;      ("\\(::\\)"                    #Xe10a)
;;      ("\\(:::\\)"                   #Xe10b)
;;      ("[^=]\\(:=\\)"                #Xe10c)
;;      ("\\(!!\\)"                    #Xe10d)
;;      ("\\(!=\\)"                    #Xe10e)
;;      ("\\(!==\\)"                   #Xe10f)
;;      ("\\(-}\\)"                    #Xe110)
;;      ("\\(--\\)"                    #Xe111)
;;      ("\\(---\\)"                   #Xe112)
;;      ("\\(-->\\)"                   #Xe113)
;;      ("[^-]\\(->\\)"                #Xe114)
;;      ("\\(->>\\)"                   #Xe115)
;;      ("\\(-<\\)"                    #Xe116)
;;      ("\\(-<<\\)"                   #Xe117)
;;      ("\\(-~\\)"                    #Xe118)
;;      ("\\(#{\\)"                    #Xe119)
;;      ("\\(#\\[\\)"                  #Xe11a)
;;      ("\\(##\\)"                    #Xe11b)
;;      ("\\(###\\)"                   #Xe11c)
;;      ("\\(####\\)"                  #Xe11d)
;;      ("\\(#(\\)"                    #Xe11e)
;;      ("\\(#\\?\\)"                  #Xe11f)
;;      ("\\(#_\\)"                    #Xe120)
;;      ("\\(#_(\\)"                   #Xe121)
;;      ("\\(\\.-\\)"                  #Xe122)
;;      ("\\(\\.=\\)"                  #Xe123)
;;      ("\\(\\.\\.\\)"                #Xe124)
;;      ("\\(\\.\\.<\\)"               #Xe125)
;;      ("\\(\\.\\.\\.\\)"             #Xe126)
;;      ("\\(\\?=\\)"                  #Xe127)
;;      ("\\(\\?\\?\\)"                #Xe128)
;;      ("\\(;;\\)"                    #Xe129)
;;      ("\\(/\\*\\)"                  #Xe12a)
;;      ("\\(/\\*\\*\\)"               #Xe12b)
;;      ("\\(/=\\)"                    #Xe12c)
;;      ("\\(/==\\)"                   #Xe12d)
;;      ("\\(/>\\)"                    #Xe12e)
;;      ("\\(//\\)"                    #Xe12f)
;;      ("\\(///\\)"                   #Xe130)
;;      ("\\(&&\\)"                    #Xe131)
;;      ("\\(||\\)"                    #Xe132)
;;      ("\\(||=\\)"                   #Xe133)
;;      ("[^|]\\(|=\\)"                #Xe134)
;;      ("\\(|>\\)"                    #Xe135)
;;      ("\\(\\^=\\)"                  #Xe136)
;;      ("\\(\\$>\\)"                  #Xe137)
;;      ("\\(\\+\\+\\)"                #Xe138)
;;      ("\\(\\+\\+\\+\\)"             #Xe139)
;;      ("\\(\\+>\\)"                  #Xe13a)
;;      ("\\(=:=\\)"                   #Xe13b)
;;      ("[^!/]\\(==\\)[^>]"           #Xe13c)
;;      ("\\(===\\)"                   #Xe13d)
;;      ("\\(==>\\)"                   #Xe13e)
;;      ("[^=]\\(=>\\)"                #Xe13f)
;;      ("\\(=>>\\)"                   #Xe140)
;;      ("\\(<=\\)"                    #Xe141)
;;      ("\\(=<<\\)"                   #Xe142)
;;      ("\\(=/=\\)"                   #Xe143)
;;      ("\\(>-\\)"                    #Xe144)
;;      ("\\(>=\\)"                    #Xe145)
;;      ("\\(>=>\\)"                   #Xe146)
;;      ("[^-=]\\(>>\\)"               #Xe147)
;;      ("\\(>>-\\)"                   #Xe148)
;;      ("\\(>>=\\)"                   #Xe149)
;;      ("\\(>>>\\)"                   #Xe14a)
;;      ("\\(<\\*\\)"                  #Xe14b)
;;      ("\\(<\\*>\\)"                 #Xe14c)
;;      ("\\(<|\\)"                    #Xe14d)
;;      ("\\(<|>\\)"                   #Xe14e)
;;      ("\\(<\\$\\)"                  #Xe14f)
;;      ("\\(<\\$>\\)"                 #Xe150)
;;      ("\\(<!--\\)"                  #Xe151)
;;      ("\\(<-\\)"                    #Xe152)
;;      ("\\(<--\\)"                   #Xe153)
;;      ("\\(<->\\)"                   #Xe154)
;;      ("\\(<\\+\\)"                  #Xe155)
;;      ("\\(<\\+>\\)"                 #Xe156)
;;      ("\\(<=\\)"                    #Xe157)
;;      ("\\(<==\\)"                   #Xe158)
;;      ("\\(<=>\\)"                   #Xe159)
;;      ("\\(<=<\\)"                   #Xe15a)
;;      ("\\(<>\\)"                    #Xe15b)
;;      ("[^-=]\\(<<\\)"               #Xe15c)
;;      ("\\(<<-\\)"                   #Xe15d)
;;      ("\\(<<=\\)"                   #Xe15e)
;;      ("\\(<<<\\)"                   #Xe15f)
;;      ("\\(<~\\)"                    #Xe160)
;;      ("\\(<~~\\)"                   #Xe161)
;;      ("\\(</\\)"                    #Xe162)
;;      ("\\(</>\\)"                   #Xe163)
;;      ("\\(~@\\)"                    #Xe164)
;;      ("\\(~-\\)"                    #Xe165)
;;      ("\\(~=\\)"                    #Xe166)
;;      ("\\(~>\\)"                    #Xe167)
;;      ("[^<]\\(~~\\)"                #Xe168)
;;      ("\\(~~>\\)"                   #Xe169)
;;      ("\\(%%\\)"                    #Xe16a)
;;      ("[0\[]\\(x\\)"                #Xe16b)
;;      ("[^:=]\\(:\\)[^:=]"           #Xe16c)
;;      ("[^\\+<>]\\(\\+\\)[^\\+<>]"   #Xe16d)
;;      ("[^\\*/<>]\\(\\*\\)[^\\*/<>]" #Xe16f))))

;; (defun add-fira-code-symbol-keywords ()
;;   (font-lock-add-keywords nil fira-code-font-lock-keywords-alist))

;; (add-hook 'prog-mode-hook
;;           #'add-fira-code-symbol-keywords)


;; (provide 'pretty-fonts)


(defun fira-code-mode--make-alist (list)
  "Generate prettify-symbols alist from LIST."
  (let ((idx -1))
    (mapcar
     (lambda (s)
       (setq idx (1+ idx))
       (let* ((code (+ #Xe100 idx))
	            (width (string-width s))
	            (prefix ())
	            (suffix '(?\s (Br . Br)))
	            (n 1))
	       (while (< n width)
	         (setq prefix (append prefix '(?\s (Br . Bl))))
	         (setq n (1+ n)))
	       (cons s (append prefix suffix (list (decode-char 'ucs code))))))
     list)))

(defconst fira-code-mode--ligatures
  '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\"
    "{-" "[]" "::" ":::" ":=" "!!" "!=" "!==" "-}"
    "--" "---" "-->" "->" "->>" "-<" "-<<" "-~"
    "#{" "#[" "##" "###" "####" "#(" "#?" "#_" "#_("
    ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*"
    "/**" "/=" "/==" "/>" "//" "///" "&&" "||" "||="
    "|=" "|>" "^=" "$>" "++" "+++" "+>" "=:=" "=="
    "===" "==>" "=>" "=>>" "<=" "=<<" "=/=" ">-" ">="
    ">=>" ">>" ">>-" ">>=" ">>>" "<*" "<*>" "<|" "<|>"
    "<$" "<$>" "<!--" "<-" "<--" "<->" "<+" "<+>" "<="
    "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<" "<~"
    "<~~" "</" "</>" "~@" "~-" "~=" "~>" "~~" "~~>" "%%"
   ; "x"
    ; ":" "+" "+" "*"
    ))



(defvar fira-code-mode--old-prettify-alist)

(defun fira-code-mode--enable ()
  "Enable Fira Code ligatures in current buffer."
  (setq-local fira-code-mode--old-prettify-alist prettify-symbols-alist)
  (setq-local prettify-symbols-alist (append (fira-code-mode--make-alist fira-code-mode--ligatures) fira-code-mode--old-prettify-alist))
  (prettify-symbols-mode t))

(defun fira-code-mode--disable ()
  "Disable Fira Code ligatures in current buffer."
  (setq-local prettify-symbols-alist fira-code-mode--old-prettify-alist)
  (prettify-symbols-mode -1))

(define-minor-mode fira-code-mode
  "Fira Code ligatures minor mode"
  :lighter " FiraCode"
  (setq-local prettify-symbols-unprettify-at-point 'right-edge)
  (if fira-code-mode
      (fira-code-mode--enable)
    (fira-code-mode--disable)))

(defun fira-code-mode--setup ()
  "Setup Fira Code Symbols"
  (set-fontset-font t '(#Xe100 . #Xe16f) "Fira Code Symbol"))

(provide 'pretty-fonts)


(provide 'fira-code-mode)
