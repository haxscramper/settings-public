;;; pretty-code.el --- Utils for prettify-symbols -*- lexical-binding: t; -*-

;;; Commentary:
;; Utility to centralize prettify-symbol replacements.

;;; Code:
;;;; Utils

(require 'dash)
(require 'prettify-utils)

;;;; Configuration

;;; TODO Add latex ligatures
(defvar pretty-code-options-alist
  ;; Functions
  '(
    (:int "ℤ") (:double "ℝ")
    (:for "∀") (:in "∈") (:not-in "∉") ; Seqs
    (:not "￢") (:and "∧") (:or "∨") ; Flow
    (:return "⟼") (:yield "⟻") (:some "∃") (:composition "∘") (:tuple "⨂") ; Misc
    )
  "kwd and composition-str alist.")

;;;; Core

;;;###autoload
(defun pretty-code-add-hook (hook kwd-name-alist)
  "Set `prettify-symbols-alist' for HOOK with choices in KWD-NAME-ALIST."
  (add-hook hook
            (lambda ()
              (setq prettify-symbols-alist
                    (->> kwd-name-alist
                       (-map (-lambda ((kwd name))
                               (cons name
                                     (alist-get kwd pretty-code-options-alist))))
                       (apply #'prettify-utils-generate-f)))
              (prettify-symbols-mode 1))))

(provide 'pretty-code)
