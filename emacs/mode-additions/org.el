;;;#== Set evaluation rules for certain languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   ;; (prolog . t)
   ;; (ipython . t)
   (plantuml . t)
   (haxtest . t)
   (sqlite . t)
   (C . t)
   (lua . t)
   (dot . t)
   (lisp . t)
   (latex . t)
   (sch . t)
   (asy . t)
   (nim . t)
   (pic . t)
   ;; (jupyter . t)
   (shell . t)
   (python . t)
   (m4circuit . t)
   (ditaa . t)))

(setq org-babel-prolog-command "swipl")
(require 'ob-C)

(add-to-list 'org-modules 'org-habit  t)
(add-to-list 'org-modules 'org-depend t)
(add-to-list 'org-modules 'org-special-block t)
(setq org-src-fontify-natively t)
(setq  org-ditaa-jar-path
       "/mnt/defaultdirs/transient/software/ditaa0_9.jar")

(add-to-list 'org-latex-packages-alist
             '("" "tikz" t))

(eval-after-load "preview"
  '(add-to-list 'preview-default-preamble
                "\\PreviewEnvironment{tikzpicture}" t))

(setq org-latex-create-formula-image-program 'imagemagick)

(setq org-plantuml-jar-path (f-join hax/file::software-dir "plantuml.jar"))

;;; TODO add wrapper functions for adding languages
(defun hax/org-confirm-babel-evaluate (lang body)
  "Only confirm evaluation of the languages not listed here"
  nil
  ;; (not (or (string= lang "dot")
  ;;          (string= lang "asy")
  ;;          (string= lang "pic")
  ;;          (string= lang "c++")
  ;;          (string= lang "plantuml")
  ;;          (string= lang "sch")
  ;;          (string= lang "lua")
  ;;          (string= lang "shell")
  ;;          (string= lang "lisp")
  ;;          (string= lang "sh")
  ;;          (string= lang "elisp")
  ;;          (string= lang "nim")
  ;;          (string= lang "jupyter-python")
  ;;          (string= lang "jupyter")
  ;;          (string= lang "ipython")
  ;;          (string= lang "m4circuit")
  ;;          (string= lang "ditaa")))
  )

(setq org-confirm-babel-evaluate 'hax/org-confirm-babel-evaluate)
(setq org-babel-default-header-args:sh
      `((:exports . "both")
        (:results . "output")
        (:eval . "no-export")))


(setq org-babel-default-header-args:lua
      `((:results . "output replace")
        (:exports . "both")))

(setq org-babel-default-header-args
      `((:session . "none")
        (:results . "replace")
        (:exports . "code")
        (:cache . "no")
        (:noweb . "no")
        (:eval . "no-export")
        (:hlines . "no")
        (:tangle . "no")))

(setq org-babel-default-header-args:nim
      `((:import . "macros")
        (:flags . "--hints:off --verbosity:0 --cc:tcc")
        (:results . "output")))


(add-to-list 'org-src-lang-modes (quote ("dot" . graphviz-dot)))

(defun org-latex-export-to-pdf-async()
  (interactive)
  (org-latex-export-to-pdf t))

(defun org-insert-todo-subheading-respect-content (arg)
  "Insert subheading after current one, add todo keyword and go
to insert mode"
  (interactive "p")
  (evil-org-org-insert-todo-heading-respect-content-below)
  (org-do-demote)
  ;; (org-todo "TODO")
  )


;; if someone know a way to just unmap this fucking piece of bugged
;; degenerate, womit-inducing, slow, useless piece of retardede shit I
;; would much apprechiate the help.
(map! :nv "S-k" nil)
(defun helm-man-woman ())
(defun woman ())

;; (org-defkey org-mode-map
;;             (kbd "M-S-<return>")
;;             #'org-insert-next-todo-heading)

;; (org-defkey org-mode-map
;;             (kbd "ESC S-<return>")
;;             #'org-insert-next-todo-heading)

;; XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX
;; TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
;; MAKE SURE THAT WHEN I INSERT NEW TASK IT HAS 'TODO' KEYWORD
;; XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX
;; TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
;; MAKE SURE THAT WHEN I INSERT NEW TASK IT HAS 'TODO' KEYWORD
;; XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX
;; TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
;; MAKE SURE THAT WHEN I INSERT NEW TASK IT HAS 'TODO' KEYWORD
;; XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX
;; TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
;; MAKE SURE THAT WHEN I INSERT NEW TASK IT HAS 'TODO' KEYWORD
;; XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX
;; TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
;; MAKE SURE THAT WHEN I INSERT NEW TASK IT HAS 'TODO' KEYWORD
;; XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX
;; TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
;; MAKE SURE THAT WHEN I INSERT NEW TASK IT HAS 'TODO' KEYWORD
;; XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX
;; TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
;; MAKE SURE THAT WHEN I INSERT NEW TASK IT HAS 'TODO' KEYWORD
;; XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX
;; TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
;; MAKE SURE THAT WHEN I INSERT NEW TASK IT HAS 'TODO' KEYWORD
(setq org-insert-heading-hook nil)
(add-hook 'org-insert-heading-hook
          #'(lambda()
              (save-excursion
                ;; (message "Start region is [%d %d]"
                ;;          (line-beginning-position)
                ;;          (line-end-position))
                ;; (let* ((line (buffer-substring (line-beginning-position)
                ;;                                (line-end-position)))
                ;;        (replaced
                ;;         (replace-regexp-in-string
                ;;          (rx (and
                ;;               (group (1+ (any "*"))))
                ;;              space (1+ word) (0+ anything))
                ;;          "\\1 TODO" line)))
                ;;   (message "Input line is [%s]" line)
                ;;   (message "Replaced line is [%s]" replaced)
                ;;   (message "Region is [%d %d]"
                ;;            (line-beginning-position)
                ;;            (line-end-position)
                ;;            )
                ;;   (delete-region (line-beginning-position)
                ;;                  (line-end-position))
                ;;   (insert replaced))
                (org-back-to-heading)
                (org-expiry-insert-created)
                )
              ))

;; (defun org-insert-next-todo-heading ()
;;   (interactive)
;;   (org-insert-heading-respect-content)
;;   (org-todo "TODO")
;;   (org-back-to-heading)
;;   (org-expiry-insert-created)
;;   (evil-end-of-line)
;;   (evil-insert-state)
;;   (insert " "))



(map! :map org-mode-map
      :nv ",eh" 'org-html-export-to-html
      :nv ",ep" 'org-latex-export-to-pdf-async
      :nv ",ea" 'org-ascii-export-to-ascii
      :nv ",eP" 'org-latex-export-to-pdf
      :nv ",el" 'org-latex-export-to-latex
      :nv "<M-up>" 'org-move-item-up
      :nv "<M-down>" 'org-move-item-down
      :nv "<S-M-up>" 'org-move-subtree-up
      :nv "<S-M-down>" 'org-move-subtree-down
      :nv "<backtab>" nil
      :nv ",s" (lambda () (org-edit-src-save))
      :nv ",iT" 'org-insert-todo-subheading-respect-content
      :nv ",ic" 'org-insert-heading-created-timestant
      :nv ",isD" (make-snippet-callback "[[https://discord.gg/hjfYJCU][discord server]]")
      :nv ",ml" 'org-autolist-mode
      ;; TODO FIXME does not work
      :n "C-k" '++mc-make-cursor-above-move-next-line
      :n "C-j" '++mc-make-cursor-below-move-prev-line
      )

(E-mmap! org-mode-map "C-i"
         (lambda () (interactive)
           (goto-char (point-max))
           (insert (format-time-string "\n\n** @time:%H:%M;\n\n"))
           (org-expiry-insert-created)
           (evil-insert-state)))


(defun hax/do-nothing() (interactive))

(cl-loop for face in (face-list) do
         (unless (eq face 'default)
           (set-face-attribute face nil :height 1.0)))

(global-visual-line-mode 1)

(defun hax/org-mode-hook()
  (interactive)
  (setq word-wrap t)
  (visual-line-mode 1)
  (org-autolist-mode)
  (SPC-map! "SPC" 'replace-next-placeholder)
  ;; (E-mmap! org-mode-map "<M-S-return>" 'org-insert-next-todo-heading)
  ;; (E-mmap! org-mode-map "<C-S-return>" 'org-insert-next-todo-heading)
  (define-key org-mode-map [?\S-\t] nil)
  (visual-line-mode 1)
  (add-to-list 'company-backends 'company-org-block)
  (company-mode +1)
  (map! :map org-mode-map :nv ",L" 'org-insert-next-link)
  ;; (hl-todo-mode)
  ;; (add-hook 'post-command-hook 'kk/org-latex-fragment-toggle t)
  )

;; FIXME
;; (defun hax/org-insert-todo-heading-more-data()
;;   "Insert new todo heading and add as much auto-generated
;;   properties as possible"
;;   (interactive)
;;   (org-insert-todo-heading "TODO")
;;   (org-expiry-insert-created))

;; (defmacro map-mode! (mode-map binding function)
;;   `(define-key ,mode-map (kbd ,binding) ,function))

;; (map-mode! org-mode-map "<C-M-return>" 'hax/org-insert-todo-heading-more-data)


(setq org-hierarchical-todo-statistics nil)

(defun org-update-all-statistics-cookies()
  (interactive)
  (let ((current-prefix-arg '(4)))
    (org-update-statistics-cookies "ALL")))

(map! ;; FIXME
 :map org-mode-map
 :nv ",#" 'org-update-all-statistics-cookies
 :nv ",&" 'org-cycle
 :nv ",ic" 'org-expiry-insert-created
 :nv ",Ta" 'outline-show-all
 :nv ",hi" 'org-indent-mode
 )

;; (E-mmap! org-mode-map "`" (lambda () (interactive) (insert "~")))

;; - ~@~ for a note with timestamp
;; - ~!~ for a timestamp
;; - left side of slash means when changed *into* the state, right side
;;   means when changed *from* the state

;; TODO write function to define keyword and face at the same time
(setq org-todo-keywords
      '((sequence
         "TODO(t!/!)"
         "LATER(l!)"
         "NEXT(n!)"
         "POSTPONED(P!/!)"
         "WIP(w!)"
         "STALLED(s!)"
         "REVIEW(r!/!)"
         "TIMEOUT(T)"
         "FAILED(f@/!)"
         "CANCELED(C@/!)"
         "|"
         "DONE(d!/@)"
         "COMPLETED(c!/@)"
         "NUKED(N@/!)"
         "PARTIALLY(p@/!)"
         "FUCKING___DONE(F)"
         )))

(setq org-todo-keyword-faces
      '(
        ("TODO" . "orange")
        ("LATER" . "DeepPink")
        ("NEXT" . "DarkRed")
        ("POSTPONED" . "coral")
        ("DONE" . "green")
        ("NUKED" . "purple")
        ("CANCELED" . "snow")
        ("PARTIALLY" . "goldenrod")
        ("WIP" . "tan")
        ("STALLED" . "gold")
        ("REVIEW" . "SteelBlue")
        ("FAILED" . "red")
        ("FUCKING___DONE" . "gold1")
        ("TIMEOUT" . "red")
        ))

(setq org-global-properties
      '(("Effort_ALL" .
         "0:10 0:30 1:00 2:00 4:00 1d 2d 3d 1w 2w 1m 2m 3m 5m 1y 100y"
         )))

(defun leuven--org-ask-effort ()
  "Ask for an effort estimate when clocking in."
  (interactive)
  (unless (org-entry-get (point) "Effort")
    (let ((effort
           (completing-read
            "Estimated time (H:MM): "
            (org-entry-get-multivalued-property (point) "Effort"))))
      (unless (equal effort "")
        (org-set-property "Effort" effort)))))

;; (setq
;;  org-tag-alist
;;  '((:startgrouptag) ("time-est")
;;    (:grouptags) ("5_10_min") ("10_30_min") (:endgrouptag)
;;    )
;;  )

;; TODO write explanation for each tag type (necessity)
(setq org-log-reschedule 'note)
(setq org-log-states-order-reversed nil)
(setq org-log-redeadline 'note)
(setq org-log-into-drawer t)

(defun org-open-daliy-note ()
  (interactive)
  (find-file
   (binutil-to-string
    "get-daily-note --update-symlink --mod-file")))

(defun org-open-weekly-note ()
  (interactive)
  (find-file
   (binutil-to-string "get-daily-note --mod-file --period:week")))

(defun org-open-monthly-note ()
  (interactive)
  (find-file
   (binutil-to-string "get-daily-note --mod-file --period:month")))

(defun org-open-daily-note-no-edit ()
  (interactive)
  (find-file
   (binutil-to-string
    "get-daily-note")))

(defun toggle-daily-note-no-edit ()
  (interactive)
  (if (string=
       (buffer-file-name (current-buffer))
       (binutil-to-string "get-daily-note"))
      (previous-buffer)
    (org-open-daily-note-no-edit)))

(E-gmap! "<f6>" 'toggle-daily-note-no-edit)


(SPC-map! "and" 'org-open-daliy-note)
(SPC-map! "anw" 'org-open-weekly-note)
(SPC-map! "anm" 'org-open-monthly-note)
(SPC-map! "aow" 'cfw:open-org-calendar)
(SPC-map! "anD" 'org-open-daily-note-no-edit)

(E-mmap! org-mode-map
         "<S-tab>" #'org-cycle
         )



;; TODO move faces into `hax/face::*'
(setq org-priority-faces
      '((?A . (:foreground
               "red"
               :background "black"
               :weight bold
               :box '(:line-width 1)))
        (?B . (:foreground
               "orange"
               :background "black"
               :weight bold
               :box '(:line-width 1)))
        (?C . (:foreground
               "green"
               :background "black"
               :weight bold
               :box '(:line-width 1)))))

(add-hook 'org-mode-hook 'hax/org-mode-hook)


(add-to-load-path "~/.config/hax-config/external/org-mode/lisp")

(require 'package)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

(require 'ob-exp)

(setq org-startup-folded nil)

(setq org-directory "~/defaultdirs/personal/org")
(defvar hax/file::org-dir org-directory
  "Should be equal to org-directory. Defined for naming
  consistency, prefer this over shortened version")



(setq org-agenda-files
      (append
       (mapcar (lambda (file) (f-join hax/file::org-dir file))
               '("repeated.org" "daily.org" "university.org" "projects-now.org"))
       (list
        ;; (concat hax/file::config-dir "/todo.org") ;; FIXME find out why ~f-join~ does not work
        ;; (concat hax/file::workspace-dir "/hax-nim/todo.org")
        ;; (concat hax/file::workspace-dir "/hack/todo.org")
        (concat hax/file::defaultdirs-dir "/personal/notes/daily/today.org")
        )
       ;; TODO Add todo items from bookmarks file (managed exteranlly)
       ))

;; https://www.reddit.com/r/emacs/comments/4366f9/how_do_orgrefiletargets_work/
(setq
 org-outline-path-complete-in-steps nil
 org-refile-use-outline-path 'file
 org-refile-allow-creating-parent-nodes 'confirm)

(setq
 org-refile-targets
 `((nil :maxlevel . 3)
   (,(f-join hax/file::defaultdirs-dir "personal/notes/daily/today.org")
    :maxlevel . 2)
   (,(f-join hax/file::defaultdirs-dir "personal/org/daily.org") :maxlevel . 1)
   (,(f-join hax/file::defaultdirs-dir
             "personal/org/personal-projects.org") :maxlevel . 4)
   (,(f-join hax/file::defaultdirs-dir "personal/org/university.org") :maxlevel . 3)
   (,(f-join hax/file::defaultdirs-dir "personal/org/projects-now.org")
    :maxlevel . 1)
   ))


(fset 'latex-surround-bold-math [?v ?i ?$ ?s ?\{ ?i ?\\ ?b ?m escape])

(fset 'edit::surround::make-line-bold [?^ ?v ?$ ?l ?s ?* escape])
(fset 'edit::surround::make-line-italic [?^ ?v ?$ ?l ?s ?/ escape])
(fset 'edit::surround::make-line-underline [?^ ?v ?$ ?l ?s ?_ escape])


(map!
 :map org-mode-map
 :nv ",mB" 'edit::surround::make-line-bold
 :nv ",mI" 'edit::surround::make-line-italic
 :nv ",mU" 'edit::surround::make-line-underline
 :nv "<f8>" 'org-latex-preview
 :nv "<f7>" 'org-toggle-inline-images
 )




(setq org-agenda-span 10
      org-agenda-start-on-weekday nil
      org-agenda-start-day "-3d"
      org-agenda-skip-scheduled-if-done t
      )

(setq org-agenda-custom-commands
      '(("t" "Todo items" agenda ""
         ((org-agenda-skip-function
           '(org-agenda-skip-entry-if 'todo 'done)))
         ("~/computer.html"))
        ))

(setq org-default-notes-file (concat org-directory "/default-notes.org"))

(setq
 org-capture-templates
 '(("t" "Todo" entry
    (file+headline org-default-notes-file "Tasks")
    "** TODO %?\n   [%U]")
   ("n" "Note" entry
    (file+headline org-default-notes-file "Notes")
    "** #note %? \n")
   ("e" "Error"
    (file+headline org-default-notes-file "Errors and issues")
    "** TODO #fixme %? \n")
   ;; TODO Template for defining tasks for next several days
   ;; (auto-geenrate prompt date, ask for time (or event after
   ;; which this one should be placed))
   ))


(setq calendar-week-start-day 1)

(defvar hax/internal::tag-body-regex
  '(1+ (any "_" "-" "A-Z" "a-z" "0-9")))

(defvar hax/internal::tag-regex
  (rx
   ;; (or string-start bol " ")
   (group
    "#" (eval hax/internal::tag-body-regex) ;; Tag head
    (0+ ;; Subtags
     (and "##" ;; subtag start
          (or (eval hax/internal::tag-body-regex) ;; Single subtag
              (and ;; Multiple subtags
               "["
               (0+ (and (eval hax/internal::tag-body-regex) ","))
               (eval hax/internal::tag-body-regex)
               "]")))))
   ;; (or string-end eol " ")
   ))

;; NOTE testing for org-mode regex tag
(--map (message it)
       (let ((test
              '(
                "#aaa"
                "#eee##eee"
                "#eeee##[eee]"
                "#eeee##[eee,sdfs]"
                "#idea##[latex,nim]"
                "#sdf##"
                "eee"
                "sdfsf##sdf"
                " #sdf##sd"
                "##sdf##dd"
                "eee##ee"
                "#project##qdelib "
                "#code##[nim,cpp]"
                )))
         (mapcar
          (lambda (str)
            (concat str
                    (if (string-match-p hax/internal::tag-regex str)
                        " ok" " fail"))
            )
          test
          )
         ))

(defvar
  rx::latex-inline-dollar
  (rx (or (and "$" (not (any " ")) (1+ (not (any "$"))) "$")
          (and "$" (** 1 2 (not (any " "))) "$")))
  "Regular expression to match inline latex formula that uses
  dollar sign (\"$\") as delimiter")

(defface org/inline-commands-face
  `((t
     :foreground "dark orange"
     :underline t
     :extend t))
  "Inline commands"
  :group 'hax/face::group-misc)

(font-lock-replace-keywords
 'org-mode
 `( ;; TODO replace with emacs regular expression (there is a function
   ;; (org a macro?) that allows to write them in more readable for)
   (,(rx (group "@"
                (+? (not (any ":"))) ":"
                (not (any " "))
                (+? (not (any ":")))
                (not (any " " ";"))
                ";"))
    . 'hax/face::simple::bold-light-blue)
   (
    ,(rx (and "#" (or "fixed" "idea" "todo")
              (* "##" (+ (or word "-")))
              )) .
    'hax/face::simple::bold-green)
   ;; ,(rx-to-string FIXME
   ;;   (rx (and "(" (+ digit) ")")) .
   ;;   'hax/face::simple::bold-dark-green)
   (,(rx "[#A]") . 'hax/face::boxed::red-bold-boxed)
   (,(rx "[#B]") . 'hax/face::boxed::orange-bold-boxed)
   (,(rx "[#C]") . 'hax/face::boxed::green-bold-boxed)
   ;; '("@todo" (optional
   ;;            "["
   ;;            (eval hax/internal::tag-regex)
   ;;            (0+ (and "," (eval hax/internal::tag-regex)))
   ;;            "]")
   ;;   (0+ anything) ";")
   ("\\(@todo\\[.+?\\]:\\)" . 'org-level-8)
   ("\\(@idea\\[.+?\\]:\\)" . 'org-level-8)
   (,(rx (and
          "@"
          (1+ word)
          (1+ (and "{" (+? anything) "}")))) .
          'org/inline-commands-face)

   ("\\(\\*wip:\\*\\)" . 'hax/face::simple::bold-light-blue)
   ("\\(\\*fix:\\*\\)" . 'hax/face::simple::bold-green)
   ("\\(\\*solved:\\*\\)" . 'hax/face::simple::bold-dark-green)
   ("\\(\\*note:\\*\\)" . 'hax/face::simple::bold-orange)
   ("\\(\\*warning:\\*\\)" . 'hax/face::simple::bold-orange)
   ("\\(\\*error:\\*\\)" . 'hax/face::simple::bold-red)
   (,(rx (group (or "NOTE" "TIP" "IMPORTANT" "WARNING" "CAUTION") ":")) .
    'hax/face::boxed::mangenta-with-pink-box)
   (,(rx
      space
      (group "|" (not (any " ")) (+? anything) (not (any " ")) "|")
      space) . 'hax/face::highlight::code)


   ;; ("\\(|.+?|\\)" . 'hax/face::highlight::code)
   ;; ("\\(~.+|.+?|\\)" . 'org-verbatim) # TODO inline code with
   ;; arguments QUESTION Is it possible to have single regex with
   ;; several capture groups each of which generates different colors?
   ;; If it is true I also need to generate different highlighting for
   ;; hastags and metatags.
   (,(rx (group (>= 5 "-") eol)) . 'org-verbatim)
   (,rx::latex-inline-dollar . 'org-formula)
   (,(rx "\\(" (group (+? anything)) "\\)") . 'tex-math)
   (,(rx "\\[" (group (+? anything)) "\\]") . 'tex-math)
   ;; FIXME
   ;; ("^\s*\\+ \\(\\[ \\]\\)". 'org-todo)
   ;; ("^\s*\\+ \\(\\[X\\]\\)". 'org-done)
   ("^\s*\\(+\\)" . 'org-level-3)
   ("(\\?)" . 'hax/face::boxed::orange-bold-boxed)

   ;; Questions about text
   (,(rx (group "(?" (+? anything) "?)")) . 'hax/face::simple::regular-green)

   ;; ;; Go-to placeholders
   ;; (,(rx (group "<++>")) . 'hax/face::simple::regular-green)
   ;; ;; Go-to placeholders with text inside
   ;; (,(rx (group "<+" (+? anything) "+>")) . 'hax/face::simple::regular-green)
   ;; ;; Placeholders that will be shown in final text
   ;; (,(rx (group "<??" (+? anything) "??>")) . 'hax/face::simple::regular-green)
   ;; ;; Simpler placehoders
   ;; (,(rx (group "<" (+? anything) ">")) . 'hax/face::simple::regular-green)

   ;; Pause
   (,(rx (group (>= 4 ".")) eol) . 'hax/face::simple::regular-green)

   ;; IDEA Highlighting of simple latex primitives in the capture?
   ;; things like ~\\\w+~ should be relatively easy to implement (this
   ;; is capture for simple macro such as ~\log, \text~ etc.)
   ("\\( `.+?`\\)" . 'org-verbatim)

   ("\\(@~.+?~\\)" . 'dired-perm-write)
   (,(rx (any " ([<")
         (group
          (1+ word)
          "~"
          (not (any space))
          (+? (not (any "~")))
          (not (any space))
          "~")
         (any " )]>.?"))

    ;; "\\([a-z]+~.+?~\\)"
    1 'hax/face::underlined::green t
    )

   ;; Commentaries
   (,(rx
      (group "@[" (not (any " ")) (+? anything) (not (any " ")) "]")
      ) . 'font-lock-comment-face)
   ;; Inline properties
   ("\\(!\\[.+?\\]\\)" . 'hax/face::simple::bold-green)
   ;; FIXME tags
   (;; TODO account for subtag list #tag##[sub1,sub2]
    ;; TODO use rx instead of writing regex manually
    ,hax/internal::tag-regex . 'hax/face::highlight::tag)
   (,(rx "src_" (+ word)) . 'hax/face::highlight::code)
   ;; TODO Higlight org-priorities faces on checklists

   ))

(setq org-image-actual-width (list 300))
(setq org-startup-with-inline-images nil)
(setq org-adapt-indentation t)
(setq org-indent-indentation-per-level 6)

;; TODO if images are not displayed do not enable them
(defun org-redisplay-inline-images ()
  "toggle image display off and on"
  (interactive)
  (org-remove-inline-images)
  (org-toggle-inline-images))


;; TODO instead of overriding save previous state and enable images
;; only if they were previously enabled
(cl-defun org-add-image-actual-width
    (&optional
     (increment 200)
     (update-display t))
  "Change `org-image-actual-width' and update images. This will
change display width of all inline images in current buffer. If
display of inline images is turned off it will be turned on"
  (if (listp org-image-actual-width)
      (setq
       org-image-actual-width
       (list (+ (car org-image-actual-width) increment)))
    (setq org-image-actual-width (+ org-image-actual-width increment)))
  (when update-display (org-redisplay-inline-images)))

;;;;;;;;  highlight some constructs using in regular text writing  ;;;;;;;;

(defvar rx::after-word-regex (rx (| space "." ":" ";")))

(defvar writing-highlight-keywords
  `(
    (,(rx
       space
       (group "\"" (not (any " ")) (*? anything) (not (any " ")) "\"")
       (eval rx::after-word-regex)) . 'hax/face::highlight::code)


    (,(rx
       space
       (group "'" (not (any " ")) (*? anything) (not (any " ")) "'")
       space) . 'hax/face::simple::bold-dark-green)

    (,(rx (group (+? (any "A-Z")) ":")) . 'hax/face::simple::regular-blue)
    ))

(eval-when-compile
  (defvar writing-highlight-warning-mode nil))

(define-minor-mode writing-highlight-warning-mode
  ""
  :lighter " ---"
  (if writing-highlight-warning-mode
      (font-lock-add-keywords nil writing-highlight-keywords 'append)
    (font-lock-remove-keywords nil writing-highlight-keywords))
  (when font-lock-mode
    (font-lock-flush)))


(map!
 :map org-mode-map

 :desc "images"
 :nv ",I" nil

 :desc "width-related"
 :nv ",Iw" nil

 :desc "inline img width +200"
 :nv ",Iw+" (lambda () (interactive) (org-add-image-actual-width 200))

 :desc "inline img width +200"
 :nv ",Iw-" (lambda () (interactive) (org-add-image-actual-width -200))

 :desc "use width variable"
 :nv ",Iwv"
 (lambda ()
   (interactive)
   (when (listp org-image-actual-width)
     (progn
       (setq org-image-actual-width (car org-image-actual-width))
       (org-redisplay-inline-images))))

 :desc "use properties"
 :nv ",Iwp"
 (lambda ()
   (interactive)
   (unless (listp org-image-actual-width)
     (progn
       (setq org-image-actual-width
             (list org-image-actual-width))
       (org-redisplay-inline-images)))))

;; TODO move into separate shell wrappers file
(defun async-shell-w-callback
    (command callback &optional output-buffer error-buffer)
  "Run asynchronous shell command and add callback to process'
state change (`set-process-sentinel'). If `output-buffer' is
`nil' new buffer with name `Async shell command' will be created.
Function returns `t' if async process started succesfully and
`nil' otherwise.

Callback must accepts two arguments: `process' and `signal' -
process and string describing the change. Some possible values
are 'finished', 'deleted' etc. For list of possible signal values
refer to documentation on \"Sentinels: Detecting Process Status
Changes\" in emacs manual"
  (let* ((output-buffer
          (if output-buffer
              output-buffer
            (generate-new-buffer "Async shell command")))
         (proc
          (progn
            (async-shell-command command output-buffer error-buffer)
            (get-buffer-process output-buffer))))
    (if (process-live-p proc)
        (progn (set-process-sentinel proc callback) t)
      nil)))


;; TODO use closure or whatever instead of global variables
(defvar
  hax/internal::xclip-paste-out-buf nil
  "temprorary variable to hold buffer for async shell output when
  pasting command")

(defvar
  hax/internal::xclip-paste-callback-func nil
  "temporary variable to hold callback function for calling fater
  xclip pasting has succeded")

(cl-defun get-buffer-content (&optional buffer)
  "Get content of the buffer as string"
  (if buffer
      (with-current-buffer buffer (buffer-string))
    (buffer-string)))

;; TODO check if target file exits

;; TODO save to temporary file immediately and show error if clipboard
;; does not contain image

(defun save-x-clipboard-to-file
    (image-path paste-completed-callback)
  "Save current content of x clipboard to file, all output will
be written to"
  (let ((out-buf (generate-new-buffer "xclip paste tmp buffer" )))
    (setq hax/internal::xclip-paste-out-buf out-buf)
    (setq hax/internal::xclip-paste-callback-func paste-completed-callback)
    (async-shell-w-callback
     (format
      "sh -c \"xclip -sel cli -t image/png -o > '%s' \""
      image-path)
     (lambda
       (process signal)
       (progn
         (message "paste completed")
         (message signal)
         (unless (string-match signal "finished")
           (let ((res (message
                       (get-buffer-content
                        hax/internal::xclip-paste-out-buf))))
             (message res)
             (unless (string-match "Error:" res)
               (funcall hax/internal::xclip-paste-callback-func))))
         (kill-buffer hax/internal::xclip-paste-out-buf)))
     out-buf) ))

(defun kill-buffer-always (buffer)
  "Kill buffer regardless of modifcation state, running processes etc."
  (with-current-buffer buffer
    (let ((kill-buffer-query-functions nil))
      (kill-buffer buffer))))


(defvar hax/internal::insert-clipboard-image-text nil)

(defun insert-x-cliboard-image-as-text (image-path insertion-text)
  "Save content of the clipboard as image and if no errors were
  reported (cliboard contains image and it can be saved) insert
  `insertion-text'. NOTE: insertion of the text is done using
  callback function and is not done immediately (since `xclip' is
  called internally for image pasting and has to be called
  asynchronously)"
  (setq hax/internal::insert-clipboard-image-text insertion-text)
  (save-x-clipboard-to-file
   image-path
   (lambda () (insert hax/internal::insert-clipboard-image-text))))

(defun org-insert-x-clipboard-image (image-path)
  "Insert content of the clipboard as org-mode image. File will
be saved in the same directory as current buffer"
  ;; TODO check for image in clipboard *before* asking to enter file
  (interactive "Fpath:")
  (when image-path
    ;; FIXME no not show buffer on success/error.

    ;; TODO If anything is written to error buffer do not insert
    ;; anythint to org-mode buffer and show error in modeline
    (let ((relative-path
           (f-relative image-path (f-dirname (buffer-file-name)))))
      (insert-x-cliboard-image-as-text
       image-path
       (format "[[file:%s]]" relative-path)))))

;; TODO add support for reformatting into '<description>:
;; [[<link-url>][link]]'



;; TODO detect whether or not it is necessary to move cursor before
;; inserting text to avoid badly formatted links without spaces around
;; them or too much spaces.
(cl-defun org-insert-clipboard-link (link-name &optional (add-spacing t))
  (interactive "slink name: ")
  (let ((run-again t))
    (while run-again
      (when link-name
        (let* ((org-link (format
                          (concat (if add-spacing " " "") "[[%s][%s]]")
                          (simpleclip-get-contents)
                          link-name))
               (read-answer-short t)
               (selected (read-answer
                          (format "Insert '%s" org-link)
                          '(("yes" ?y "insert link")
                            ("no" ?n "do not insert link")
                            ("update" ?u "update link from clipboard")
                            ("help" ?h "show help")
                            ("quit" ?q "quit")
                            ("edit" ?e "prompt for new link name")
                            ))))
          (cond
           ((string= selected "yes") (insert org-link) (setq run-again nil))
           ((string= selected "no") (setq run-again nil))
           ((string= selected "update") t)
           ((string= selected "help") (setq run-again nil))
           ((string= selected "edit") (setq link-name
                                            (read-string "New link name: ")))
           ((string= selected "quit") (setq run-again nil))
           )
          )))))

(defun org-insert-next-link (description)
  (interactive "slink description: ")
  (goto-char (line-end-position))
  (org-meta-return)
  (org-insert-clipboard-link "link" nil)
  (goto-char (line-end-position))
  (insert "*" description "*. "))

(map!
 :map org-mode-map
 :nv ",iI" 'org-insert-x-clipboard-image
 :nv ",l" 'org-insert-clipboard-link
 :nv ",L" 'org-insert-next-link
 )

(map!
 :map spacemacs-org-mode-map
 :ni ",il" 'org-insert-clipboard-link ;; FIXME does not work
 )

(setq org-html-head-include-default-style t)

(when (not org-html-head-include-default-style)
  (setq org-html-head
        (f-read-text
         (f-join hax/file::config-dir "emacs/org_html.html"))))


(setq org-latex-create-formula-image-program 'dvipng)
(setq org-format-latex-options (plist-put org-format-latex-options :scale 1.5))
(add-to-list 'org-latex-packages-alist '("" "hax_latex_macro_simple" t))
(add-to-list 'org-latex-packages-alist '("" "latex_help" t))
(add-to-list 'org-latex-packages-alist '("" "newpxmath" t))
(auto-insert-mode 1)
(eval-after-load 'autoinsert
  '(define-auto-insert
     '("\\.org\\'" . "org-mode setupfile")
     '("org-mode setupfile"
       "#+setupfile: ~/.config/hax-config/emacs/latex-export-header.org"
       )
     )
  )

;; TODO it technically works but needs some updates
;; (defun my/org-render-latex-fragments ()
;;   (if (org--list-latex-overlays)
;;       (progn (org-toggle-latex-fragment)
;;              (org-toggle-latex-fragment))
;;     (org-toggle-latex-fragment)))

;; (add-hook 'org-mode-hook
;;           (lambda ()
;;             (add-hook
;;              'after-save-hook
;;              'my/org-render-latex-fragments
;;              nil
;;              'make-the-hook-local)))

;;;(setq 'org-startup-folded nil)

;;;#== Defining

(defun hax/format-from-dynalist (input-string)
  (with-temp-buffer
    (insert input-string)
    (fill-region (point-min) (point-max))
    (insert (let ((str (buffer-string)))
              (erase-buffer)
              (replace-regexp-in-string
               (rx ;; bol
                (group (= 2 digit)) ":"
                (group (= 2 digit)) (* space)
                ;; (group (* (not (any "\n"))))
                ;; eol
                )
               "\n\n** @time:\\1:\\2;\n\\3\n"
               str)))
    (buffer-string)))

(defun insert-formatted-dynalist ()
  (interactive)
  (let* ((input-string (simpleclip-get-contents))
         (output-string (hax/format-from-dynalist input-string)))
    ;; (message input-string)
    ;; (message "Formatted string:")
    ;; (message output-string)
    (insert output-string)))

(defun insert-nim-src ()
  (interactive)
  (insert "#+begin_src nim\n")
  (save-excursion (insert "\n#+end_src"))
  (evil-insert-state))

(map! :map org-mode-map
      :n ",iN" 'insert-nim-src)

(defvar hax/internal::ascii-text-insert-started nil)

(defun ascii-insert-activated? () hax/internal::ascii-text-insert-started)

(defun start-ascii-text-insert ()
  (if hax/internal::ascii-text-insert-started
      (message "already started acsii insert")
    (setq hax/internal::previous-input-method current-input-method)
    (when current-input-method (deactivate-input-method))
    (set-input-method nil)
    (setq hax/internal::ascii-text-insert-started t)))

(defun end-ascii-text-insert ()
  (if  (not hax/internal::ascii-text-insert-started)
      (message "not in ascii insert")
    (activate-input-method hax/internal::previous-input-method)
    (setq hax/internal::previous-input-method nil)
    (setq hax/internal::ascii-text-insert-started nil)))

(cl-defun transition-ascii-insert-state
    (&key
     call-on-state-enter
     call-on-state-leave
     call-on-transition-request)
  (when call-on-transition-request (funcall call-on-transition-request))
  (cond ((ascii-insert-activated?)
         (end-ascii-text-insert)
         (when call-on-state-leave (funcall call-on-state-leave)))
        ((not (ascii-insert-activated?))
         (when call-on-state-enter (funcall call-on-state-enter))
         (start-ascii-text-insert)))
  nil)


(defun transition-ascii-insert-state-wrapped (start-string leave-string)
  (transition-ascii-insert-state
   :call-on-transition-request (lambda () (message "state-transition"))
   :call-on-state-enter (lambda () (insert start-string))
   :call-on-state-leave (lambda () (insert leave-string))))

(map! :map org-mode-map
      :desc "start/end inline latex insert"
      :ni "<M-l>" (lambda ()
                    (message "hello")
                    (interactive)
                    (transition-ascii-insert-state-wrapped "$" "$")))

;;;;;;;;;;;;;;;;;;;;;  use '>' as list character too  ;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;  preview generated latex in zathura  ;;;;;;;;;;;;;;;;;;;

(defun org-preview-pdf ()
  "Open current buffer's pdf file in zathura"
  (interactive)
  (let* ((file (f-full (buffer-file-name)))
         (basename (s-chop-suffix "org" file))
         (pdf-path (concat basename "pdf"))
         (command (concat "zathura " pdf-path " &")))
    (message "running-command '%s'" command)
    (call-process-shell-command command nil 0)
    )
  )

(map! :map org-mode-map
      :n ",v" 'org-preview-pdf)

(defvar hax/internal::math-insert-state nil "t/nil - whether or
not user is inserting math")

(defmacro map-snippet! (mode chord snippet-name)
  "Bind key sequence `chord' as normal-mode shortcut for `mode'
to lambda that inserts `snippet-name'.

For example `(map-snippet! org-mode-map \",isd\"
\"org-mode/latex-definition\")' will create shortcut `,isd' for
org-mode map. This shortcut inserts `org-mode/latex-definitions'
snippet."
  `(map! :map ,mode
         :desc ,snippet-name
         :n ,chord
         (lambda () (interactive)
           (insert-snippet-by-name ,snippet-name))))

(map-snippet! org-mode-map ",isd" "org-mode/latex-definition")
(map-snippet! org-mode-map ",islc" "org-mode/latex-cases")

(E-mmap! org-mode-map "<print>" (lambda () (interactive) (insert "_")))

(when t
  (defun make-wrap-yasnippet (key expansion)
    (define-yasnippet key expansion :expand-env '((yas-wrap-around-region nil))))

  (defun yas-expand-snippet-here (snippet)
    (yas-expand-snippet snippet nil nil (yas--template-expand-env snippet)))

  (defmacro make-wrap-snippet-callback (snippet-key snippet-expansion)
    `(lambda () (interactive)
       (yas-expand-snippet-here (make-wrap-yasnippet ,snippet-key ,snippet-expansion))))

  ;; (map! :map emacs-lisp-mode-map
  ;;       :nv ",i&"
  ;;       (lambda ()
  ;;         (interactive)
  ;;         (yas-expand-snippet-here (make-wrap-yasnippet "__" "&`yas-selected-text`&"))))

  ;; (with-temp-buffer
  ;;   (yas-minor-mode)
  ;;   (yas-expand-snippet-here (make-wrap-yasnippet "__" "&`yas-selected-text`&"))
  ;;   (buffer-string))
  )


(map! :map org-mode-map

      :desc "wrap in `_{\text{<>}}'"
      :nv ",wt"
      (make-wrap-snippet-callback "$" "_{\\text{`yas-selected-text`}}")

      :desc "wrap in \\( \\)"
      :nv ",w4"
      (make-wrap-snippet-callback "\\(\\)" "\\\\( `yas-selected-text` \\\\)")

      :desc "Insert latex theorem"
      ;; :n ",is" nil
      :n ",ist" (lambda () (interactive)
                  (insert-snippet-by-name "org-mode/latex-theorem"))

      :desc "Insert latex definition"
      ;; :n ",is" nil
      :n ",isd" (lambda () (interactive)
                  (insert-snippet-by-name "org-mode/latex-definition"))

      :desc "Insert latex export"
      :nv ",ise" (lambda () (interactive)
                   (insert-snippet-by-name "org-mode/latex-equation"))

      :desc "Insert latex formula"
      :nv ",isf" (lambda () (interactive)
                   (insert-snippet-by-name "org-mode/latex-formula"))


      :desc "Insert source code block"
      :nv ",ic" (lambda () (interactive)
                  (insert-snippet-by-name "org-mode/begin-src"))

      :desc "Insert image"
      :n ",isi" (lambda () (interactive)
                  (insert-snippet-by-name "org-mode/latex-export-image"))


      :desc "subscript text"
      :n ",islt" (lambda () (interactive)
                   (insert-snippet-by-name "org-mode/latex-underscore-text"))


      :desc "insert code"
      :n "<f12>"
      (lambda (text)
        (interactive "scode: ")
        (goto-char (line-end-position))
        (insert (concat " ~" (s-trim text) "~ ")))
      ;; (lambda () (interactive)
      ;;   (if hax/internal::math-insert-state
      ;;       (progn
      ;;         (toggle-input-method)
      ;;         (goto-char (line-end-position))
      ;;         (setq hax/internal::math-insert-state nil))
      ;;     (progn (goto-char (line-end-position))
      ;;            (when current-input-method (deactivate-input-method))
      ;;            (when (eq evil-state 'normal) (evil-insert-state))
      ;;            (insert " $")
      ;;            (save-excursion (insert "$ "))
      ;;            (setq hax/internal::math-insert-state t))))
      )

(setq org-babel-inline-result-wrap "%s")

(E-mmap!
 org-mode-map
 "<f9>" (lambda (text)
          (interactive "smath: ")
          (message "asda")
          (insert (concat "\\(" (s-trim text) "\\) "))))


(E-mmap!
 org-mode-map
 "<f11>" (lambda (text)
          (interactive "smath: ")
          (message "asda")
          (insert (concat "~" (s-trim text) "~"))))

(defun insert-other-lang()
  "Read input string in the non-default (`toggle-input-method')
language."
  (interactive)
  (add-hook 'minibuffer-setup-hook 'toggle-input-method)
  (insert
   (unwind-protect (read-from-minibuffer "?> ")
     (remove-hook 'minibuffer-setup-hook 'toggle-input-method))))

(E-mmap!
 org-mode-map
 "C-SPC" (lambda ()
           (interactive)
           (search-backward "{}" (- (point) 120))
           (forward-char)
           (insert-other-lang)))


(E-mmap! org-mode-map "<f10>" 'insert-other-lang)

(spacemacs/set-leader-keys-for-major-mode 'org-mode "is" nil)
(spacemacs/declare-prefix-for-mode 'org-mode "is" "insert-snippet")
(spacemacs/declare-prefix-for-mode 'org-mode "isl" "latex-snippet")

;; (spacemacs/set-leader-keys-for-major-mode
;;   'org-mode
;;   "ist" (lambda () (interactive)
;;         (insert-snippet-by-name "org-mode/latex-theorem"))
;;   )




;; (setq org-export-latex-listings 'minted)
;; (add-to-list 'org-latex-packages-alist '("" "minted" t))

;; ;; Add the shell-escape flag
;; (setq
;;  org-latex-pdf-process
;;  '(
;;    "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
;;    ;; "bibtex %b"
;;    "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
;;    "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
;;    ))

;; Sample minted options.
(setq
 org-latex-minted-options
 '(
   ;; ("breaklines" "true")
   ;; ("linenos" "")
   ;; ("showtabs" "")
   ))

(setq org-latex-listings 'minted
      org-latex-packages-alist '(("" "minted"))
      org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;  langtool-check  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(spacemacs/set-leader-keys-for-major-mode 'org-mode "'" nil)
(spacemacs/declare-prefix-for-mode 'org-mode "'" "---")
(spacemacs/declare-prefix-for-mode 'org-mode "'l" "langtool")

(define-minor-mode hax/langtool-mode "langtool mode"
  :keymap (make-sparse-keymap)
  )

(map! :map org-mode-map
      :nv "'lc" 'langtool-check
      :nv "'lf" 'langtool-correct-buffer
      :nv "'ld" 'langtool-check-done
      )

(defun langtool-check-start ()
  "Start langtool check and init `hax/langtool-mode'"
  (interactive)
  (langtool-check)
  (hax/langtool-mode 1))

(defun langtool-check-stop ()
  "Stop langtool check and disable `hax/langtool-mode'"
  (interactive)
  (langtool-check-done)
  (hax/langtool-mode -1)
  )

(map! :map org-mode-map
      :nv "'lc" 'langtool-check-start
      :nv "'lf" 'langtool-correct-buffer
      :nv "'ld" 'langtool-check-stop
      )


(map! :map hax/langtool-mode-map
      :nv "C-n" 'langtool-goto-next-error
      :nv "C-p" 'langtool-goto-previous-error)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  odt export  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq org-latex-to-mathml-convert-command
      "latexmlmath \"%i\" --presentationmathml=%o")


;;;;;;;;;;;;;;;;;;;;;;  include files inside range  ;;;;;;;;;;;;;;;;;;;;;;;


(defun endless/update-includes (&rest ignore)
  "Update the line numbers of #+INCLUDE:s in current buffer.
Only looks at INCLUDEs that have either :range-begin or :range-end.
This function does nothing if not in org-mode, so you can safely
add it to `before-save-hook'."
  (interactive)
  (when (derived-mode-p 'org-mode)
    (save-excursion
      (goto-char (point-min))
      (while (search-forward-regexp
              "^\\s-*#\\+include: *\"\\([^\"]+\\)\".*:range-\\(begin\\|end\\)"
              nil 'noerror)
        (let* ((file (expand-file-name (match-string-no-properties 1)))
               lines begin end)
          (forward-line 0)
          (when (looking-at "^.*:range-begin *\"\\([^\"]+\\)\"")
            (setq begin (match-string-no-properties 1)))
          (when (looking-at "^.*:range-end *\"\\([^\"]+\\)\"")
            (setq end (match-string-no-properties 1)))
          (setq lines (endless/decide-line-range file begin end))
          (when lines
            (if (looking-at ".*:lines *\"\\([-0-9]+\\)\"")
                (replace-match lines :fixedcase :literal nil 1)
              (goto-char (line-end-position))
              (insert " :lines \"" lines "\""))))))))

(defun endless/decide-line-range (file begin end)
  "Visit FILE and decide which lines to include.
BEGIN and END are regexps which define the line range to use."
  (let (l r)
    (save-match-data
      (with-temp-buffer
        (insert-file file)
        (goto-char (point-min))
        (if (null begin)
            (setq l "")
          (search-forward-regexp begin)
          (setq l (line-number-at-pos (match-beginning 0))))
        (if (null end)
            (setq r "")
          (search-forward-regexp end)
          (setq r (1+ (line-number-at-pos (match-end 0)))))
        (format "%s-%s" l r)))))

(add-hook
 'org-mode-hook
 (lambda ()
   (add-hook 'before-save-hook #'endless/update-includes)))


;;;;;;;;;;;;;;;;;;;;  org-refile add 'from' location  ;;;;;;;;;;;;;;;;;;;;;
;; NOTE: adapted from https://emacs.stackexchange.com/questions/36390/add-original-location-of-refiled-entries-to-logbook-after-org-refile


;; do not use default refile logging
(setq org-log-refile nil)

;; add custom logging instead
(add-hook 'org-after-refile-insert-hook #'hax/org::org-refile-add-refiled-from-note)

(advice-add 'org-refile
            :before
            #'hax/org::org-save-source-id-and-header)

(defvar hax/org::org-refile-refiled-from-id nil)
(defvar hax/org::org-refile-refiled-from-header nil)

;; (defvar hax/internal::org-refile-refiled-from-params nil "Parameters of ")

(defun hax/org::org-save-source-id-and-header ()
  "Saves refile's source entry's id and header name to
`hax/org::org-refile-refiled-from-id' and
`hax/org::org-refile-refiled-from-header'. If refiling entry is
first level entry then it stores file path and buffer name
respectively."
  (interactive)
  (save-excursion
    (if (org-up-heading-safe)
        (progn
          (setq hax/org::org-refile-refiled-from-id (org-id-get nil t))
          (setq hax/org::org-refile-refiled-from-header
                (org-get-heading 'no-tags 'no-todo 'no-priority 'no-comment)))
      (setq hax/org::org-refile-refiled-from-id (buffer-file-name))
      (setq hax/org::org-refile-refiled-from-header (buffer-name)))))

;; IDEA add more complex log entries too: make this function accept
;; org-mode subtree in elisp form (parsed from org-elements)
(defun org-add-log-entry (text)
  "Add log entry for current subtree"
  (save-excursion
    (goto-char (- (org-log-beginning t) 1))
    (insert (concat "\n" (s-repeat (current-indentation) " ") "- " text))))

(defun hax/org::org-refile-add-refiled-from-note ()
  "Adds a note to entry at point on where the entry was refiled
from using the org ID from `hax/org::org-refile-refiled-from-id'
and `hax/org::org-refile-refiled-from-header' variables."
  (interactive)
  (when (and hax/org::org-refile-refiled-from-id
             hax/org::org-refile-refiled-from-header)
    (let* ((note-format "Refiled on [%s] from [[id:%s][%s]]")
           (time-format (substring (cdr org-time-stamp-formats) 1 -1))
           (time-stamp (format-time-string time-format (current-time))))
      (org-add-log-entry (format note-format
                                 time-stamp
                                 hax/org::org-refile-refiled-from-id
                                 hax/org::org-refile-refiled-from-header)))
    (setq hax/org::org-refile-refiled-from-id nil)
    (setq hax/org::org-refile-refiled-from-header nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;  pause/resume tasks  ;;;;;;;;;;;;;;;;;;;;;;;;;;;

(cl-defun toggle-task-postponed ()
  "Toggle task between 'POSTPONED' and 'WIP' status"
  (interactive)
  (let ((kwd (org-get-todo-state)))
    (cond
     ((string= kwd "WIP") (org-todo "POSTPONED"))
     ((string= kwd "POSTPONED") (org-todo "WIP")))))

(E-mmap! org-mode-map "C-M-p" 'toggle-task-postponed)


;;;;;;;;;;;;;;  Output compilation errors as regular result  ;;;;;;;;;;;;;;
(defvar org-babel-eval-verbose nil
  "A non-nil value makes `org-babel-eval' display")

(defvar hax/internal::nim-compile-result-tmp nil)


(defun replace-in-string (what with in)
  (replace-regexp-in-string (regexp-quote what) with in nil 'literal))

(when nil
  (defun org-babel-eval (cmd body)
    "Run CMD on BODY.
If CMD succeeds then return its results, otherwise display
STDERR with `org-babel-eval-error-notify'."
    (let ((err-buff (get-buffer-create " *Org-Babel Error result*")) exit-code)
      (with-current-buffer err-buff (erase-buffer))
      (with-temp-buffer
        (insert body)
        (setq exit-code
              (org-babel--shell-command-on-region
               (point-min) (point-max) cmd err-buff))

        ;; (concat
        ;;  (with-temp-buffer err-buff (buffer-string))
        ;;  (buffer-string)
        ;;  )
        ;; (message "out: %s -> [[%s]]" cmd (buffer-string))
        ;; (message "err: %s -> [[%s]]" cmd (get-buffer-content err-buff))
        (when (s-starts-with-p "nim" cmd)
          ;; (progn
          ;;   ;; (message "%s -> [[[%s]]]" cmd (buffer-string))
          (setq hax/internal::nim-compile-result-tmp
                (if (> exit-code 0)
                    (replace-regexp-in-string
                     (rx "/tmp/babel-" (*? any) "/nim_src" (*? any) ".nim")
                     ""
                     ;; "/tmp/babel-g77lkS/nim_src_dyPVrl.nim(4, 1) Error: undeclared identifier: 'dumpTree'"
                     (get-buffer-content err-buff)
                     )
                  (buffer-string))))

        (concat
         hax/internal::nim-compile-result-tmp
         (buffer-string))

        ;; (if (> exit-code 0)
        ;;     (get-buffer-content hax)
        ;;   ;; (with-current-buffer err-buff
        ;;   ;;   (buffer-string)
        ;;   ;;   ;; hax/internal::nim-compile-result-tmp
        ;;   ;;   ;; (when hax/internal::nim-compile-result-tmp
        ;;   ;;   ;;   (let ((tmp hax/internal::nim-compile-result-tmp))
        ;;   ;;   ;;     (setq hax/internal::nim-compile-result-tmp nil)
        ;;   ;;   ;;     (message "<%s>" tmp)
        ;;   ;;   ;;     tmp))
        ;;   ;;   )
        ;;   (concat
        ;;    hax/internal::nim-compile-result-tmp
        ;;    (buffer-string))
        ;;   )
        ;; (let ((tmp ))
        ;;   ;; (message "(%s)" tmp)
        ;;   tmp
        ;;   )


        ;; (if (or (not (numberp exit-code)) (> exit-code 0)
        ;;         (and org-babel-eval-verbose
        ;;              (> (buffer-size err-buff) 0))) ; new condition
        ;;     (progn
        ;;       (with-current-buffer err-buff
        ;;         (let ((str (buffer-string)))
        ;;           (message str)
        ;;           str
        ;;           )
        ;;         ;; (message (buffer-string))
        ;;         ;; ;; (org-babel-eval-error-notify exit-code (buffer-string))
        ;;         ;; (buffer-string)
        ;;         )
        ;;       ;; nil
        ;;       )
        ;;   (buffer-string))
        ))))

(message "export-pdf definition")
(defun export-pdf()
  (interactive)
  (org-latex-export-to-pdf)
  (message "%s" (get-buffer-content "*Org PDF LaTeX Output*")))


(defun org-babel-nim-execute (body params)
  "Overide for default org-babel execute for nim. Use `nim c -r'
and return both compiler output and regular text"
  (let* ((tmp-bin-file (org-babel-nim--sanitize-file-name
                        (org-babel-temp-file
                         "nim_src_" )))
         (tmp-src-file (concat tmp-bin-file
                               ".nim"))
         (cmdline (cdr (assoc :cmdline params)))
         (cmdline (if cmdline (concat " " cmdline) ""))
         (define (org-babel-read
                  (or (cdr (assoc :define params))
                      (org-entry-get nil "define" t))
                  nil))
         (define (if (stringp define) (split-string define " ") nil))
         (define (if define
                     (mapconcat
                      (lambda (inc) (format "-d:%s" inc)) define
                      " ")
                   ""))
         (flags (cdr (assoc :flags params)))
         (flags (mapconcat 'identity
                           (if (listp flags) flags (list flags)) " "))
         (libs (org-babel-read
                (or (cdr (assq :libs params))
                    (org-entry-get nil "libs" t))
                nil))
         (libs (mapconcat #'identity
                          (if (listp libs) libs (list libs))
                          " "))
         (full-body
          (org-babel-nim-expand-nim   body params)))
    (with-temp-file tmp-src-file (insert full-body))
    (org-babel-eval
     (format "nim c -r %s %s -o:%s %s %s"
             define
             flags
             (org-babel-process-file-name tmp-bin-file)
             (org-babel-process-file-name tmp-src-file)
             libs)
     "")))

(setq org-blank-before-new-entry '((heading . t) (plain-list-item . nil)))
