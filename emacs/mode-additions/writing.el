;;;#== Writing
(setq ispell-program-name "/usr/bin/aspell")
(setq flyspell-issue-message-flag nil)

(defun toggle-clean-write()
  (interactive)
  (if (bound-and-true-p writeroom-mode)
      (writeroom--disable)
    (writeroom-mode 1)))

(add-hook
 'text-mode-hook
 (lambda ()
   (map! :niv "<f12>" 'toggle-clean-write)))
