(setq font-latex-fontify-script nil)



(cl-defun tex-get-buffer-pdf (&optional buffer)
  "Return pdf file associated with buffer (current buffer is used
  if no argument is supplied)"
  (concat
   (f-base (if buffer (buffer-file-name buffer) (buffer-file-name)))
   ".pdf"))

;; FIXME Cannot generate correct name
;; TODO Hide output buffer if command is succesfully started
(defun zathura-open-latex ()
  "Open pdf file associated with current latex buffer"
  (interactive)
  (call-process-shell-command
   (concat "zathura " (tex-get-buffer-pdf) " &")
   nil
   0))

;; TODO use interactive option selector for page ranges, media format
;; and other cli options.
(defun lpr-print-latex ()
  "Use lpr to print file"
  (interactive)
  (let ((command (concat "lpr -o media=a4 " (tex-get-buffer-pdf) " &")))
    (when (yes-or-no-p (format "run printing using '%s' ?" command))
      (call-process-shell-command command nil 0))))

(defun hax/latex-format-buffer ()
  "`latex-format-buffer' + preserve position"
  (interactive)
  (let ((curr-line (line-number-at-pos)))
    (latex-format-buffer)
    (forward-line curr-line)))


;; FIXME does not work WIP code
(cl-defun surround-with-s-expression(&optional (first-argument nil))
  "Surround selection with s-expression braces and put
`first-argument' as, well, first one in the s-expression. If
`first-argument' is `equal' to `nil' it won't be used"
  (interactive))

(setq hax/internal::latex::image-insert-options '((nothing . t)))
(cl-defun hax/internal::change-optlist
    (optlist mod option &optional (value t))
  "Add/remove/change-value of the associative list element.
Mostly used for building configuration options as associative
lists."
  (cond
   ((eq mod :add)
    (progn
      (message
       (concat "Added " (symbol-name option)))
      (add-to-list optlist `(,option . ,value)
                   )))))


(defhydra
  hax/internal::latex::image-insert-options
  ( ;; IDEA define helper function for setting options in less
   ;; boilerplat-ish manner (writing
   ;; hax/internal::very-long-global-variable-name-so-everyone-knows-what-it-does)
   ;; each time I want to set options for inserting latex image is
   ;; not really interesting thing to do.
   :post
   (progn
     (message "selection finished")
     ;; TODO Save image insertion configuration between states if
     ;; ~hax/latex::image::preserve-inseet-config~ is set to true.
     (call-interactively 'latex-insert-x-clipboard-image)
     ;; TODO clean insertion options
     ))
  "Select different image modifiers for inserted image and then
press `ESC' to finish selecting. You will be prompted with
relative path for saving image file and after selecting file
*and* if there were no errors (i.e. clipboard contained
`image/png' and it was possible to write data into file) text
will be inserted
"
  ("c"
   (lambda () (interactive)
     (hax/internal::change-optlist
      'hax/internal::latex::image-insert-options :add :centered))
   "centered")
  )


(defun latex-insert-x-clipboard-image (image-path)
  "Insert content of the clipboard as includegraphics image. File
will be saved in selected path and relative path to file will be
used. This function does not provide options for customizing
insertion parameters (centered vs uncentered etc) but uses
`hax/internal::latex::image-insert-options' for reading
configuration. For interactively changing this variable see hydra
`hax/internal::latex::image-insert-options/body'"
  ;; IDEA show temporary buffer with interactive display of generated
  ;; latex code that changes when I use different modifiers in hydra

  ;; TODO check for correct cliboard content first, before displaying
  ;; any images. Check whether or not function was called
  ;; interactively and only then prompt for file.
  (interactive "Fpath:")
  (when image-path
    (let ((relative-path
           (f-relative image-path (f-dirname (buffer-file-name)))))
      (insert-x-cliboard-image-as-text
       image-path
       (let ((opts hax/internal::latex::image-insert-options))
         (print opts)
         (concat
          (when (assq :centered opts) "\\begin{center}\n")
          (format "\\includegraphics{%s}" relative-path)
          (when (assq :centered opts) "\n\\end{center}")
          ))))))

(defun ++yas-expand-snippet-by-name (snippet-name)
  (yas-expand-snippet (yas-lookup-snippet snippet-name)))



;; TODO Add support for creating snippets in-place from string
;; FIXME Does not work
; (defmacro SPC-map-snippet-name! (prefix snippet-name)
;   (SPC-map! prefix (lambda ()
;                      (interactive)
;                      (++yas-expand-snippet-by-name `( ,snippet-name )))))
;
; (defun hax/lisp-mode-hook()
;   (SPC-map-snippet-name! "Esl" "lisp-let-surround"))

(defun latex-format-buffer-file ()
  "Save current buffer and format the file"
  (interactive)
  (save-buffer)
  (let ((current-pos (point)))
    (shell-command-on-buffer-file "latex_indent_file.sh %s"))
  (revert-buffer-no-confirm))


(defun latex-format-buffer ()
  "Format current buffer"
  (interactive)
  (let ((current-pos (point)))
    (shell-command-on-buffer "latexindent")))


(defun LaTeX-env-with-preset (env pre post)
  (LaTeX-env-label env)
  (insert pre)
  (save-excursion (LaTeX-newline)
                  (insert post))
  (newline)
  (LaTeX-indent-line))



(defun hax/latex-mode-hook ()
  (interactive)
  ;;;#== Document viewers
  (setq TeX-view-program-list '(("zathura" "zathura %o")))
  (setq TeX-view-program-selection '((output-pdf "zathura")))
  (yasnippet-snippets-initialize)
  (highlight-parentheses-mode 1)
  (LaTeX-add-environments
   '("Remark" LaTeX-env-with-preset "" ""))
  (map!
   :n ", o" 'zathura-open-latex
   ;; :n ", = f" 'latex-format-buffer-file
   :n ", = B" (lambda ()
                (interactive)
                (latex-format-buffer-file)
                (latex/build))
   :n ",is" 'insert-comment-separator-medium
   )
  (E-mmap! LaTeX-mode-map "<C-return>" 'newline-and-indent-same-level)
  (E-mmap! LaTeX-mode-map "<C-M-return>"
           (lambda () (interactive)
             (newline-and-indent-same-level 2)))

  (SPC-map! "Esvb"
            (lambda ()
              (interactive)
              (++yas-expand-snippet-by-name
               "latex-math-surround-vector-bold"))))

(add-hook 'latex-mode-hook 'hax/latex-mode-hook)
(add-hook 'LaTeX-mode-hook 'hax/latex-mode-hook)



(setq TeX-master nil)
