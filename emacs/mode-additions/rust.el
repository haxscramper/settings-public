(defun shell-command-to-string-no-newline (command-string)
  (replace-regexp-in-string
   "\n\\'" ""
   (shell-command-to-string command-string)))

(setq racer-cmd (shell-command-to-string-no-newline "which racer"))

(setq racer-rust-src-path
      (concat
       (shell-command-to-string-no-newline "rustc --print sysroot")
       "/lib/rustlib/src/rust/src"))
