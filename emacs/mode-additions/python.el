(defun hax/python-mode-hook ()
  (python-docstring-mode 1)
  )


(map! :map python-mode-map
      :nv "M->" 'nim-indent-shift-right
      :nv "M-<" 'nim-indent-shift-left
      :desc "Insert docstring"
      :n ",id" (lambda () (interactive)
                 (indent-for-tab-command) (evil-insert-state)
                 (insert "\"\"\"")
                 (save-excursion (insert "\"\"\"")) ))

(add-hook 'python-mode-hook 'hax/python-mode-hook)
