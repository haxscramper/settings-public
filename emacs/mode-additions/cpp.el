(defmacro make-snippet-callback (string)
  `(lambda () (interactive)
     (insert-snippet-from-string ,string)
     (evil-insert-state)))


(defun hax/c++-mode-hook ()
  (interactive)
  ;; (yasnippet-snippets-initialize)
  (map! :map c++-mode-map
        :nv ", = b" 'clang-format-buffer
        :nv ",is" 'insert-cpp-section-comment
        :nv ",ie" 'insert-cpp-explanation-comment
        :desc "Template struct"
        :nv ",its" (make-snippet-callback
                    "\ntemplate <typename T>\nstruct $1 {\n\n};")

        :desc "Template using"
        :nv ",itu" (make-snippet-callback
                    "\ntemplate <typename T>\nusing $1 = $2;")

        )

  (message "Running hax/c++ mode hook")
  ;; WIP FIXME
  (remove-hook 'before-save-hook 'clang-format-buffer)
  (add-hook 'before-save-hook 'clang-format-buffer t t
            ;; (lambda ()
            ;;   (shell-command-to-string
            ;;    (format "clang-format -style=File %s" buffer-file-name)))
            )
  ;; (function-args-mode)
  ;; (helm-gtags-mode)
  ;;(SPC-map-major! 'c++-mode-map
  ;;  "j i" 'moo-jump-local
  ;;  "j t" 'helm-gtags-select)

  (highlight-lines-matching-regexp
   (rx "// --- " (*? anything) " --- //") 'font-lock-warning-face)

  (define-key c-mode-map (kbd "M-RET") 'srefactor-refactor-at-point)
  (define-key c++-mode-map (kbd "M-RET") 'srefactor-refactor-at-point)
  ;; (fira-code-mode)
  (message "Done hax/c++ mode hook")
  )


(defun hax/c-mode-hook ()
  (map! :map c-mode-map
        :nv ", = b" 'clang-format-buffer
        :nv ",is" 'insert-cpp-section-comment
        :nv ",ie" 'insert-cpp-explanation-comment

        )
  (3dash-warning-mode)
  )

(add-hook 'c-mode-hook 'hax/c-mode-hook)

;; (require 'generic-x)

;; (define-generic-mode 'c++-additional-comments
;;   nil
;;   nil
;;   `((,(rx "// --- " (*? anything) " --- //") . 'font-lock-warning-face))
;;   nil
;;   nil
;;   "A mode for foo files"
;;   )

(setq
 kwds
 `((
    ,(rx "// --- " (*? anything) " --- //") .
    'font-lock-warning-face
    )))

(define-minor-mode c++-additional-comments-mode "Doc string."
  nil "c++-additional-comments" nil
  (font-lock-add-keywords nil kwds)
  (font-lock-fontify-buffer))

(defface red-box-mediumblue-foreground-face
  '((t :foreground "green"
       :weight bold
       :box (
             :line-width 1
             :color "red")))
  "Additional highlighting faces"
  :group 'basic-faces)


(defun insert-cpp-section-comment (comm)
  (interactive "sname: ")
  (insert (pad-string-centered
           (concat "  " comm "  ")
           60 :inner-padding "/")))

(defun insert-cpp-explanation-comment (comm)
  (interactive "sname: ")
  (insert (concat "// --- " comm " --- //")))

(defun font-lock-replace-keywords (mode keyword-list)
  "Use `font-lock-remove-keywords' on each keyword and then add
  `keyword-list' to font lock."
  (font-lock-remove-keywords
   mode
   (-map 'car keyword-list))
  (font-lock-add-keywords mode keyword-list))

;; ("\\(\\+\\+\\)" . 'spacemacs-mode-line-new-version-lighter-error-face)
;; (,(rx "[#A]") . 'hax/face::boxed::red-bold-boxed)

(defun remove-c-comments-from-syntax-table ()
  (interactive)
  (set-syntax-table
   (let ((table (make-syntax-table)))
     (modify-syntax-entry ?/ "w" table)
     table)))

(font-lock-replace-keywords
 'c++-mode
 `((,(rx "// --- " (*? anything) " --- //") . 'font-lock-warning-face)))

(add-hook 'c++-mode-hook 'remove-c-comments-from-syntax-table)
(add-hook 'c++-mode-hook 'hax/c++-mode-hook)


(defface hax/face::doxygen-verbatim-face
  '((default :inherit default :foreground "green"))
  "Face used to show Doxygen block regions"
  :group 'font-lock-faces)

(defvar hax/face::doxygen-verbatim-face 'hax/face::doxygen-verbatim-face)

;;===

(defface hax/face::doxygen-code-face
  '((default :inherit default))
  "Face used to show Doxygen block regions"
  :group 'font-lock-faces)

(defvar hax/face::doxygen-code-face 'hax/face::doxygen-code-face)

;;===

(defface hax/face::doxygen-tag-face
  '((default :inherit default))
  "Face used to show Doxygen block regions"
  :group 'font-lock-faces)

(defvar hax/face::doxygen-tag-face 'hax/face::doxygen-tag-face)

;;===

(defface hax/face::doxygen-match-face
  '((default :inherit default)
    (t :underline t))
  "Face used to show Doxygen region start end commands"
  :group 'font-lock-faces)

(defvar hax/face::doxygen-match-face 'hax/face::doxygen-match-face)

;;===

(defface hax/face::doxygen-html-tag-face
  '((default :inherit default :foreground "olive"))
  "Face used to show Doxygen region start end commands"
  :group 'font-lock-faces)

(defvar hax/face::doxygen-html-tag-face 'hax/face::doxygen-html-tag-face)

(defconst custom-font-lock-doc-comments
  `(
    (,(rx
       (group  "\\" (| "param")) " "
       (group (+ (any "a-z" "A-Z" "0-9" "_" "*"))))
     (1 'font-lock-keyword-face prepend nil)
     (2 'font-lock-string-face prepend nil)
     )

    ;; FIXME Use `rx' instead of this abomination `concat' on regex?
    ;; seriously? I think the person who initally wrote this
    ;; discovered some way to connect his brain directly to
    ;; regex101.com

    ;; ;; Highlight Doxygen special commands,
    ;; ;;   \cmd or @cmd
    ;; ;; and the non [a-z]+ commands
    ;; ;;   \\ \@ \& \# \< \> \% \" \. \| \-- \--- \~[LanguageId]
    ;; (,(concat
    ;;    "\\(?:"
    ;;    "[\\@][a-z]+"     ;; typical word doxygen special @cmd or \cmd
    ;;    "\\|"
    ;;    ;; non-word commands, e.g. \\ or @\
    ;;    "[\\@]\\(?:\\\\\\|@\\|&\\|#\\|<\\|>\\|%\\|\"\\|\\.\\|::\\||\\|---?\\|~[a-z]*\\)"
    ;;    "\\)")
    ;;  0 ,hax/face::doxygen-match-face prepend nil)

    ;; highlight autolinks. these are referring to functions, so we
    ;; use a different font face from the doxygen special commands.
    (,(concat
       "\\(?:"
       ;; function() or function(int, std::string&, void*) or more
       ;; complex where we only match the first paren, function(x->(),
       ;; 2*(y+z)).
       "[a-za-z_0-9]+(\\([a-za-z_0-9:&*, ]*)\\)?"
       ;; classname::memberfcn or the destructor
       ;; classname::~classname. can also do unqualified references,
       ;; e.g. ::member. the parens are optional, ::member(int, int),
       ;; ::member(a, b). we only require matching of first paren to
       ;; make cases like ::member(x->(), 2*(y+z)) work. we don't want
       ;; \::thing to be highlighed as a function, hence reason to
       ;; look for class::member or space before ::member. note '#'
       ;; can be used instead of '::'
       "\\|"
       "\\(?:[a-za-z_0-9]+\\|\\s-\\)\\(?:::\\|#\\)~?[a-za-z_0-9]+(?\\(?:[a-za-z_0-9:&*, \t]*)\\)?"
       ;; file.cpp, foo/file.cpp, etc. don't want to pickup "e.g." or foo.txt because
       ;; these are not autolinked so look for common c++ extensions.
       "\\|"
       "[a-za-z_0-9/]+\\.\\(?:cpp\\|cxx\\|cc\\|c\\|hpp\\|hxx\\|hh\\|h\\)"
       "\\)")
     0 ,font-lock-function-name-face prepend nil)
    ;; highlight urls, e.g. http://doxygen.nl/autolink.html note we do this
    ;; after autolinks highlighting (we don't want nl/autolink.h to be file color).
    ("https?://[^[:space:][:cntrl:]]+"
     0 ,font-lock-keyword-face prepend nil)
    ;; highlight html tags - these are processed by doxygen, e.g. <b> ... </b>
    (,(concat "</?\\sw"
              "\\("
              (concat "\\sw\\|\\s \\|[=\n\r*.:]\\|"
                      "\"[^\"]*\"\\|'[^']*'")
              "\\)*>")
     0 ,hax/face::doxygen-html-tag-face prepend nil)
    ;; e-mails, e.g. first.last@domain.com. we don't want @domain to be picked up as a doxygen
    ;; special command, thus explicitly look for e-mails and given them a different face than the
    ;; doxygen special commands.
    ("[a-za-z0-9.]+@[a-za-z0-9_]+\\.[a-za-z0-9_.]+"
     0 ,font-lock-keyword-face prepend nil)
    ;; quotes: doxygen special commands, etc. can't be in strings when on same line, e.g.
    ;; "foo @b bar line2 @todo foobar" will not bold or create todo's.
    ("\"[^\"[:cntrl:]]+\""
     0 ,hax/face::doxygen-verbatim-face prepend nil)

    ("[^\\@]\\([\\@]f.+?[\\@]f\\$\\)"  ;; single line formula but an escaped formula, e.g. \\f[
     1 ,hax/face::doxygen-verbatim-face prepend nil)

    ;; doxygen verbatim/code/formula blocks should be shown using doxygen-verbatim-face, but
    ;; we can't do that easily, so for now flag the block start/ends
    (,(concat
       "[^\\@]"  ;; @@code shouldn't be matched
       "\\([\\@]\\(?:verbatim\\|endverbatim\\|code\\|endcode\\|f{\\|f\\[\\|f}\\|f]\\)\\)")
     1 ,hax/face::doxygen-match-face prepend nil)


    ;; (,(rx (group "aaa") "  " (group "bbb"))
    ;;  (1 'hax/face::boxed::green-bold-boxed)
    ;;  (2 'hax/face::boxed::red-bold-boxed)
    ;;  )

    ;; here's an attempt to get blocks shown using doxygen-verbatim-face. however, font-lock doesn't
    ;; support multi-line font-locking by default and i'm not sure the best way to make these work.
    ;;
    ;; doxygen special commands, etc. can't be in verbatim/code blocks
    ;;   @verbatim
    ;;      @cmd  -> not a doxygen special command
    ;;   @endverbatim
    ;; so set verbatim/code to a different font.  verbatim/code blocks spans multiple lines and thus
    ;; a refresh of a buffer after editing a verbatim/code block may be required to have the font
    ;; updated.
    ;;("[^\\@][\\@]\\(verbatim\\|code\\)\\([[:ascii:][:nonascii:]]+?\\)[\\@]end\\1"
    ;; 2 'doxygen-verbatim-face prepend nil)
    ;; doxygen formulas are link verbatim blocks, but contain latex, e.g.
    ;;("[^\\@][\\@]f.+[\\@f]\\$"  ;; single line formula
    ;; 0 'doxygen-verbatim-face prepend nil)
    ;; multi-line formula,
    ;;   \f[ ... \f]     or    \f{ ... \}
    ("[^\\@][\\@]f\\(?:{\\|\\[\\)\\([[:ascii:][:nonascii:]]+?\\)[\\@]f\\(?:}\\|\\]\\)"
     1 'hax/face::doxygen-verbatim-face prepend nil)

    (,(rx
       (group "`" (not (any " ")) (*? anything) (not (any " ")) "`"))
     0 ,hax/face::doxygen-code-face prepend nil)
    ))



;; matches across multiple lines:
;;   /** doxy comments */
;;   /*! doxy comments */
;;   /// doxy comments
;; doesn't match:
;;   /*******/
(defconst custom-font-lock-keywords
  `((,(lambda (limit)
        (c-font-lock-doc-comments "/\\(//\\|\\*[\\*!][^\\*!]\\)"
            limit custom-font-lock-doc-comments)))))

(setq-default c-doc-comment-style (quote (custom)))
