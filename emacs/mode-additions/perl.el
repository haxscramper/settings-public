(defun hax/perl-mode-hook ()
  (yasnippet-snippets-initialize))

(add-hook 'cperl-mode-hook 'hax/perl-mode-hook)
