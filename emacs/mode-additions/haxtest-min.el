(defvar haxtest-min-mode nil)
(make-variable-buffer-local 'haxtest-min-mode)
(put 'haxtest-min-mode 'permanent-local t)

(defun haxtest-min-mode (&optional arg)
  "haxtest minor mode"
  (interactive "P")
  (setq haxtest-min-mode
        (if (null arg) (not haxtest-min-mode)
          (> (prefix-numeric-value arg) 0)))
  (force-mode-line-update))

(provide 'haxtest-min-mode)
