;; This file contains various t


;;;;;;;;;;;;;;;;;  font-lock with monospacified fraktur  ;;;;;;;;;;;;;;;;;;

(when nil
  (defface test-blackletter
    '((default
        :inherit default
        :family "Chomsky monospacified for JetBrains Mono"
        ))
    "Blackletter font"
    :group 'font-lock-faces)

  (setq
   tmp/test-mode-highlights
   `((,(rx (group  "\\" (* word)))
      (1 'test-blackletter)
      )))


  (define-derived-mode hax/tmp::test-mode fundamental-mode
    "hax/tmp::test-mode"
    (setq font-lock-defaults '(hax/tmp::test-mode-highlights))))


(defface test2-font
  '((default
      :inherit default
      :extend t
      :background "green"
      ))
  ""
  :group 'font-lock-faces)

(setq tmp/test-mode-highlights `((,"#test.*" . 'test2-font)))
(define-derived-mode tmp/test-mode fundamental-mode
  "tmp/test-mode"
  (setq font-lock-defaults '(tmp/test-mode-highlights)))


;;;;;;;;;;;;;;;;;;;;;;;;  create new abbrev-table  ;;;;;;;;;;;;;;;;;;;;;;;;
;; DOCS declare new abbrev table and add it to new mode.

(when nil
  (setq hax/tmp::test-mode-abbrev-table nil)
  (define-abbrev-table 'hax/tmp::test-mode-abbrev-table
    '(
      ;; Regular abbreviation
      ("in" "INPUT")
      ;; Abbreviation that will be expanded by running hook. NOTEl!!:
      ;; Anything that is a non-string will be expanded using hook.
      ("sout" ["System.out.println(\"\")" 2 () nil] expand-abbrev-hook 0)
      ;; Custom hook on abbrevation expansion. No arguments are
      ;; supplied to expansion hook - you need to do all
      ;; matching/searching yourself.
      ("hk" ["<>"] (lambda () (message "Running abbrev hook")) 0)
      ))

  (define-derived-mode hax/tmp::test-mode prog-mode "hax/tmp::test-mode-"
    "A major mode for emacs lisp...."

    (abbrev-mode 1)

    ;; NOTEl: looks like the name of abbrev table has to match name of
    ;; the mode regardless of this argument. To create table for mode
    ;; `<mode>' you need to create table canned `<mode>-abbrev-table'.
    :abbrev-table hax/tmp::test-mode-abbrev-table))


;;;;;;;;;;;;;;;;;;;;;;;;;  minibuffer yasnippet  ;;;;;;;;;;;;;;;;;;;;;;;;;;

(cl-defun hax/tmp::prompt-with-snippets (prompt snippet-config &key (default-text ""))
  "Prompt user with string input. `snippet-config' is list of
  pairs: `(name . template)' where template is a regular template
  string. "

  (define-minor-mode hax/tmp::test-minor-mode "hax/tmp::test-minor-mode")

  (add-hook 'minibuffer-setup-hook 'yas-minor-mode)
  (add-hook 'minibuffer-setup-hook 'hax/tmp::test-minor-mode)
  (yas--define-parents 'minibuffer-inactive-mode '(hax/tmp::test-minor-mode))
  (yas-define-snippets
   'hax/tmp::test-minor-mode
   (--map (yas-snippet-to-list (parse-yas-snippet-template
                                (cdr it)
                                :name (car it)))
          snippet-config))

  ;; (define-key minibuffer-local-map [tab] 'yas-expand)
  (unwind-protect (read-from-minibuffer prompt)
    (remove-hook 'minibuffer-setup-hook 'yas-minor-mode)
    (remove-hook 'minibuffer-setup-hook 'hax/tmp::test-minor-mode)))

(when nil
  (erase-buffer-by-name "*scratch*")
  (hax/tmp::prompt-with-snippets "??> " '(("//" . "\\cfrac{$1}{$2}"))))


;;;;;;;;;;;;;;;;;;  syntax highlighting for minibuffer  ;;;;;;;;;;;;;;;;;;;

(defun erase-buffer-by-name (name)
  "Erase all content from buffer `name'"
  (with-current-buffer name (erase-buffer)))

(defun font-lock-force-all (L R &rest _)
  (when (eq this-command 'self-insert-command)
    (save-excursion
      (let* ((beg (point-min))
             (pend (minibuffer-prompt-end))
             (end (point-max)))
        (font-lock-fontify-region pend end)
        (let ((inhibit-read-only t))
          (add-text-properties beg pend '(face minibuffer-prompt)))))))

(defun font-lock-clear-keywords (mode)
  "Remove all keywords from `mode'"
  (font-lock-remove-keywords
   mode
   (-map 'car (cdr (assq mode font-lock-keywords-alist)))))

(cl-defun prompt-with-syntax-higlight (prompt font-lock-kwds &key (default-text ""))
  "Prompt user with text input. Provide syntax highlighting via
  font-lock using `font-lock-kwds' as configuration"

  (when (boundp 'hax/tmp::test-minibuffer-highlight-minor-mode)
    (font-lock-clear-keywords 'hax/tmp::test-minibuffer-highlight-minor-mode))

  (define-minor-mode hax/tmp::test-minibuffer-highlight-minor-mode
    "hax/tmp::test-minibuffer-highlight-minor-mode"
    nil nil nil
    (font-lock-add-keywords nil font-lock-kwds))

  (add-hook 'after-change-functions #'font-lock-force-all)
  (add-hook 'minibuffer-setup-hook 'font-lock-mode)
  (add-hook 'minibuffer-setup-hook 'hax/tmp::test-minibuffer-highlight-minor-mode)

  (unwind-protect (read-from-minibuffer prompt default-text)
    (remove-hook 'after-change-functions #'font-lock-force-all)
    (remove-hook 'minibuffer-setup-hook 'font-lock-mode)
    (remove-hook 'minibuffer-setup-hook 'hax/tmp::test-minibuffer-highlight-minor-mode)))

(when nil
  (erase-buffer-by-name "*scratch*")
  (prompt-with-syntax-higlight "$$> " '(("cfrac" . font-lock-variable-name-face))))



;;;;;;;;;;;;;;;;;;;;;;  combing everything together  ;;;;;;;;;;;;;;;;;;;;;;
(cl-defstruct hax/internal-prompt::history-struct
  buffer-string
  is-escaped)

(defvar hax/internal-prompt::history-table
  (make-hash-table :test #'equal)
  "Table of all history entries. History entry is decribed by
  `hax/internal-prompt::history-struct'")

(defun hax/prompt::get-history (hist-name)
  "Get history entry for a `name'"
  (gethash hist-name hax/internal-prompt::history-table))

(defun hax/prompt::set-history (hist-name entries)
  "Set history entries for `hist-name'. WARNING: this replaces
  all previous history entries with new value if `entries'"
  (setf (gethash hist-name hax/internal-prompt::history-table) entries))

(cl-defun hax/prompt::add-history (hist-name history-entry)
  "Add new history entry by adding it to the start of the history
list for a `hist-name'"
  (hax/prompt::set-history
   hist-name
   (append (list history-entry)
           (hax/prompt::get-history hist-name))))

(when nil
  (hax/prompt::add-history
   "-default-"
   (make-hax/internal-prompt::history-struct :buffer-string "buffer-string")))

(defun buffer-string-no-properties ()
  "Return buffer substring without properies. NOTE: this function
  uses `minibuffer-prompt-end' to get starting position of the
  text. In regular buffers this is identical to `point-min', but
  in minibuffer this will return start of the user inputed text."
  (buffer-substring-no-properties (minibuffer-prompt-end) (point-max)))

(defun hax/internal::font-lock-force-all (L R &rest _)
  "Force update syntax highlighting in the minibuffer"
  (when (eq this-command 'self-insert-command)
    (save-excursion
      (let* ((beg (point-min))
             (pend (minibuffer-prompt-end))
             (end (point-max)))
        (font-lock-fontify-region pend end)
        (let ((inhibit-read-only t))
          (add-text-properties beg pend '(face minibuffer-prompt)))))))

(cl-defun hax/prompt
    (prompt-string &key
                   (font-lock-rules '(()))
                   (snippet-configs '(()))
                   (default-text "")
                   (additional-keymap '(()))
                   (history-name "-default-")
                   (escape-history t)
                   (return-to-escaped-history t)
                   (simple-multiline nil) ; IMPLEMENT
                   (action-logging nil)
                   (additional-hooks nil) ; IMPLEMENT
                   )
  "WARNING:

  tl;dr: this function could easily blow up in your face if you do
  anything funny. It is made using concentrated hacks and works purely
  by coincidence.

  * Usage

  This function is a wrapper around ~read-from-minibuffer~ with lots
  of additional features such as

  - yasnippet templates
  - font-lock syntax higlighting
  - rememeber history even in exit via ~ESC~
  - named history list
  - optional verbose logging

  * Arguments

  - font-lock-rules :: syntax higlighting rules in for font-lock. List
    font-lock keywords. In simples form it is a ~(regex . face)~ pair.
    For more examples see `font-lock-keywords'
  - snippet-configs :: List of pairs. Used to provide snippet
    expanstions. Each pair in list should be in form ~(key .
    expanstion)~ where ~expansion~ is a yasnippet template string (for
    example ~\"\\cfrac{$1}{$2}\"~) and ~key~ is a trigger string (for
    example \"//\")
  - default-text :: default content of the prompt
  - additional-keymap :: TODO DOC
  - history-name :: Named history for a minibuffer. All buffers with
    identical history names share history entries.
  - escape-history :: If buffer exited with ~ESC~ still save content
    in the history list
  - return-to-escaped-history :: _If_ last item in the history was
    added during ~ESC~ handing (input was rejected by user midway)
    _then_ add this item as default text insted of ~default-text~.
    NOTE: This way you can start editing minbuffer, exit edit and
    continue from the place where you left off.
  - simple-mulitline :: Use ~RET~ for inserting newline, ~C-ret~ for
    finishing editing
  - action-loggin :: Provide log messages (added history entries,
    canceled intput etc.) into ~*Messages*~ buffer
  - additional-hooks :: Functions that will be added to buffer setup
    hooks. Lambdas are also allowed - they will be wrapped into
    automatically generated functions and placed to setup hooks. TODO.
    All setup functions are removed from setup hooks when minibuffer
    editing is finished.

  * Todo

  - [ ] Better documentation for key mapping
  - [ ] Preset keymaps for editing differnt types of texts (special
    shortcut to edit latex or shell string literals)
  - [ ] Derive font-lock configuration from mode
  - [ ] Evil mode in minibuffer (protect agains ~ESC~)"

  (when (boundp 'hax/internal::prompt-minor-mode)
    (font-lock-clear-keywords 'hax/internal::prompt-minor-mode))

  (define-minor-mode hax/internal::prompt-minor-mode "INTERNAL
  minor mode used to provide snippets and syntax highlighting in
  minibuffer"
    nil nil nil
    ;; HACK I couldn't find how to set keywords for a minor mode after
    ;; it's creation.
    (font-lock-add-keywords nil font-lock-rules))

  (let* ((minibuffer-hooks
          '(yas-minor-mode
            hax/internal::prompt-minor-mode
            font-lock-mode))
         (escape-handler ;; Handle `ESC'
          (lambda ()
            (interactive)
            (when escape-history
              (hax/prompt::add-history history-name
                                       (make-hax/internal-prompt::history-struct
                                        :buffer-string (buffer-string-no-properties)
                                        :is-escaped t)))
            (keyboard-escape-quit)))
         (confirm-handler ;; Handle regular edit finish
          (lambda ()
            (interactive)
            (when action-logging
              (message "Confirmed result, adding to history list: [%s]"
                       (buffer-string-no-properties)))

            (hax/prompt::add-history history-name
                                     (make-hax/internal-prompt::history-struct
                                      :buffer-string (buffer-string-no-properties)
                                      :is-escaped nil))

            (when action-logging
              (message "New history content:")
              (dolist (it (hax/prompt::get-history history-name))
                (message "\t '%s'" it)))
            (exit-minibuffer)))
         ;; Get list of item structs from table
         (history-items (hax/prompt::get-history history-name))
         ;; Get first history item (last added)
         (first-history (car history-items))
         ;; Use last item in history as init value?
         (init-with-history (and return-to-escaped-history
                                 first-history
                                 (hax/internal-prompt::history-struct-is-escaped first-history)))
         ;; Set actual prompt value
         (prompt (if init-with-history
                     ;; From last item in history
                     (hax/internal-prompt::history-struct-buffer-string first-history)
                   ;; From function parameter
                   prompt-string))
         (history-list (--map (hax/internal-prompt::history-struct-buffer-string it)
                              (if init-with-history (cdr history-items) history-items))))

    ;; Add configuration hooks to minibuffer setup hooks
    (dolist (hk minibuffer-hooks)
      (add-hook 'minibuffer-setup-hook hk)
      (when action-logging (message "Added hook: %s" hk)))
    ;; HACK Force syntax highlighting update on each change.
    (add-hook 'after-change-functions #'hax/internal::font-lock-force-all)

    (when action-logging
      (message "Calling minibuffer completion")
      (message "History: ")
      (dolist (it history-list)
        (message "\t '%s'" it)))

    (unwind-protect (read-from-minibuffer
                     prompt
                     default-text
                     `(keymap
                       (escape . ,escape-handler) ;; Editing cancel
                       (13 . ,confirm-handler) ;; Regular exit
                       (tab . yas-expand) ;; Snippet expansion
                       (up . previous-line-or-history-element)
                       (prior . previous-history-element)
                       (down . next-line-or-history-element)
                       (next . next-history-element)
                       )
                     nil
                     'history-list ;; List of history items
                     )
      ;; `unwind-protect' prevents nonlocal exit to main control loop.
      ;; This is necessary to execute cleanup code (removing all
      ;; hooks) in the even of user cancelling buffer input.
      (dolist (hk minibuffer-hooks) (remove-hook 'minibuffer-setup-hook hk))
      ;; WARNING it is absolutely mandatory to remove this function
      ;; from `after-change-hook'. If this fails it will lead to
      ;; completely unoperable text input in all buffers.
      (remove-hook 'after-change-functions #'hax/internal::font-lock-force-all))))


(when nil
  ;; NOTE simple example of custom minibuffer history
  (defvar tmp/histlist '("sdf " "!!!!!!"))
  (read-from-minibuffer "((( " nil nil nil 'tmp/histlist))

(when t
  (erase-buffer-by-name "*scratch*")
  (hax/prompt "&&> "
              :font-lock-rules `((,(rx "\\" "cfrac") . font-lock-variable-name-face))
              :snippet-configs '(("//" . "\\cfrac{$1}{$2}"))
              :action-logging nil
              ))



;; (when nil
;;   (maphash (lambda (key val) (message "%s -> %s" key val)) hax/internal-prompt::history-table)
;;   (puthash "-default-" '(sfda) hax/internal-prompt::history-table)
;;   (gethash "-default-" hax/internal-prompt::history-table)
;;   )
