;;;;# Nim pegs module grammar

(setq
 nim-pegs-mode-imenu-generic-expression
 '(
   ("rule" "\\(^[a-zA-Z_][a-zA-Z_0-9]*\\)" 1)
   ))

(setq nim-pegs-mode-highlights
      '(
        ("^#.*$" . font-lock-comment-face)
        ("<-" . font-lock-variable-name-face)
        ("^[a-zA-Z_][a-zA-Z_0-9]*" . font-lock-keyword-face)
        ("\\\\[a-zA-Z_][a-zA-Z_0-9]*" . font-lock-function-name-face)
        ("#.*$" . font-lock-comment-face)
        ;; (" | " . flycheck-error)
        ))

(define-derived-mode nim-pegs-mode fundamental-mode
  "nim-pegs-mode"
  (setq font-lock-defaults '(nim-pegs-mode-highlights)))

(add-to-list 'auto-mode-alist '("\\.pegs" . nim-pegs-mode))
(add-hook
 'nim-pegs-mode-hook
 (lambda ()
   ;; (spacemacs/toggle-relative-line-numbers-on)
   (setq imenu-generic-expression nim-pegs-mode-imenu-generic-expression)
   ))

;;;;# BNF converter grammar description

(setq bnfc-mode-highlights
      '(
        ("::=" . font-lock-function-name-face)
        ("^[a-zA-Z0-9]+\\." . font-lock-variable-name-face)
        ("\".?\"" . font-lock-string-face)
        ("\\[[a-zA-Z]+\\]" . font-lock-keyword-face)
        ("^--.*" . font-lock-comment-face)
        ("token \\([a-zA-Z]+\\)" . font-lock-variable-name-face)
        ))

(define-derived-mode bnfc-mode fundamental-mode
  "bnfc-mode"
  (setq font-lock-defaults '(bnfc-mode-highlights)))

(add-to-list 'auto-mode-alist '("\\.cf" . bnfc-mode))
(add-hook
 'bnfc-mode-hook
 (lambda ()
   ;; (spacemacs/toggle-relative-line-numbers-on)
   ))

;;;;# Hcheat cli cheat-sheet files

;; TODO Move into directory for hcheat and load it from there.


;; TODO use nested font-lock to highlight command-line arguments
;; (flags) and old placeholders
(setq hcheat-mode-highlights
      `(("^#[? ].*" . font-lock-doc-face)
        ("^#[%].*" . font-lock-comment-face)
        ;; For example
        ("^#[E].*" . hax/face::simple::bold-dark-green)
        ("^#[!].*" . font-lock-warning-face)
        ("^%.*" . font-lock-function-name-face)
        (,(rx "%" (one-or-more anything) "%") . 'org-code)
        ("^\\$ *\\w+:.*" . font-lock-variable-name-face)
        ((rx-to-string (rx ;; FIXME does not work
                        bol
                        (not (any "#" "%" "$"))
                        (zero-or-more anything)
                        eol)) . font-lock-variable-name-face)
        (;; FIXME does not work
         "^\\$.*" . font-lock-string-face)))

(define-derived-mode hcheat-mode fundamental-mode
  "hcheat-mode"
  (setq font-lock-defaults '(hcheat-mode-highlights)))

(add-to-list 'auto-mode-alist '("\\.cheat" . hcheat-mode))


;;;;#= Shell-sript additiosn

;; FIXME does not work
;; (font-lock-replace-keywords
;;  'shell-script-mode
;;  '(("\"\\$[a-zA-Z_]*\"" . font-lock-variable-name-face))
;;  )

(defun hax/shell-mode-hook()
  (interactive)
  (bash-completion-setup))

(add-hook 'shell-mode-hook 'hax/shell-mode-hook)

(setq
 scad-mode-imenu-generic-expression
 '(("module" "^ *module *\(.*\)" 1)))

(add-hook
 'scad-mode-hook
 (lambda ()
   (setq imenu-generic-expression scad-mode-imenu-generic-expression)))

;;;;#= Common word typos

(define-minor-mode common-typos-highlight-mode
  "common-typos-highlight-mode" nil "dd" nil

  (font-lock-add-keywords
   nil
   '(
     ("[lL]enght" . 'hax/face::boxed::red-bold-boxed)
     ("[Ww]idht" . 'hax/face::boxed::red-bold-boxed)
     ("[Kk]db" . 'hax/face::boxed::red-bold-boxed)
     ("[рР]ассчет" . 'hax/face::boxed::red-bold-boxed)
     ("[aA]ss?ing" . 'hax/face::boxed::red-bold-boxed)
     )
   )

  (if (fboundp 'font-lock-flush)
      (font-lock-flush)
    (when font-lock-mode
      (with-no-warnings (font-lock-fontify-buffer))))
  )


;;;;#= m4circuit

;; (setq m4circuit-mode-highlights '())

;; (define-derived-mode m4circuit-mode fundamental-mode
;;   "m4circuit-mode"
;;   (setq font-lock-defaults '(m4circuit-mode-highlights)))

;; (add-to-list 'auto-mode-alist '("\\.m4circuit" . m4circuit-mode))
