;; https://github.com/syl20bnr/spacemacs/issues/4207
(setq projectile-enable-caching t)

(defun hax/magit-mode-hook()
  (interactive)
  (SPC-prefix! "gI" "ignore")
  (SPC-map!
   "gc" 'magit-commit
   "gA" 'magit-stage-modified
   "ga" 'magit-commit-amend
   ;; "gIt" 'magit-gitignore-in-topdir
   ;; "gIs" 'magit-gitignore-in-subdir
   ;; "gIh" 'helm-gitignore
   ))

(SPC-map! "gdu" 'magit-diff-unstaged)
(SPC-map! "gds" 'magit-diff-staged)
(SPC-map! "gdW" 'magit-diff-working-tree)
(SPC-map! "hdM" 'man)


(spacemacs/declare-prefix "gI" "ignore")

(add-hook 'magit-mode-hook 'hax/magit-mode-hook)
(add-hook 'git-commit-mode-hook 'turn-off-auto-fill)
(setq git-commit-major-mode 'org-mode)



(defun hax/projectile-mode-hook()
  (interactive)
  ;;  (SPC-map! "p#" 'projectile- ripgrep)
  ;;  (SPC-map! "p$" 'spacemacs/helm-project-do-ag)
  )

(add-hook 'projectile-mode-hook 'hax/projectile-mode-hook)

(setq org-projectile-file "todo.org")

(SPC-map! "srp" 'helm-projectile-rg)
(setq git-commit-summary-max-length 50)

(defun hax/git::stage-commit-file()
  "Stage and commit current file"
  (interactive)
  (magit-stage-file (buffer-file-name))
  (magit-commit-create))

(SPC-map! "gC" 'hax/git::stage-commit-file)
