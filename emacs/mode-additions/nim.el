(setq nimsuggest-path "~/.nimble/bin/nimsuggest")

;; (defun +nim|init-nimsuggest-mode ()
;;   "Conditionally load `nimsuggest-mode', instead of clumsily
;; erroring out if nimsuggest isn't installed."
;;   (unless (stringp nimsuggest-path)
;;     (setq nimsuggest-path (executable-find "nimsuggest")))
;;   (when (and nimsuggest-path (file-executable-p nimsuggest-path))
;;     (nimsuggest-mode))

;;   (company-mode 1)
;;   (add-to-list 'company-backends 'company-nimsuggest))


;; (defun +nim|init-nimlsp-mode ()
;;   (flycheck-mode 1)
;;   (add-to-list 'lsp-language-id-configuration '(nim-mode . "nim"))

;;   (lsp-register-client
;;    (make-lsp-client :new-connection (lsp-stdio-connection "nimlsp")
;;                     :major-modes '(nim-mode)
;;                     :server-id 'nimlsp))
;;   (lsp))

(setq
 nim-imenu-generic-expression
 '(
   ("object" "^  \\(.*?\\) = .*?object" 1)
   ("object" "^  \\(.*?\\) = .*?enum" 1)
   ("proc" "^ *proc *\\(.*\\)" 1)
   ("func" "^ *func *\\(.*\\)" 1)
   ("macro" "^ *macro*\\(.*\\)" 1)
   ("method" "^ *method*\\(.*\\)" 1)
   ("converter" "^ *converter*\\(.*\\)" 1)
   ("template" "^ *template *\\(.*\\)" 1)
   ("iterator" "^ *iterator *\\(.*\\)" 1)
   ("separator" "^ *\\(#=* [^#]* =*#\\)$" 1)
   ("separator" "^ *\\(#-* [^#]* -*#\\)$" 1)
   ("separator" "^#\\(\\** [^#]* \\**#\\)$" 1)
   ("suite" "suite \\(\".*\"\\):" 1)
   ("test" "  test \\(\".*\"\\):" 1)
   ("test" "  multitest \\(\".*\"\\):" 1)
   ("task" "task \\(\".*\"\\):" 1)
   ("const" "^const \\(.*\\)" 1)
   ("let" "^let \\(.*\\)" 1)
   ("var" "^var \\(.*\\)" 1)
   ;; FIXME kind of works, but Imenu does not show name of the
   ;; type (even though this is the first capture
   ;; ("Type decl" "\^ *\\(\w*\\)\\*? = \\(object\\|ref object\\|enum\\)" 1)
   ))

(defun toggle-string-literal ()
  "Toggle between single-line and multi-line string literal"
  (interactive)
  ;; TODO
  )


;; (font-lock-remove-keywords 'nim-mode (make-face-bold 'hax/face::simple::bold-dark-green))

(font-lock-replace-keywords
 'nim-mode
 `(
   ;; TODO highlight iteration, declaration and conditional keywords
   ;; differently.
   (" \\(await\\)" . 'font-lock-keyword-face)
   (" \\(runnableExamples\\)" . 'font-lock-keyword-face)
   (" \\(while_let\\)" . 'font-lock-keyword-face)
   (" \\(some\\)" . 'font-lock-keyword-face)
   (" \\(none\\)" . 'font-lock-keyword-face)
   (" \\(collect\\)" . 'font-lock-keyword-face)
   ("XXXX" . 'hax/face::boxed::red-bold-boxed)
   ("notNil" . 'font-lock-builtin-face)
   ;; ("plog:" . 'compilation-mode-line-exit)

   (,(rx (| "echov" "dechofmt" "echo" "debugecho" "plog:" "echoi"))
    . 'anzu-replace-to)

   ;; FIXME too annoying in general
   ;; (,(rx (* word) "1" (| ")" "." "]" " ")) . 'hax/face::underlined::green)
   ;; (,(rx (* word) "2" (| ")" "." "]" " ")) . 'hax/face::underlined::orange)
   (,(rx "@" (+ word)) . 'font-lock-variable-name-face)
   (,(rx (| "iflet")) . 'font-lock-keyword-face)

   (,(rx
      (| " " "." "]" ")")
      (|
       (and (|
             "with" "withRes" "map" "filter" "sortedBy"
             "allOf" "noneOf" "anyOf" "typeCond" "withIt"
             "withDeepIt"

             "someOf" ;; TODO IMPLEMENT in code
             ) "It")
       (| "mapPairs" "toSeq" "tern" "unzip" "zip" "cond"))
      (| " " "." "(" ":")
      )
    . 'custom-face-tag
    )

   (,(rx (| "rmpairs" "rmitems" "rpairs" "ritems")) . font-lock-builtin-face)

   (,(rx
      (| " ")
      (| "shCmd"
         "shAsgn"
         "shAnd"
         "shOr"
         )
      (| " " "." "(" ":")
      )
    . 'custom-group-tag
    )

   ;; (make-face-bold 'hax/face::simple::bold-dark-green)
   ;; FIXME not working correctly (function definitions are also
   ;; colored

   ;; (,(rx (any "func" "proc" "macro" "template" "converter")
   ;;       (group (1+ word)) "(") . 'font-lock-function-name-face)
   ;; (,(rx (group (1+ word)) "(") . 'magit-signature-good)

   ;; TODO use less colorful schemes for operators.
   ;; TODO add more operators
   ;; (" \\(\\.\\.<\\) " . 'font-lock-function-name-face)
   ;; (" \\(\\.\\.\\) " . 'font-lock-function-name-face)
   (" *\\(#%.*\\)" . 'avy-lead-face-1) ;; FIXME
   ;; TODO Add underline/highlighting for chained function calls that
   ;; start at the beginning of the line
   ))

(define-key evil-normal-state-map (kbd "j") 'evil-next-visual-line)
(define-key evil-normal-state-map (kbd "k") 'evil-previous-visual-line)

(define-key evil-visual-state-map (kbd "j") 'evil-next-visual-line)
(define-key evil-visual-state-map (kbd "k") 'evil-previous-visual-line)

(defun nim-insert-doc-here()
  "Insert multiline documentation comment at point"
  (interactive)
  (goto-char (line-end-position))
  (insert "\n  ##[\n\n")
  (save-excursion
    (insert "\n\n  ]##"))
  (evil-insert-state))


(defmacro make-snippet-lambda! (snippet-name)
  `(lambda () (interactive) (insert-snippet-by-name ,snippet-name)))

(defun nim-comment-edit-rst ()
  "Edit nim documentation comment in rst-mode. Default map
  contains ~C-x C-s~ bound to ~comment-edit-end~"
  (interactive)
  (setq comment-edit-style "simplest")
  (setq comment-edit-comment-string "##")
  (setq comment-edit-edit-buffer-mode (lambda() (rst-mode) (comment-edit-minor-mode)))
  (comment-edit))

;;;;;;;;;;;;;;;;;;;;;;;;;;  format TODO comment  ;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun format-nim-comment ()
  (interactive)
  (progn
    (let* ((original (buffer-string))
           (formatted (with-temp-buffer
                        (insert (s-join
                                 ","
                                 (mapcar (lambda (hl-kwd) (car hl-kwd))
                                         hl-todo-keyword-faces)))
                        (insert original)
                        (shell-command-on-region
                         (point-min) (point-max) (f-join hax/binutils-dir "format_comment")
                         t t)
                        (buffer-substring-no-properties (point-min) (point-max))
                        )))
      (erase-buffer)
      (insert formatted))))


(defun nim-simple-comment-edit-rst ()
  ;; TODO dynamically detect comment type and switch dynamically
  "Edit nim regular comment in rst-mode. Default map contains
  ~C-x C-s~ bound to ~comment-edit-end~"
  (interactive)
  (setq comment-edit-style "simplest")
  (setq comment-edit-comment-string "#")
  (setq comment-edit-edit-buffer-mode (lambda() (rst-mode) (comment-edit-minor-mode)))
  (comment-edit))

(defun insert-implement-placeholder ()
  (interactive)
  (insert " #[ IMPLEMENT ]#"))

(defun hax/nim::insert-level-1-section (name)
  (interactive "ssection name: ")
  (insert-comment-separator-large name "*"))


(defun hax/nim::insert-separator-echo (text)
  (interactive "ssection name: ")
  (insert "echo \"\\e[41m")
  (insert (pad-string-centered
           (concat "\\e[49m  " text "  \\e[41m") 40 :padding-char "*"))
  (insert "\\e[49m\""))


(defun hax/nim::insert-separator-echo-static (text)
  (interactive "sStatic echo: ")
  (insert "static: echo \"\\e[41m")
  (insert (pad-string-centered
           (concat "\\e[49m  " text "  \\e[41m") 40 :padding-char "*"))
  (insert "\\e[49m\""))

(defun insert-type-definition-comments (typename)
  (interactive "sType name: ")
  (insert "\n\n")
  (insert-comment-separator-large typename "*")
  (dolist (section '(
                     "Type definition"
                     "Predicates"
                     "Getters"
                     "Setters"
                     "Constructors"
                     "Other implementation"
                     "Pretty-printing"
                     ))
    (insert "\n")
    (insert-comment-separator-medium section "=")
    (insert "\n")))

;; (insert-comment-separator-medium "3" :inner-pa "=")


(spacemacs/set-leader-keys-for-major-mode 'nim-mode ",ia" nil)
;; (spacemacs/declare-prefix-for-mode 'nim-mode ",ia" "assertions")
;; (spacemacs/set-leader-keys-for-major-mode
;;   'nim-mode
;;   ",ia" nil)

;; (spacemacs/set-leader-keys-for-major-mode 'nim-mode
;;   ",iaa" 'insert-implement-assert-placeholder)

;; (which-key-declare-prefixes-for-mode
;;   'c-mode ",iz" "123")

;; (evil-define-key 'normal c-mode-map ",iz" (lambda ()))
;; (evil-define-key 'normal c-mode-map ",iza" (lambda ()))

;; (general-evil-setup)
;; (general-define-key
;;  :states 'normal
;;  :keymaps 'nim-mode-map
;;  :prefix ",ia"
;;  "a" 'insert-implement-assert-placeholder)




(defface comment-section-1-face
  '((default
      :extend t
      :inherit 'font-lock-comment-face
      :underline t
      :overline t))
  "" :group 'font-lock-faces)

(defvar doc-section-highlight-keywords
  '(("## \*[^\n]*\n" 0 'comment-section-1-face t)))
(defvar doc-section-highlight nil)

(define-minor-mode doc-section-highlight "" :lighter "-"
  (if doc-section-highlight
      (font-lock-add-keywords nil doc-section-highlight-keywords 'append)
    (font-lock-remove-keywords nil doc-section-highlight-keywords))
  (when font-lock-mode (font-lock-flush)))


(defun insert-hmisc-exception ()
  (interactive)
  (insert
   (ivy-select-alist
    "Exception > "
    '(
      ("ArgumentError"               . "raise newArgumentError(")
      ("Getter error"                . "raise newGetterError(")
      ("Setter error"                . "raise newSetterError(")
      ("Ref nil argumentError"       . "raise newNilArgumentError(")
      ("Option none argumentError"   . "raise newNoneArgumentError(")
      ("Parse error"                 . "raise newParseError(")
      ("Logic error"                 . "raise newLogicError(")
      ("UnexpectedKindError"         . "raise newUnexpectedKindError(")
      ("ImplementError"              . "raise newImplementError(")
      ("ImplementBaseError"          . "raise newImplementBaseError(")
      ("ImplementKindError"          . "raise newImplementKindError(")
      ("Environment assertion error" . "raise newEnvironmentAssertionError(")
      ("Unexpected char error"       . "raise newUnexpectedCharError(")
      ("Unexpected token error"      . "raise newUnexpectedTokenError(")
      ("Assert argument"             . "assertArg(")
      ("Assert kind"                 . "assertKind(")
      ("Assert has index"            . "assertHasIdx(")
      ("Assert ref"                  . "assertRef(")
      ("Assert ref fields"           . "assertRefFields(")
      ("Assert option"               . "assertOption(")
      )))
  (save-excursion (insert ")"))
  (evil-insert-state))

(defun hax/nim-mode-hook ()
  (interactive)
  ;; (doc-section-highlight)
  (define-key nim-mode-map (kbd "TAB") 'indent-for-tab-command)
  ;; (map! (:prefix ",ia" "a" 'insert-implement-assert-placeholder))
  (map! :map nim-mode-map
        ;; :prefix ",i"
        :desc "Insert any exception"
        :n ",ii" 'insert-hmisc-exception

        :desc "Insert echo for location"
        :n ",il" (make-snippet-callback
                  (concat "debugecho \"\\e[31m!!\\e[39m `(buffer-name)`, `(what-line)` \""))

        :desc "Insert static echo for location"
        :n ",iL" (make-snippet-callback
                  (concat "static: debugecho \"\\e[31m!!\\e[39m `(buffer-name)`, `(what-line)` \""))

        :desc "Assert placeholder"
        :n ",ia" (make-snippet-callback
                  (concat
                   "$0raise newImplementError($1)"))

        :n ",iA" (make-snippet-callback
                  (concat
                   "$0raise newArgumentError($1)"))

        :desc "Insert 'implement for kind' placeholder"
        :n ",ik" (make-snippet-callback
                  (concat
                   "$0raise newImplementKindError($1)"))

        :n ",iu" (make-snippet-callback
                  (concat
                   "$0raise newUnexpectedKindError($1)"))

        :desc "Inline comment"
        :ni ",i#" (make-snippet-callback " #[ $0 ]#")

        )

  (map! :map nim-mode-map
        :nv "M->" 'nim-indent-shift-right
        :nv "M-<" 'nim-indent-shift-left
        :nv "M-RET" 'newline-and-indent-next-level
        :nv "TAB" 'indent-for-tab-command
        :n ",is" 'insert-comment-separator-medium
        :n ",iS" 'hax/nim::insert-level-1-section

        :desc "Insert small section"
        :n ",i-" (make-snippet-callback "# ~~~~ $1 ~~~~ #")

        :n "zc" 'origami-close-node
        :n "zO" 'origami-open-node-recursively
        :n "zo" 'origami-open-node
        :n "zl" 'origami-close-all-nodes
        :n "zL" 'origami-open-all-nodes
        ;; :n ",id" nil

        :desc "Argument description list"
        :ni "s-a" (make-hax-doc-line-callback "- @arg{$1} :: $0")

        :desc "Source code block in documentation comment"
        :ni "s-s" (make-hax-doc-line-callback
                   "#+begin_src nim\n  $0\n#+end_src"
                   )


        :desc "Comment block with source"
        :ni "s-S" (make-hax-doc-line-callback
                   "
  ##[

\\`\\`\\`${1:nim}
$0
\\`\\`\\`

]##
"
                   )

        :desc "Todo comment start"
        :ni "s-d"
        (make-hax-doc-line-callback
         "- ${1:$$(yas-choose-value '(TODO IDEA REFACTOR NOTE))} :: $0"
         )


        :desc "Documentation block"
        :ni "s-D"
        (make-hax-doc-line-callback "  ##[\n$0\n]##")

        :n ",iT" 'insert-type-definition-comments
        :n ",ie" 'hax/nim::insert-separator-echo
        :n ",iE" 'hax/nim::insert-separator-echo-static
        :n ",iU" 'select-common-unicode-char

        :desc "Insert fixme comment"
        :n ",if" (make-snippet-callback " #[ FIXME ]#")

        :desc "Raise static assert"
        :n ",iA" (make-snippet-callback "
  staticAssert($1,
               \"$2\",
               \"$3\"
               hxInfo())
")


        :desc "Raise value error"
        :n ",iv" (make-snippet-lambda! "nim-mode/value-error-exception"))
  (setq imenu-generic-expression nim-imenu-generic-expression)
  ;; (+nim|init-nimsuggest-mode)
  ;; (spacemacs/toggle-visual-line-numbers-on)
  (common-typos-highlight-mode)
  (E-mmap! nim-mode-map "<C-return>" 'newline-and-indent-same-level)
  (E-mmap! nimscript-mode-map "<C-return>" 'newline-and-indent-same-level)
  (E-mmap! nim-mode-map "C-c /" 'nim-comment-edit-rst)
  (E-mmap! nim-mode-map "C-c |" 'nim-simple-comment-edit-rst)
  (hax/hook::prog-mode-hook)
  (outline-minor-mode)
  (hax/nim::outline-nim))


(defun hax/nim::outline-nim ()
  (interactive)
  (setq outline-regexp
        (rx
         (or "." " ")
         (or (and (and "#" (1+ "*") "#" anything)
                  (and "#" (1+ "*")
                       (group (+? anything)) (1+ "*") "#" anything)
                  (and "#" (1+ "*") "#" eol))
             (and "#" (1+ "=") (group (+? anything)) (1+ "=") "#"))
         (or " " "(" "["))))


(add-hook 'write-file-hooks 'delete-trailing-whitespace)
(setq require-final-newline t)

(add-hook 'nim-mode-hook 'hax/nim-mode-hook)
(add-hook 'nimscript-mode-hook 'hax/nim-mode-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;  common imports  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (eval-after-load 'autoinsert
;;   '(define-auto-insert
;;      '("\\.nim\\'" . "nim-lang common imports")
;;      '("nim-lang common imports"
;;        "import std/[sugar, strutils, sequtils, strformat]

;; #===========================  implementation  ============================#

;; #================================  tests  ================================#

;; import unittest

;; suite \"Main\":
;;   test \"test\":
;;     echo 1

;; "
;;        )
;;      )
;;   )


;;;;;;;;;;;;;;;;;;;;;;;;  Shell code highlighting  ;;;;;;;;;;;;;;;;;;;;;;;;
;; (when t
;;   (defvar nim-separator-comments-keywords
;;     `(
;;       (,(rx "#" (>= 4 "=")
;;             (group (*? not-newline))
;;             (>= 4 "=") "#") 1 'hax/face::boxed::orange-bold-boxed t)
;;       ))

;;   (eval-when-compile
;;     (defvar nim-hmisc-hshell-rx-highlight-mode nil))

;;   (define-minor-mode nim-hmisc-hshell-rx-highlight-mode
;;     ""
;;     :lighter " ---"
;;     (if nim-hmisc-hshell-rx-highlight-mode
;;         (font-lock-add-keywords
;;          nil nim-hmisc-hshell-keywords 'append)
;;       (font-lock-remove-keywords nil nim-hmisc-hshell-keywords))
;;     (when font-lock-mode
;;       (font-lock-flush))))
