(defun qdelib-yaml-insert-creation-time-and-date ()
  (interactive)
  (insert (format-time-string "creation-time: %H:%M:%S\n")
          (format-time-string "creation-date: %Y-%m-%d")))
