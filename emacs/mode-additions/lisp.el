(defun hax/lisp-mode-hook()
  (interactive)
  (SPC-map!
   "Esl" (lambda ()
           (interactive)
           (++yas-expand-snippet-by-name "lisp-let-surround"))
   "Esp" (lambda ()
           (interactive)
           (++yas-expand-snippet-by-name "lisp-progn-surround")))
  (map!
   :nv ",dq" 'macrostep-collapse-all
   :nv ",de" 'macrostep-expand)
  (spacemacs/toggle-aggressive-indent-on))

(add-hook 'lisp-mode-hook 'hax/lisp-mode-hook)

;; FIXME does not work
(font-lock-replace-keywords
 'emacs-lisp-mode
 '(
   ;; ("regex" . "font"')
   ("^\\(;;;;#=.*\\)" . 'org-level-1)
   ("^\\(;;;#=.*\\)" . 'org-level-2)
   ("^\\(;;#=.*\\)" . 'org-level-3)))

(define-minor-mode comment-edit-minor-mode
  "Minor mode to indicate comment being edited"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "C-x C-s") 'comment-edit-end)
            (define-key map (kbd "<C-return>") 'newline-and-indent-same-level)
            map))

(defun elisp-comment-edit-org ()
  "Edit elisp comment in org-mode. Default map contains ~C-x C-s~ bound
  to ~comment-edit-end~"
  (interactive)
  (setq comment-edit-style "simplest")
  (setq comment-edit-comment-string ";;")
  (setq comment-edit-edit-buffer-mode (lambda() (org-mode) (comment-edit-minor-mode)))
  ;; (E-mmap! 'comment-edit-mode-map "C-x C-s" 'comment-edit-end)
  (comment-edit))


;;;#== Elisp mode
;; FIXME Hooks is not being triggered
(defun hax/elisp-mode-hook ()
  (interactive)
  (evil-ex-define-cmd
   "wfr[write format reload]"
   (lambda ()
     (elisp-format-buffer)
     (save-buffer)
     (dotspacemacs/sync-configuration-layers)))

  (map!
   :map emacs-lisp-mode-map
   :nv ",hb" 'rainbow-blocks-mode
   :nv ",em" 'macrostep-expand
   :nv ",eM" 'macrostep-collapse-all
   :n "zc" 'hs-hide-block
   :n "zo" 'hs-show-block)
  (E-mmap! emacs-lisp-mode-map "C-c \\" 'poporg-org-mode)
  (E-mmap! emacs-lisp-mode-map "C-c /" 'elisp-comment-edit-org)
  (aggressive-indent-mode +1))

(SPC-map-major! 'emacs-lisp-mode "hdD" 'find-function)


(add-hook 'emacs-lisp-mode-hook 'hax/elisp-mode-hook)
