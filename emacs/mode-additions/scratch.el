(defvar hax/nim::outline-regexp-map
  `((
     :name "Level 1"
     :regexp ,(rx (and (and "#" (1+ "*") "#" anything)
                       (and "#" (1+ "*") (group (+? anything)) (1+ "*") "#" anything)
                       (and "#" (1+ "*") "#" eol)))
     :menu-group 1
     :regex-group 0
     :level 1
     :face 'org-todo)
    (
     :name "Level 1"
     :regexp ,(rx (and "#" (1+ "=") (group (+? anything)) (1+ "=") "#"))
     :menu-group 1
     :regex-group 0
     :level 2
     :face 'org-todo)))

(defun hax/nim::get-outline-level (string regexp-map)
  (cl-loop
   for rx in regexp-map
   do (print rx)
   when (string-match (plist-get rx :regexp) string)
   return (plist-get rx :level)))

(defun hax/nim::outline-nim ()
  (interactive)
  (setq outline-regexp
        (concat
         "\\("
         (s-join "|"
                 (mapcar
                  (lambda (val) (concat "\\(" (plist-get val :regexp) "\\)"))
                  hax/nim::outline-regexp-map))
         "\\)")))

(defvar-local hax/buffer-outline-config '()
  "Outline configuration for current buffer")

(defvar-local hax/internal::outline-highlight-mode-enabled nil)

(defun hax/add-font-lock-from-outline ()
  "Add font-lock configuration from current value of `hax/buffer-outline-config'"
  (message "Adding font-lock from outline")
  (let ((kwds (mapcar
               (lambda (re)
                 (message "Input: %s" re)
                 (list (plist-get re :regexp)
                       (plist-get re :regex-group)
                       (plist-get re :face)))
               hax/buffer-outline-config)))
    (message "Adding font lock keywords from outline")
    (message "Configurationg: %s" kwds)
    (if hax/internal::outline-highlight-mode-enabled
        (font-lock-add-keywords nil kwds 'append)
      (font-lock-remove-keywords nil kwds)))
  (when font-lock-mode (font-lock-flush)))

(define-minor-mode hax/nim::outline-highlight-mode
  ""
  :lighter
  (hax/add-font-lock-from-outline))

(add-hook
 'hax/nim::outline-highlight-mode-hook
 'hax/add-font-lock-from-outline)

(defun hax/nim::enable-outline-highlighting ()
  (interactive)
  (setq hax/buffer-outline-config hax/nim::outline-regexp-map)
  (hax/nim::outline-highlight-mode))
