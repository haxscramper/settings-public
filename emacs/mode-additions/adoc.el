(defun internal/build-adoc-file-simple ()
  (message "Building adoc file")
  (shell-command
   (format "asciidoctor"
           (shell-quote-argument
            (buffer-file-name)))))

;; TODO optimize perfomance to avoid multiple trimming of the same
;; string

;; TODO write shortcut to surround piece of text with asciidoctor
;; [source] wrapper

(defun is-adoc-list-line (string)
  "Determine whether or not string can be used as asciidoctor
list using crude regular expression"
  (let ((trimmed-string (s-trim-left string)))
    (s-match "^(\*{1,6})|^(\.{1,6})|^(-{1,6})"
             "*" trimmed-string)))

(defun get-adoc-list-level (string)
  "Determine asciidoctor list level from string. If string cannot
  be used as adoc list return nil, otherwise return
  level (integer in range [1,6]). Testing for list/not list is
  done using `is-adoc-list-line'"
  (if (is-adoc-list-line string)
      (let ((trimmed  (s-trim-left string)))
        (loop
         for prefix in '("\*" "\." "-")
         )
        )
    nil
    )
  )

(defun justify-current-adoc-list-block()
  (interactive)
  (save-excursion
    (if (is-adoc-list-line (thing-at-point 'line t)))))


(defun extract-words-from-region (start end)
  "Extract words from the region [start, end]. return: list of strings"
  (list))

(defun find-list-section-bottom ()
  "Find bottom of current list section. Bottom is either start of
new section (it might be on lower, higher oor the same level as
current one) or two consecutive empty lines. If you have list
block, two or more empty lines, this will be treated as end of
list section"
  (save-excursion
    (if (is-adoc-list-line (thing-at-point 'line t))
        )
    ))

(defun find-list-start-position (region-start)
  region-start)

(defun is-top-delimiter-line (string)
  "Check if string is top-level delimiter (i.e. starts with
  ----)"
  (s-starts-with-p "----" string))

(defmacro increment-by-one (value)
  `(setf ,value (+ ,value 1)))



(defun find-list-section-top ()
  "Find the top of current list section (position of the first
  character after list prefix (excluding whitespaces)). Return:
  nil if search for top failed (no lines lines within range of
  search were able to satisfy `is-adoc-line')"
  (save-excursion
    (do* ((current-line
           (thing-at-point 'line t)
           (progn
             (forward-line -1)
             (thing-at-point 'line t)))
          (empty-line-count 0 (if (string-empty-p current-line)
                                  (+ empty-line-count 0)
                                0))
          (top-found nil (is-adoc-list-line current-line)))
        (and (not (is-top-delimiter-line current-line))
             (or (> 2 empty-line-count)
                 (not (is-adoc-list-line current-line))) )
      (if top-found
          (find-list-start-position (line-beginning-position))
        nil)
      )
    )
  )


(defun adoc-justify-current-list-section ()
  "If current position is inside of the adoc list justify section
of the list (not parent section and not child sections). List is
left padded to start at the column where list prefix ends (i.e
you have prefix ** that should have two whitespaces before it. So
total left padding will be 4 spaces) and total width of the line
should not exceed some number (#TODO: add support for controlling
right padding size or use common emacs variables)"
  (let* ((region-start (find-list-section-top))
         (region-end (find-list-section-bottom))
         (list-level (get-list-level-at region-start))
         (indent-width (get-list-indent-size list-level)))
    (justify-region-indented
     region-start
     region-end
     indent-width
     65)))




(defun justify-region-indented (start end indent-width right-stop-col)
  (let ((word-list (extract-words-from-region start end))
        (current-pos indent-width)
        (line-width (- right-stop-col
                       indent-width))
        (newline-wordlist '())
        (line-finalized nil)
        (loop
         for word in word-list
         collect
         (progn (when newline-finalized
                  (setf newline-wordlist (list)))
                (cond ((> right-stop-col
                          (length word))
                       (progn
                         (setf newline-wordlist
                               (append newline-wordlist
                                       (list word)))
                         (setf line-finalized t)))
                      ((and (> (length word)
                               line-width)
                            (eq newline-wordlist
                                (list)))
                       (setf newline-wordlist
                             (append newline-wordlist (list word)))))
                (if line-finalized
                    newline-wordlist))))))


;;;#== Asciidoctor hook

(SPC-prefix! "E" "edit")

(defun hax/adoc-mode-hook ()
  "Stop the org-level headers from increasing in height relative to the other text."
;;  (global-set-key (kbd "<f6>") 'internal/build-adoc-file-simple)
  (define-key yas-minor-mode-map (kbd "<tab>") nil)
  (define-key yas-minor-mode-map (kbd "TAB") nil)
  (define-key yas-minor-mode-map (kbd "<C-tab>") 'yas-expand)
  (yas-minor-mode-on)
  (message "Triggered adoc mode hook"))

(add-hook 'adoc-mode-hook 'hax/adoc-mode-hook)
