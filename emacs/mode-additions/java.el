(setq java-imenu-generic-expression
  '(
    ("class" "^.*?class" 1)
    ("Toplevel section" "//##==== \\(.*\\)" 1)
    ("Subsection" "//#~#=== \\(.*\\)" 1)
    ("Local section" "//#~#== \\(.*\\)" 1)
    ))

(defun hax/code::java::insert-printf ()
  (interactive)
  (insert "System.out.printf(\"")
  (save-excursion (insert "\\n\");\n")))

(defun hax/code::insert-here (head tail)
  (interactive)
  (insert head)
  (save-excursion (insert tail)))

(map!
 :map java-mode-map

 :desc "sysprintf+insert state"
 :nv ",if" (lambda() (interactive) (hax/code::java::insert-printf) (evil-insert-state))
 :nv ",ip" (lambda() (interactive)
             (hax/code::insert-here "System.out.println(\"" "\");"))
 )

                                        ;
                                        ;
;; (global-semanticdb-minor-mode 1);
                                        ; (semantic-load-enable-gaudy-code-helpers)

(defun hax/java-mode-hook()
  (interactive)
  (semantic-mode)
  ;; (flymake-mode)
  (semantic-add-system-include "/usr/lib/jvm/java-11-openjdk" 'java-mode)
  (visual-line-mode t)
  (add-hook 'before-save-hook 'clang-format-buffer t t)
  ;; (remove-hook 'before-save-hook 'clang-format-buffer)
  )

;; https://my-clojure.blogspot.com/2012/05/cedet-11-emacs-java.html
;; http://alexott.net/en/writings/emacs-devenv/EmacsCedetOld.html
;; https://stackoverflow.com/questions/4173737/how-to-include-standard-jdk-library-in-emacs-semantic

;; (defun my-java-flymake-init ()
;;   (list "javac" (list (flymake-init-create-temp-buffer-copy
;;                        'flymake-create-temp-with-folder-structure))))

(add-hook 'java-mode-hook 'hax/java-mode-hook)
