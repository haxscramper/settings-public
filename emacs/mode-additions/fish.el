(setq fish-imenu-generic-expression '(
 ("function" "^ *function *\\(.*\\)" 1)
 ))


(defun hax/fish-mode-hook ()
  (interactive)
  (setq imenu-generic-expression fish-imenu-generic-expression)
  )

(add-hook 'fish-mode-hook 'hax/fish-mode-hook)
