;;;; Set of DOOM! macros adapted from https://github.com/gilbertw1/bmacs/blob/master/bmacs.org

(eval-and-compile
  (defun bmacs-enlist (exp)
    "Return EXP wrapped in a list, or as-is if already a list."
    (if (listp exp) exp (list exp)))

  (defun doom-unquote (exp)
    "Return EXP unquoted."
    (while (memq (car-safe exp) '(quote function))
      (setq exp (cadr exp)))
    exp)

  (defvar bmacs-evil-state-alist
    '((?n . normal)
      (?v . visual)
      (?i . insert)
      (?e . emacs)
      (?o . operator)
      (?m . motion)
      (?r . replace))
    "A list of cons cells that map a letter to a evil state symbol.")

  ;; Register keywords for proper indentation (see `map!')
  (put ':after        'lisp-indent-function 'defun)
  (put ':desc         'lisp-indent-function 'defun)
  (put ':leader       'lisp-indent-function 'defun)
  (put ':local        'lisp-indent-function 'defun)
  (put ':localleader  'lisp-indent-function 'defun)
  (put ':map          'lisp-indent-function 'defun)
  (put ':map*         'lisp-indent-function 'defun)
  (put ':mode         'lisp-indent-function 'defun)
  (put ':prefix       'lisp-indent-function 'defun)
  (put ':textobj      'lisp-indent-function 'defun)
  (put ':unless       'lisp-indent-function 'defun)
  (put ':when         'lisp-indent-function 'defun)

;; specials
  (defvar bmacs--keymaps nil)
  (defvar bmacs--prefix  nil)
  (defvar bmacs--defer   nil)
  (defvar bmacs--local   nil)

(defun bmacs--keybind-register (key desc &optional modes)
  "Register a description for KEY with `which-key' in MODES.

  KEYS should be a string in kbd format.
  DESC should be a string describing what KEY does.
  MODES should be a list of major mode symbols."
  (if modes
      (dolist (mode modes)
        (which-key-add-major-mode-key-based-replacements mode key desc))
    (which-key-add-key-based-replacements key desc)))

(defun bmacs--keyword-to-states (keyword)
  "Convert a KEYWORD into a list of evil state symbols.

For example, :nvi will map to (list 'normal 'visual 'insert). See
`bmacs-evil-state-alist' to customize this."
  (cl-loop for l across (substring (symbol-name keyword) 1)
           if (cdr (assq l bmacs-evil-state-alist))
             collect it
           else
             do (error "not a valid state: %s" l)))

(defmacro map! (&rest rest)
  "A nightmare of a key-binding macro that will use `evil-define-key*',
`define-key', `local-set-key' and `global-set-key' depending on context and
plist key flags (and whether evil is loaded or not). It was designed to make
binding multiple keys more concise, like in vim.

If evil isn't loaded, it will ignore evil-specific bindings.

States
    :n  normal
    :v  visual
    :i  insert
    :e  emacs
    :o  operator
    :m  motion
    :r  replace

    These can be combined (order doesn't matter), e.g. :nvi will apply to
    normal, visual and insert mode. The state resets after the following
    key=>def pair.

    If states are omitted the keybind will be global.

    This can be customized with `bmacs-evil-state-alist'.

    :textobj is a special state that takes a key and two commands, one for the
    inner binding, another for the outer.

Flags
    (:mode [MODE(s)] [...])    inner keybinds are applied to major MODE(s)
    (:map [KEYMAP(s)] [...])   inner keybinds are applied to KEYMAP(S)
    (:map* [KEYMAP(s)] [...])  same as :map, but deferred
    (:prefix [PREFIX] [...])   assign prefix to all inner keybindings
    (:after [FEATURE] [...])   apply keybinds when [FEATURE] loads
    (:local [...])             make bindings buffer local; incompatible with keymaps!

Conditional keybinds
    (:when [CONDITION] [...])
    (:unless [CONDITION] [...])

Example
    (map! :map magit-mode-map
          :m \"C-r\" 'do-something           ; assign C-r in motion state
          :nv \"q\" 'magit-mode-quit-window  ; assign to 'q' in normal and visual states
          \"C-x C-r\" 'a-global-keybind

          (:when IS-MAC
           :n \"M-s\" 'some-fn
           :i \"M-o\" (lambda (interactive) (message \"Hi\"))))"
  ;; TODO I will always use evil => No need to handle cases where evil
  ;; is absent from my config.
  (let ((bmacs--keymaps bmacs--keymaps)
        (bmacs--prefix  bmacs--prefix)
        (bmacs--defer   bmacs--defer)
        (bmacs--local   bmacs--local)
        key def states forms desc modes)
    (while rest
      (setq key (pop rest))
      (cond
       ;; it's a sub expr
       ((listp key)
        (push (macroexpand `(map! ,@key)) forms))

       ;; it's a flag
       ((keywordp key)
        (cond ((eq key :leader)
               (push 'emacs-leader-key rest)
               (setq key :prefix
                     desc "<leader>"))
              ((eq key :localleader)
               (push 'bmacs-localleader-key rest)
               (setq key :prefix
                     desc "<localleader>")))
        (pcase key
          (:when    (push `(if ,(pop rest)       ,(macroexpand `(map! ,@rest))) forms) (setq rest '()))
          (:unless  (push `(if (not ,(pop rest)) ,(macroexpand `(map! ,@rest))) forms) (setq rest '()))
          (:after   (push `(after! ,(pop rest)   ,(macroexpand `(map! ,@rest))) forms) (setq rest '()))
          (:desc    (setq desc (pop rest)))
          (:map*    (setq bmacs--defer t) (push :map rest))
          (:map
            (setq bmacs--keymaps (bmacs-enlist (pop rest))))
          (:mode
            (setq modes (bmacs-enlist (pop rest)))
            (unless bmacs--keymaps
              (setq bmacs--keymaps
                    (cl-loop for m in modes
                             collect (intern (format "%s-map" (symbol-name m)))))))
          (:textobj
            (let* ((key (pop rest))
                   (inner (pop rest))
                   (outer (pop rest)))
              (push (macroexpand `(map! (:map evil-inner-text-objects-map ,key ,inner)
                                        (:map evil-outer-text-objects-map ,key ,outer)))
                    forms)))
          (:prefix
            (let ((def (pop rest)))
              (setq bmacs--prefix `(vconcat ,bmacs--prefix (kbd ,def)))
              (when desc
                (push `(bmacs--keybind-register ,(key-description (eval bmacs--prefix))
                                                ,desc ',modes)
                      forms)
                (setq desc nil))))
          (:local
            (setq bmacs--local t))
          (_ ; might be a state bmacs--prefix
           (setq states (bmacs--keyword-to-states key)))))

       ;; It's a key-def pair
       ((or (stringp key)
            (characterp key)
            (vectorp key)
            (symbolp key))
        (unwind-protect
            (catch 'skip
              (when (symbolp key)
                (setq key `(kbd ,key)))
              (when (stringp key)
                (setq key (kbd key)))
              (when bmacs--prefix
                (setq key (append bmacs--prefix (list key))))
              (unless (> (length rest) 0)
                (user-error "map! has no definition for %s key" key))
              (setq def (pop rest))
              (when desc
                (push `(bmacs--keybind-register ,(key-description (eval key))
                                                ,desc ',modes)
                      forms))
              (cond ((and bmacs--local bmacs--keymaps)
                     (push `(lwarn 'bmacs-map :warning
                                   "Can't local bind '%s' key to a keymap; skipped"
                                   ,key)
                           forms)
                     (throw 'skip 'local))
                    ((and bmacs--keymaps states)
                     (dolist (keymap bmacs--keymaps)
                       (push `(,(if bmacs--defer 'evil-define-key 'evil-define-key*)
                               ',states ,keymap ,key ,def)
                             forms)))
                    (states
                     (dolist (state states)
                       (push `(define-key
                                ,(intern (format "evil-%s-state-%smap" state (if bmacs--local "local-" "")))
                                ,key ,def)
                             forms)))
                    (bmacs--keymaps
                     (dolist (keymap bmacs--keymaps)
                       (push `(define-key ,keymap ,key ,def) forms)))
                    (t
                     (push `(,(if bmacs--local 'local-set-key 'global-set-key) ,key ,def)
                           forms))))
          (setq states '()
                bmacs--local nil
                desc nil)))

       (t (user-error "Invalid key %s" key))))
    `(progn ,@(nreverse forms)))))

