;;;#= Buffers

(map!
 :n "<s-next>" 'previous-buffer
 :n "<s-prior>" 'next-buffer)

;;;#= Windows

(defun hax/window::delete-current-window ()
  "Delete current window using `spacemacs/delete-window' and
update display for all visible frames"
  (interactive)
  (spacemacs/delete-window)
  (winum--update)
  (redraw-display)
  )

(defun hax/window::delete-other-window ()
  "Delete window using `spacemacs/ace-delete-window' and update
display for all visible frames"
  (interactive)
  (spacemacs/ace-delete-window)
  (winum--update)
  (redraw-display)
  )

(defun hax/window::split-below ()
  "Split window vertically and update display for all visible
frames"
  (interactive)
  (split-window-below)
  (winum--update)
  (redraw-display)
  )



(defun hax/window::split-right ()
  "Split window vertically and update display for all visible
frames"
  (interactive)
  (split-window-right)
  (winum--update)
  (redraw-display)
  )

(defun hax/frame::make-new-frame ()
  "Create new frame and update display for all visible frames"
  (interactive)
  (make-frame)
  (winum--update)
  (redraw-display)
  )

;; TODO find a way to change shortcut description
(SPC-map! "wD" 'hax/window::delete-current-window)
(SPC-map! "wd" 'hax/window::delete-other-window)
(SPC-map! "ws" 'hax/window::split-below)
(SPC-map! "wv" 'hax/window::split-right)
(SPC-map! "Fn" 'hax/frame::make-new-frame)


(SPC-map! "hdm" 'man)
;;(SPC-map! "hdM" 'describe-mode)


(SPC-map! "xar" 'align-regexp)
(SPC-map! "xaR" 'align-regexp-history)


(map!
 :nv ",hs" 'highlight-symbol
 :nv ",hi" 'highlight-indent-guides-mode
 :nv ",hd" 'indent-guide-mode
 :nv "zl" 'hs-hide-level
 :nv "zL" 'hs-show-block
 )

(map! :nv "C-s" 'cheat-sh)
(map! :nv "<S-k>" nil)
(map!
 :nvi "<S k>" nil
 :nvi "<S j>" nil)

(SPC-map! "SPC" 'replace-next-placeholder)

(map!
 ;; TODO Clear region selection
 :desc "Copy without indentation"
 :nv ",y" (lambda (beginning end)
            (interactive "r")
            (copy-region-zero-indentation nil beginning end t t)
            (deactivate-mark)))



(eval-after-load "ibuffer"
  '(progn
     (map! :map ibuffer-mode-map
           :nv "<up>" 'ibuffer-previous-line
           :nv "<down>" 'ibuffer-next-line
           :nv "k" 'ibuffer-previous-line
           :nv "j" 'ibuffer-next-line)

     (setq ibuffer-formats
	         '((mark modified read-only " "
		               (name 18 18 :left :elide)
		               " "
		               (mode 16 16 :left :elide)
		               " "
		               filename-and-process)))

     (defun hax/ibuffer-mode-hook ()
       (interactive)
       (fira-code-mode)
       (spacemacs/toggle-relative-line-numbers-on)
       )

     (add-hook 'ibuffer-mode-hook 'hax/ibuffer-mode-hook) ))

(SPC-map! "br" 'rename-buffer)

(SPC-map! "hdd" 'evil-goto-definition)

(E-gmap! "<f1>" 'winum-select-window-1)
(E-gmap! "<f2>" 'winum-select-window-2)
(E-gmap! "<f3>" 'winum-select-window-3)
(E-gmap! "<f4>" 'winum-select-window-4)
(E-gmap! "<f5>" 'winum-select-window-5)
