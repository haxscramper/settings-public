;;;#= Placeholder replacement

;; TODO add version with support for replacing commented placeholder
;; with code. (~# <++> optional commentary~ transforms into ~<++> #
;; optional commentary~ and then placeholder is killed)


;; TODO use =ESC= to revert placeholder replacement
(defun replace-next-placeholder ()
  "Find placeholder string, ('{{{replace-target}}}') delete it and enter insert
mode"
  (interactive)
  (search-forward "{{{replace-target}}}")
  (kill-backward-chars 20)
  (evil-insert-state))

;;;#= Copy region

(defun copy-region-equal-indentation (pad beginning end)
  "Copy the region, un-indented by the length of its minimum indent.

If numeric prefix argument PAD is supplied, indent the resulting
text by that amount."
  (interactive "P\nr")
  (let ((buf (current-buffer))
        (itm indent-tabs-mode)
        (tw tab-width)
        (st (syntax-table))
        (indent nil))
    (with-temp-buffer
      (setq indent-tabs-mode itm
            tab-width tw)
      (set-syntax-table st)
      (insert-buffer-substring buf beginning end)
      ;; Establish the minimum level of indentation.
      (goto-char (point-min))
      (while (and (re-search-forward "^[[:space:]\n]*" nil :noerror)
                  (not (eobp)))
        (let ((length (current-column)))
          (when (or (not indent) (< length indent))
            (setq indent length)))
        (forward-line 1))
      (if (not indent)
          (error "Region is entirely whitespace")
        ;; Un-indent the buffer contents by the length of the minimum
        ;; indent level, and copy to the kill ring.
        (when pad
          (setq indent (- indent (prefix-numeric-value pad))))
        (indent-rigidly (point-min) (point-max) (- indent))
        (copy-region-as-kill (point-min) (point-max))))))


;; Credit
;; https://emacs.stackexchange.com/questions/34966/copy-region-without-leading-indentation

;; TODO remove trailing newlines copy
(defun copy-region-zero-indentation (pad beginning end remove-empty no-trail-newline)
  "Copy the region, un-indented by the length of its minimum indent.

If numeric prefix argument PAD is supplied, indent the resulting
text by that amount."
  (interactive "P\nr")
  (let ((buf (current-buffer))
        (itm indent-tabs-mode)
        (tw tab-width)
        (st (syntax-table))
        (indent 120) ;; Change from original version, indentation is
        ;; removed completely
        )
    (with-temp-buffer
      (setq indent-tabs-mode itm tab-width tw)
      (set-syntax-table st)
      (insert-buffer-substring buf beginning end)
      ;; Un-indent the buffer contents by the length of the minimum
      ;; indent level, and copy to the kill ring.
      (when remove-empty
        (progn
          (goto-char (point-min))
          (while (re-search-forward "\n\n+" nil "move")
            (replace-match "\n"))))
      (when pad (setq indent (- indent (prefix-numeric-value pad))))
      (when no-trail-newline
        (progn
          (goto-char (point-max))
          (re-search-backward "\n+" nil "move")
          (replace-match "")))
      (indent-rigidly (point-min) (point-max) (- indent))
      (copy-region-as-kill (point-min) (point-max)))))


(defun hax/backward-kill-word ()
  "Delete word backwards but do not cross over lines"
  ;; Credit:
  ;; https://stackoverflow.com/questions/28221079/ctrl-backspace-in-emacs-deletes-too-much
  ;; (renamed for consistency reasons)
  (interactive)
  (let* ((cp (point))
         (backword)
         (end)
         (space-pos)
         (backword-char (if (bobp)
                            ""           ;; cursor in begin of buffer
                          (buffer-substring cp (- cp 1)))))
    (if (equal (length backword-char) (string-width backword-char))
        (progn
          (save-excursion
            (setq backword (buffer-substring (point) (progn (forward-word -1) (point)))))
          (setq ab/debug backword)
          (save-excursion
            (when (and backword          ;; when backword contains space
                       (s-contains? " " backword))
              (setq space-pos (ignore-errors (search-backward " ")))))
          (save-excursion
            (let* ((pos (ignore-errors (search-backward-regexp "\n")))
                   (substr (when pos (buffer-substring pos cp))))
              (when (or (and substr (s-blank? (s-trim substr)))
                        (s-contains? "\n" backword))
                (setq end pos))))
          (if end
              (kill-region cp end)
            (if space-pos
                (kill-region cp space-pos)
              (delete-word-no-kill -1))))
      (kill-region cp (- cp 1)))         ;; word is non-english word
    ))

(global-set-key  [C-backspace] 'hax/backward-kill-word)
(add-to-list 'spacemacs-indent-sensitive-modes 'nim-mode)

(defun delete-word-no-kill (arg)
  "Delete characters forward until encountering the end of a word.
With argument ARG, do this that many times."
  (interactive "p")
  (delete-region (point) (progn (forward-word arg) (point))))


(defun delete-nobreak-spaces ()
  "Delete nobreak space, char 2208, #o4240, #x8a0 from buffer or region. "
  (interactive "*")
  (let ((beg (cond ((region-active-p)
                    (region-beginning))
                   (t (point-min))))
        (end (cond ((region-active-p)
                    (region-end))
                   (t (point-max)))))
    (save-excursion
      (goto-char beg)
      (while (re-search-forward (char-to-string 2208) end t 1)
        (replace-match "")))))
