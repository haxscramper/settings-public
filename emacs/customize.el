(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   ["#404B5C" "#B26BB8" "#76A8A0" "#C79474" "#6886A6" "#515275" "#7D8AA8" "#8C92A1"])
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(csv-separators '("," ","))
 '(custom-safe-themes
   '("27a1dd6378f3782a593cc83e108a35c2b93e5ecc3bd9057313e1d88462701fcd" "ae65ccecdcc9eb29ec29172e1bfb6cadbe68108e1c0334f3ae52414097c501d2" "1dd7b369ab51f00e91b6a990634017916e7bdeb64002b4dda0d7a618785725ac" "8a97050c9dd0af1cd8c3290b061f4b6032ccf2044ddc4d3c2c39e516239b2463" "621595cbf6c622556432e881945dda779528e48bb57107b65d428e61a8bb7955" "76c5b2592c62f6b48923c00f97f74bcb7ddb741618283bdb2be35f3c0e1030e3" "2d835b43e2614762893dc40cbf220482d617d3d4e2c35f7100ca697f1a388a0e" "6bc387a588201caf31151205e4e468f382ecc0b888bac98b2b525006f7cb3307" "274fa62b00d732d093fc3f120aca1b31a6bb484492f31081c1814a858e25c72e" "59e82a683db7129c0142b4b5a35dbbeaf8e01a4b81588f8c163bd255b76f4d21" "d9aa334b2011d57c8ce279e076d6884c951e82ebc347adbe8b7ac03c4b2f3d72" "cd7ffd461946d2a644af8013d529870ea0761dccec33ac5c51a7aaeadec861c2" "ec5f697561eaf87b1d3b087dd28e61a2fc9860e4c862ea8e6b0b77bd4967d0ba" "d1cc05d755d5a21a31bced25bed40f85d8677e69c73ca365628ce8024827c9e3" "7356632cebc6a11a87bc5fcffaa49bae528026a78637acd03cae57c091afd9b9" "ab04c00a7e48ad784b52f34aa6bfa1e80d0c3fcacc50e1189af3651013eb0d58" default))
 '(diary-entry-marker 'font-lock-variable-name-face)
 '(electric-indent-mode nil)
 '(electric-pair-mode nil)
 '(emms-mode-line-icon-image-cache
   '(image :type xpm :ascent center :data "/* XPM */
static char *note[] = {
/* width height num_colors chars_per_pixel */
\"    10   11        2            1\",
/* colors */
\". c #1ba1a1\",
\"# c None s None\",
/* pixels */
\"###...####\",
\"###.#...##\",
\"###.###...\",
\"###.#####.\",
\"###.#####.\",
\"#...#####.\",
\"....#####.\",
\"#..######.\",
\"#######...\",
\"######....\",
\"#######..#\" };"))
 '(eval-expression-print-level nil)
 '(evil-want-Y-yank-to-eol nil)
 '(fci-rule-color "#383838")
 '(font-latex-fontify-script nil t)
 '(gnus-logo-colors '("#4c8383" "#bababa") t)
 '(gnus-mode-line-image-cache
   '(image :type xpm :ascent center :data "/* XPM */
static char *gnus-pointer[] = {
/* width height num_colors chars_per_pixel */
\"    18    13        2            1\",
/* colors */
\". c #1ba1a1\",
\"# c None s None\",
/* pixels */
\"##################\",
\"######..##..######\",
\"#####........#####\",
\"#.##.##..##...####\",
\"#...####.###...##.\",
\"#..###.######.....\",
\"#####.########...#\",
\"###########.######\",
\"####.###.#..######\",
\"######..###.######\",
\"###....####.######\",
\"###..######.######\",
\"###########.######\" };") t)
 '(hl-todo-keyword-faces
   '(("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#4f97d7")
     ("OKAY" . "#4f97d7")
     ("REVIEW" . "#4f97d7")
     ("IDEA" . "#4f97d7")
     ("REFACTOR" . "#4f97d7")
     ("DONT" . "#f2241f")
     ("DOC" . "#f2241f")
     ;; ("FAIL" quote hax/face::nuclear)
     ("ERROR" . "#f2241f")
     ("TEST" quote hax/face::boxed::orange-bold-boxed)
     ("WARNING" quote hax/face::boxed::red-bold-boxed)
     ("IMPLEMENT" . "#f2241f")
     ("DONE" . "#86dc2f")
     ("NOTE" quote hax/face::boxed::green-bold-boxed)
     ("QUESTION" quote hax/face::boxed::dim-yellow-bold-boxed)
     ("STYLE" quote hax/face::boxed::dim-yellow-bold-boxed)
     ("KLUDGE" quote hax/face::boxed::dim-yellow-bold-boxed)
     ("HACK" quote hax/face::boxed::dim-yellow-bold-boxed)
     ("TEMP" . "#9932cc")
     ("FIXME" quote hax/face::boxed::orange-bold-boxed)
     ("XXX" . "#a52a2a")
     ("XXXX" quote hax/face::boxed::red-bold-boxed)))
 '(markdown-header-scaling nil)
 '(markdown-header-scaling-values '(1.0 1.0 1.0 1.0 1.0 1.0))
 '(nrepl-message-colors
   '("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3"))
 '(org-agenda-files
   '("~/defaultdirs/personal/org/daily.org" "~/defaultdirs/personal/org/default-notes.org" "~/defaultdirs/personal/org/distant.org" "~/defaultdirs/personal/org/events.org" "~/defaultdirs/personal/org/personal-projects.org" "~/defaultdirs/personal/org/repeated.org" "~/defaultdirs/personal/org/todo.org" "~/defaultdirs/personal/org/university.org" "~/defaultdirs/personal/org/weekly.org" "~/defaultdirs/personal/notes/daily/today.org"))
 '(org-default-notes-file "/home/test/org/notes.org")
 '(org-image-actual-width nil)
 '(org-imenu-depth 8)
 '(org-log-done 'time)
 '(org-modules
   '(org-bbdb org-bibtex org-docview org-eww org-gnus org-info org-irc org-mhe org-rmail org-w3m))
 '(org-src-tab-acts-natively t)
 '(org-startup-with-inline-images t)
 '(package-selected-packages
   '(bison-mode docker-tramp ix posframe haxe-mode ox-qmd dockerfile-mode docker rainbow-identifiers general doom peep-dired ob-prolog ox-rst ox-gfm color-theme-approximate bnf-mode image+ outshine yafolding undo-fu-session org-drill flymake-jslint proportional poporg spice-mode feature-mode origami font-lock-studio linum-relative rg wgrep smex ivy-yasnippet ivy-xref ivy-rtags ivy-purpose ivy-hydra counsel-gtags counsel-css typit speed-type rainbow-mode treemacs-magit org-ql peg ts telega evil-collection undo-fu quelpa org-super-agenda python-docstring ob-ipython rainbow-blocks ksp-cfg-mode org-mind-map langtool simpleclip xclip ob-nim helm-org let-alist chocolate-theme cheat-sh string-edit zmq jupyter ob-async go-mode webpaste pastebin wrap-region ob-svgbob bash-completion yasnippet-classic-snippets flycheck-plantuml plantuml-mode zones highlight-function-calls hybrid-mode color-identifiers-mode highlight-symbol highlight-stages highlight-escape-sequences highlight-quoted org-dp systemd calfw-org calfw polymode websocket ein mvn meghanada maven-test-mode lsp-java groovy-mode groovy-imports pcache gradle-mode ensime sbt-mode scala-mode company-emacs-eclim eclim simplenote highlight-indent-guides suggestion-box exec-path-from-shell lsp-ui lsp-treemacs lsp-python-ms python helm-lsp cquery company-lsp ccls utop tuareg caml ocp-indent merlin dune google-this simple-mpc toml test-simple loc-changes load-relative attrap flymake-racket irony realgud projectile-ripgrep ripgrep helm-rg highlight-doxygen qt-pro-mode faceup symbol-overlay org-cliplink nodejs-repl devdocs cpp-auto-include company-reftex geiser quack racket-mode find-file-in-project elpy elvish-mode nim-mode flycheck-nimsuggest commenter flycheck-nim ggtags ov hide-lines marmalade-client org-autolist manage-minor-mode web-mode tagedit slim-mode scss-mode sass-mode pug-mode impatient-mode helm-css-scss haml-mode emmet-mode company-web web-completion-data ibuffer-projectile srefactor command-log-mode helm-gtags function-args lsp-haskell lsp-mode intero flycheck-package package-lint evil-textobj-line dante lcr company-ghc ghc blacken lv latex-pretty-symbols pretty-mode prettify-utils all-the-icons-ivy all-the-icons-dired ranger company-lua lua-mode evil-vimish-fold vimish-fold ag vimrc-mode dactyl-mode evil-easymotion evil-snipe yaml-mode writeroom-mode visual-fill-column writegood-mode web-beautify treemacs-projectile treemacs-evil treemacs ht pfuture scad-mode prettier-js parinfer livid-mode skewer-mode simple-httpd json-navigator hierarchy json-mode json-snatcher json-reformat js2-refactor multiple-cursors js2-mode js-doc ini-mode transient ess-R-data-view ess julia-mode csv-mode company-tern dash-functional tern goto-chg undo-tree zenburn-theme zen-and-art-theme yasnippet-snippets yapfify white-sand-theme underwater-theme ujelly-theme twilight-theme twilight-bright-theme twilight-anti-bright-theme toxi-theme toml-mode tao-theme tangotango-theme tango-plus-theme tango-2-theme sunny-day-theme sublime-themes subatomic256-theme subatomic-theme spacegray-theme soothe-theme solarized-theme soft-stone-theme soft-morning-theme soft-charcoal-theme smyx-theme smeargle slime-company slime seti-theme reverse-theme rebecca-theme railscasts-theme racer pos-tip pyvenv pytest pyenv-mode py-isort purple-haze-theme professional-theme planet-theme pippel pipenv pip-requirements phoenix-dark-pink-theme phoenix-dark-mono-theme orgit organic-green-theme org-projectile org-category-capture org-present org-pomodoro alert log4e gntp org-mime org-download org-brain omtose-phellack-theme oldlace-theme occidental-theme obsidian-theme noctilux-theme naquadah-theme mustang-theme monokai-theme monochrome-theme molokai-theme moe-theme mmm-mode minimal-theme material-theme markdown-toc majapahit-theme magit-svn magit-gitflow madhat2r-theme lush-theme live-py-mode light-soap-theme kaolin-themes jbeans-theme jazz-theme ir-black-theme insert-shebang inkpot-theme importmagic epc ctable concurrent deferred htmlize hlint-refactor hindent heroku-theme hemisu-theme helm-rtags helm-pydoc helm-org-rifle helm-hoogle helm-gitignore helm-git-grep helm-ctest helm-company helm-c-yasnippet hc-zenburn-theme haskell-snippets gruvbox-theme gruber-darker-theme graphviz-dot-mode grandshell-theme gotham-theme google-c-style gnuplot gitignore-templates gitignore-mode gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link gh-md gandalf-theme fuzzy flycheck-bashate flycheck flatui-theme flatland-theme fish-mode farmhouse-theme eziam-theme exotica-theme evil-org evil-multiedit evil-magit magit magit-popup git-commit with-editor espresso-theme elisp-format dracula-theme doom-themes django-theme disaster darktooth-theme autothemer darkokai-theme darkmine-theme darkburn-theme dakrone-theme cython-mode cyberpunk-theme company-statistics company-shell company-rtags rtags company-plsense company-ghci haskell-mode company-cabal company-c-headers company-auctex company-anaconda company common-lisp-snippets color-theme-sanityinc-tomorrow color-theme-sanityinc-solarized cmm-mode cmake-mode cmake-ide levenshtein clues-theme clang-format cherry-blossom-theme cargo markdown-mode rust-mode busybee-theme bubbleberry-theme birds-of-paradise-plus-theme badwolf-theme auto-yasnippet yasnippet auctex-latexmk auctex apropospriate-theme anti-zenburn-theme anaconda-mode pythonic ample-zen-theme ample-theme alect-themes afternoon-theme adoc-mode markup-faces ac-ispell auto-complete ws-butler winum volatile-highlights vi-tilde-fringe uuidgen toc-org symon string-inflection spaceline-all-the-icons spaceline powerline restart-emacs request rainbow-delimiters popwin persp-mode password-generator paradox spinner overseer org-bullets open-junk-file neotree nameless move-text macrostep lorem-ipsum link-hint indent-guide hungry-delete hl-todo highlight-parentheses highlight-numbers parent-mode highlight-indentation helm-xref helm-themes helm-swoop helm-purpose window-purpose imenu-list helm-projectile helm-mode-manager helm-make helm-flx helm-descbinds helm-ag google-translate golden-ratio flx-ido flx fill-column-indicator fancy-battery eyebrowse expand-region evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-lisp-state evil-lion evil-indent-plus evil-iedit-state iedit evil-goggles evil-exchange evil-escape evil-ediff evil-cleverparens smartparens paredit evil-args evil-anzu anzu eval-sexp-fu highlight elisp-slime-nav editorconfig dumb-jump doom-modeline eldoc-eval shrink-path all-the-icons memoize f dash s define-word counsel-projectile projectile counsel swiper ivy pkg-info epl column-enforce-mode clean-aindent-mode centered-cursor-mode auto-highlight-symbol auto-compile packed aggressive-indent ace-window ace-link ace-jump-helm-line helm avy helm-core popup which-key use-package pcre2el org-plus-contrib hydra font-lock+ evil dotenv-mode diminish bind-map bind-key async))
 '(pdf-view-midnight-colors '("#b2b2b2" . "#292b2e"))
 '(pos-tip-background-color "#3D4E54")
 '(pos-tip-foreground-color "#C1CADE")
 '(telega-chat-fill-column 60 t)
 '(telega-chat-use-markdown-version 1 t)
 '(telega-notifications-mode t)
 '(tex-fontify-script nil)
 '(vc-annotate-background "#404040")
 '(vc-annotate-color-map
   '((20 . "#ea4141")
     (40 . "#db4334")
     (60 . "#e9e953")
     (80 . "#c9d617")
     (100 . "#dc7700")
     (120 . "#bcaa00")
     (140 . "#29b029")
     (160 . "#47cd57")
     (180 . "#60a060")
     (200 . "#319448")
     (220 . "#078607")
     (240 . "#1ec1c4")
     (260 . "#1ba1a1")
     (280 . "#26d5d5")
     (300 . "#58b1f3")
     (320 . "#00a2f5")
     (340 . "#1e7bda")
     (360 . "#da26ce")))
 '(vc-annotate-very-old-color "#da26ce")
 '(xterm-color-names
   ["#404B5C" "#B26BB8" "#76A8A0" "#C79474" "#6886A6" "#515275" "#7D8AA8" "#8C92A1"])
 '(xterm-color-names-bright
   ["#666B88" "#C27CBE" "#7FBAB0" "#9FC7AD" "#76A0C4" "#898EC4" "#A4A4BD" "#858B99"])
 '(yas-global-mode t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil))))
 '(bold ((t (:weight bold :height 1.0 :width normal))))
 '(font-latex-sectioning-0-face ((t (:inherit bold :foreground "#67b11d" :height 1.0))))
 '(font-latex-sectioning-1-face ((t (:inherit bold :foreground "#b1951d" :height 1.0))))
 '(font-latex-sectioning-2-face ((t (:inherit bold :foreground "#4f97d7" :height 1.0))))
 '(font-latex-sectioning-3-face ((t (:inherit bold :foreground "#2d9574" :height 1.0))))
 '(font-latex-sectioning-4-face ((t (:foreground "#67b11d" :weight normal :height 1.0))))
 '(font-latex-sectioning-5-face ((t (:foreground "#b1951d" :weight normal :height 1.0))))
 '(font-latex-slide-title-face ((t (:inherit (variable-pitch font-lock-type-face) :weight bold))))
 '(font-latex-subscript-face ((t nil)))
 '(font-latex-superscript-face ((t nil)))
 '(font-lock-doc-face ((t (:foreground "indian red" :height 1.0))))
 '(markup-hide-delimiter-face ((t (:inherit markup-meta-face :foreground "gray25"))) t)
 '(markup-meta-face ((t (:stipple nil :foreground "gray30" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 1.0 :width normal :foundry "unknown" :family "Monospace"))))
 '(markup-meta-hide-face ((t (:inherit markup-meta-face :foreground "gray25"))))
 '(markup-secondary-text-face ((t (:inherit markup-gen-face :foreground "firebrick"))))
 '(markup-small-face ((t (:inherit markup-gen-face))))
 '(markup-subscript-face ((t (:inherit markup-gen-face))))
 '(markup-superscript-face ((t (:inherit markup-gen-face))))
 '(markup-title-0-face ((t (:inherit markup-gen-face :foreground "white" :underline nil :weight extra-bold))))
 '(markup-title-1-face ((t (:inherit markup-gen-face :foreground "red" :weight extra-bold))))
 '(markup-title-2-face ((t (:inherit markup-gen-face :foreground "orange" :weight bold))))
 '(markup-title-3-face ((t (:inherit markup-gen-face :foreground "chartreuse" :weight semi-bold))))
 '(markup-title-4-face ((t (:inherit markup-gen-face :foreground "cyan" :slant italic))))
 '(markup-title-5-face ((t (:inherit markup-gen-face :foreground "magenta" :underline t))))
 '(rainbow-blocks-depth-1-face ((t (:foreground "green"))))
 '(rainbow-blocks-depth-2-face ((t (:foreground "orange"))))
 '(rainbow-blocks-depth-3-face ((t (:foreground "yellow"))))
 '(rainbow-blocks-depth-4-face ((t (:foreground "brown"))))
 '(rainbow-blocks-depth-5-face ((t (:foreground "blue"))))
 '(rainbow-blocks-depth-6-face ((t (:foreground "firebrick"))))
 '(rainbow-blocks-depth-7-face ((t (:foreground "white"))))
 '(rainbow-blocks-depth-8-face ((t (:foreground "cyan")))))
