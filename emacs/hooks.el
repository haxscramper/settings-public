(add-hook 'write-file-hooks 'delete-trailing-whitespace)
(setq require-final-newline t)

;; TODO Format rust buffer on save using rustfmt rules
;;;#== Perl mode
(setq cperl-indent-level 4              ;
      cperl-brace-offset 0              ;
      cperl-label-offset 0              ;
      cperl-indent-parens-as-block t    ;
      )


;;;#= C++ hook

;; (defun my/c++-mode-hook ()
;;   (SPC-map! "m=" 'clang-format-buffer))

;; (add-hook 'c++-mode 'my/c++-mode-hook)
