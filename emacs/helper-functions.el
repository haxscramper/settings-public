(defun interpolate-float (x y a)
  "x(1-a) + ya"
  (cond ((not x) y) ;; for some reason when launching in tty x is nil
        ((not y) x)
        (t (+ (* x (- 1 a))
              (* y a)))))


(defun interpolate-rgb (rgb-1 rgb-2 mix)
  "Interpolate between two rgb values"
  (list (floor (interpolate-float (nth 0 rgb-1) (nth 0 rgb-2) mix))
        (floor (interpolate-float (nth 1 rgb-1) (nth 1 rgb-2) mix))
        (floor (interpolate-float (nth 2 rgb-1) (nth 2 rgb-2) mix))))

(defun generate-transparent-on (background foreground transparency)
  "Simulate transparent colors by interpolating between
`background' and `foreground'"
  (interpolate-rgb (color-values background)
                   (color-values foreground)
                   transparency))

(defun rgb-to-hex (rgb)
  "Convert (r g b) list to #RRGGBB recongnised by face colors"
  (let ((r-256 (floor (/ (nth 0 rgb) 256)))
        (g-256 (floor (/ (nth 1 rgb) 256)))
        (b-256 (floor (/ (nth 2 rgb) 256))))
    (format "#%02X%02X%02X" r-256 g-256 b-256)))

(defun generate-transparent (color transparency)
  "Simulate transparent color by using default background as
underlying color"
  (generate-transparent-on (face-attribute 'default :background)
                           color transparency))
