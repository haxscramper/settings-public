;;;;;;;;;;;;;;;;;;;;;;;;;  get image properties  ;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro assert-file-exist-p (path on-error)
  `(if (file-exists-p path) t
     (error "No such file or directory '%s'. %s" ,path ,on-error)
     nil))

(defun get-image-height (path)
  "Return image height in pixels"
  (when (assert-file-exist-p path "Failed to get image height")
    (cdr (image-size (create-image path) t))))

(defun get-image-width (path)
  "Return image width in pixels"
  (when (assert-file-exist-p path "Failed to get image height")
    (car (image-size (create-image path) t))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  telegra.el  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun telega-send-string (text)
  ;; TODO add support for sending formatted messages. Make function
  ;; accept either stirng or list of list of text blocks. Possible
  ;; text blocks: `(text . formatting)', `(plaintext)'
  "Send string in current buffer"
  (telega--sendMessage
   telega-chatbuf--chat
   `(
     :@type "inputMessageText"
     :text (
            :@type "formattedText"
            :text ,text ; Message string
            :entities []
            ;; Entities in the text. Regions of the
            ;; bold/italic/udnerline etc. text. Example:
            ;;
            ;; `(:@type "inputMessageText" :text (:@type
            ;; "formattedText" :text "sss" :entities [(:@type
            ;; "textEntity" :offset 0 :length 3 :type (:@type
            ;; "textEntityTypeBold"))] :@extra 84) :clear_draft t)'
            :@extra 111)
     :clear_draft t)))

;; (setq telega-chat-upload-attaches-ahead t)

;; (telega-chatbuf--gen-input-file "/tmp/result.png")

(defun telega-send-test ()
  (let* ((tg-file (telega-chatbuf--gen-input-file "/tmp/result.png" 'Photo))
         (tg-imc `(:@type "inputMessagePhoto"
                          :photo ,tg-file
                          :width 70
                          :height 58)))
    (telega--sendMessage telega-chatbuf--chat tg-imc)))


(cl-defun telega-send-image (path &optional caption)
  "Send image to current chat"
  (when (assert-file-exist-p path "Failed to send image")
    (let* ((image-height (get-image-height path))
           (image-width (get-image-width path))
           (tg-file (telega-chatbuf--gen-input-file path 'Photo))
           (tg-caption (if caption caption ""))
           (tg-imc `(:@type "inputMessagePhoto"
                            :photo ,tg-file
                            :width ,image-width
                            :height ,image-height
                            :caption (:@type "formattedText"
                                             :text ,tg-caption
                                             :entities []
                                             :@extra 149))))


      (telega--sendMessage telega-chatbuf--chat tg-imc))))

(cl-defun telega-send-latex (string &key (displaystyle t))
  "Convert latex string to image and send it. Original text will
  be used as caption for an image. Return `t' on success,
  otherwise return `nil'"
  (let* ((res-text (if displaystyle
                       (concat "\\displaystyle" string)
                     string))
         (image-path "/tmp/telega-latex-render.png")
         )
    (org-create-formula-image res-text image-path nil nil)
    (if (not (file-exists-p image-path))
        (progn (warn "Failed to render latex image") nil)
      ;; IDEA add formatting to string so (use monspaced?)
      (telega-send-image image-path string)
      t)))

(defun telega-protmp-send-latex (string)
  (interactive "slatex: ")
  (telega-send-latex string))
