(setq-default                                  ;
 dotspacemacs-distribution 'spacemacs          ;
 dotspacemacs-enable-lazy-installation 'unused ;
 dotspacemacs-ask-for-lazy-installation t      ;
 dotspacemacs-configuration-layer-path         ;
 '("~/.config/hax-config/emacs/layers")        ;
 dotspacemacs-configuration-layers             ;
 '(                                            ;
   ;;#== Scripting languages
   python
   ;; common-lisp
   perl5
   shell-scripts
   vimscript
   lua
   ;; ess
   emacs-lisp
   ;; racket
   (cmake :variables
          cmake-enable-cmake-ide-support t)
   ;;#== Compiled languages
   rust
   (c-c++ :variables
          c-c++-enable-clang-support t
          c-c++-enable-clang-format-on-save t
          c-c++-enable-rtags-completion nil)
   haskell
   ocaml
   ;;#== Markup languages
   graphviz
   markdown
   asciidoc
   org
   html
   (latex :variables
          latex-enable-auto-fill nil)
   ;;#== Data serialization languages
   javascript
   yaml
   csv
   ;;#== Misc layers
   themes-megapack
   ;; helm
   ivy
   gtags
   git
   theming
   ibuffer
   lsp
   systemd
   command-log ;
   ;;#== Personal configuration layers
   (display :location local)                                  ;
   (auto-completion :variables                                    ;
                    auto-completion-return-key-behavior 'complete ;
                    auto-completion-tab-key-behavior 'cycle       ;
                    auto-completion-complete-with-key-sequence-delay 0.01 ;
                    auto-completion-enable-snippets-in-popup t ;
                    ;; TODO why disabled?
                    ;;:  auto-completion-enable-sort-by-usage t ;
                    ;;:  spacemacs-default-company-backends         ;
                    ;; '(company-files company-capf))             ;
                    )                                       ;
   )                                                         ;
 dotspacemacs-additional-packages
 '(
   multiple-cursors ; Multiple cursors
   evil-mc
   elisp-format    ; Format emacs lisp code
   writegood-mode  ; Writing
   scad-mode       ; Support for openscad syntax
   ini-mode        ; Support for ini-file syntax
   parinfer        ; Aggresive indent mode alternative
   evil-snipe      ; Select multi-char combinations to jump to
   ranger          ; Ranger-like file preview
   evil-easymotion ;
   ag              ; Seach using ag instead of grep
   function-args   ; Jump to tag in c++ file
   helm-gtags      ; Jump to tag in project (requires gtags installed)
   srefactor       ; C++ code refactoring
   org-autolist    ; Smart org mode list indentation
   qt-pro-mode     ; Qt .pro project files
   hide-lines      ;
   ov              ;
   elvish-mode     ; Support elvish shell syntax
   elpy
   projectile-ripgrep
   ripgrep
   helm-rg
   helm-projectile
                                        ;lsp-mode
                                        ;lsp-ui
                                        ;company-lsp
   nim-mode
   yasnippet-snippets
   exec-path-from-shell
   toml ; Reading main configuration files
   realgud ; Debugging support for various languages

   helm-org

   highlight-quoted ;; Highlight quoted in lisp modes
   highlight-symbol
   bash-completion
   webpaste

   ;;;#= Org-mode
   ob-async
   ob-nim

   string-edit
   highlight-quoted
   cheat-sh
   simpleclip
   highlight-indent-guides
   rainbow-blocks
   rainbow-mode

   ;;;#= Python
   ;; jupyter
   ;; ob-ipython
   python-docstring

   undo-fu

   telega

   rg
   langtool
   spice-mode
   evil-collection
   poporg
   org-drill
   slime

   undo-fu-session
   outshine

   ox-gfm
   docker-tramp
   )                       ;
 dotspacemacs-frozen-packages '()                           ;
 dotspacemacs-excluded-packages '()                         ; dotspacemacs-install-packages 'used-only                   ;
 dotspacemacs-themes '(lush                                 ;
                       alect-black-alt                      ;
                       toxi                                 ;
                       afternoon                            ;
                       alect-black                          ;
                       cyberpunk                            ;
                       )                                    ;
 )
