;;;; This file will be loaded from dotspacemacs/user-config function

(message "==== Loading user config files")
;; (toggle-debug-on-error)
(setq-default explicit-shell-file-name "/bin/bash")
(setq exec-path-from-shell-shell-name "/bin/bash")
(add-to-load-path "~/.config/hax-config/emacs/load-path/")
;; (toggle-debug-on-error)

(require 'comment-edit)

;;;#= Config files loading
;;;#== Support/misc/package variables setting
;;;#=== Misc macro/helpers
(load-config "misc.el")
;;;#=== DOOM emacs macro
(load-config "doom_macros.el")
;;;#== Multiple cursors
(load-config "multiple_cursors.el")
;;;#=== Vim delete == cut fix
(load-config "delete-kill.el") ; Have to load after multiple cursors
(load-config "global-settings.el")
;;;#=== Evil settings
(load-config "evil.el")

;;;#== Hooks
(load-config "hooks.el")
;; (load-config "fira-code.el")
;;;#=== Per-mode hooks

(load-config "helper-functions.el")

(load-config "mode-additions/adoc.el")
(load-config "mode-additions/cpp.el")
(load-config "mode-additions/latex.el")
(load-config "mode-additions/git.el")
(load-config "mode-additions/lisp.el")
(load-config "mode-additions/bash.el")
(load-config "mode-additions/qde-yaml.el")
(load-config "mode-additions/rust.el")
(load-config "mode-additions/org.el")
(load-config "mode-additions/nim.el")
(load-config "mode-additions/java.el")
;; (load-config "mode-additions/fish.el")
(load-config "mode-additions/custom.el")
(load-config "mode-additions/bash.el")

(load-config "files.el")
(load-config "shortcuts.el")
(load-config "temp-settings.el")
(load-config "hax-faces.el")
(load-config "text-edit.el")



(message "==== Done loading additional config files")

(require 'syslog-mode)



(SPC-prefix! "E" "edit")



;;;#= Overriding emacs functions here

(defadvice keyboard-escape-quit (around keyboard-escape-quit-dont-close-windows activate)
  (let ((buffer-quit-function (lambda()
                                ()))) ad-do-it))
;;;#= Writing
;;;#== Spellchecking
(setq ispell-program-name "hunspell")
(setq ispell-local-dictionary "en")


;;;#== Ex-commands
(load-config "evil_ex_commands.el")

;;;#== line manipulation
(define-all-mode-key "<M-up>" 'move-text-up)
(define-all-mode-key "<M-down>" 'move-text-down)

;;;#== Replacement
;; TODO Map C-/ to :%s///g
;; TODO Add bindings to replace in selection
(defun visual-mode-selection-replace (region-beginning  region-end)
  "Start interactive replace using current selected reguion as
  from string. If no selected region is present fallback to
  default interactive replace"
  (interactive "r")
  (if (use-region-p)
      (let* ((region-text (buffer-substring-no-properties (region-beginning)
                                                          (region-end)))
             (replace-with (read-from-minibuffer "Replace-with: ")))
        (query-replace region-text replace-with))
    (query-replace)))

(define-key evil-visual-state-map
  (kbd "M-%") 'visual-mode-selection-replace)




(defun spacemacs-leader-key-hard-set (leader-key target-name target-function)
  "Set new leader key combination and delete anything that was used before."
  (delete-spacemeacs-leader-key leader-key)
  (spacemacs/declare-prefix leader-key target-name)
  (spacemacs/set-leader-keys leader-key target-function))


;; (require 'elisp-format)
;;;#= Major mode
(spacemacs/declare-prefix-for-mode 'c++-mode "s" "switch header source")
(spacemacs/set-leader-keys-for-major-mode 'c++-mode "s" 'ff-find-other-file)

;;;#= Spacemacs bindings
;;;#== Leader keys
;; TODO Jump to configuration files or buffers (if file is already being edited)

;;;#=== File operations
(spacemacs/declare-prefix "fer" "write-current-buffer-sync-spacemacs")
(spacemacs/set-leader-keys "fer"        ;
  (lambda ()
    (interactive)
    ('save-buffer)
    ('dotspacemacs/sync-configuration-layers)))

(spacemacs/declare-prefix "fw" "write-file")
(spacemacs/set-leader-keys "fw" 'write-file)

(spacemacs/declare-prefix "si" "isearch-forward")
(spacemacs/set-leader-keys "si" 'isearch-forward)


;;;#=== Bufer navigation
;; TODO Add table of contents for section comments (ones that start with #==)
;; FIXME Key remapping does not work

;; ;; (spacemacs/declare-prefix "bi" "open-ibuffer")
;; (spacemacs/set-leader-keys "bi" 'ibuffer)
;; (delete-spacemeacs-leader-key "bs")
;; (spacemacs/declare-prefix "bs" "switch buffers")
;; (spacemacs/set-leader-keys                  ;
;; "bss" 'spacemacs/switch-to-scratch-buffer ;
;; "bsn" 'switch-to-buffer                   ;
;; "bsm" 'spacemacs/switch-to-messages-buffer)

;; ;; FIXME Does not show in M-x menu
;; TODO:STUB
;; (defun format-current-buffer
;; (interactive)
;; (cond ((equal major-mode 'elisp-mode)
;; (elisp-format-buffer)
;; (delete-trailing-whitespace))
;; ((equal major-mode 'c++-mode)
;; (clang-format-buffer)
;; (delete-trailing-whitespace))
;; )
;; )


;;;#=== Window navigation
;; TODO Add function for remapping keys to funciton with the same name
;;      and using it in list (similar to spacemacs/set-leader-keys)
(delete-spacemeacs-leader-key "bl")
(spacemacs/declare-prefix "bl" "evil-window-left")
(spacemacs/set-leader-keys "bl" 'evil-window-left)

(spacemacs/set-leader-keys-for-major-mode
  'elisp-mode "mf" 'elisp-format-buffer) ;; FIXME Does not work

(spacemacs/declare-prefix-for-mode
  'elisp-mode "mf" "elisp-format-buffer")

;;;#== Window
(global-set-key (kbd "<s-S-up>") 'evil-window-increase-height)
(global-set-key (kbd "<s-S-down>") 'evil-window-decrease-height)
(global-set-key (kbd "<s-S-left>") 'evil-window-decrease-width)
(global-set-key (kbd "<s-S-right>") 'evil-window-increase-width)

(define-all-mode-key "<s-C-S-up>" 'evil-window-move-very-top)
(define-all-mode-key "<s-C-S-down>" 'evil-window-move-very-bottom)
(define-all-mode-key "<s-C-S-left>" 'evil-window-move-far-left)
(define-all-mode-key "<s-C-S-right>" 'evil-window-move-far-right)
(define-all-mode-key "s-r" 'spacemacs/rotate-windows-forward)
(define-all-mode-key "s-S-r" 'spacemacs/rotate-windows-backward)
(define-all-mode-key "<C-tab>" 'next-multiframe-window)
(define-all-mode-key "<C-iso-lefttab>" 'previous-multiframe-window)

;;;#= Random text generation
;; ///
;; Below is placed random junk that im not really sure about
;; ///

;; Source: http://www.emacswiki.org/emacs-en/download/misc-cmds.el
(defun revert-buffer-no-confirm ()
  "Revert buffer without confirmation."
  (interactive)
  (revert-buffer
   :ignore-auto
   :noconfirm))

(message "==== Done loading user config")
;; https://github.com/syl20bnr/spacemacs/issues/7226
