(setq require-final-newline t)
;;;#== Evil undo granularity fix
(setq evil-want-fine-undo t)
;;;#== Completion
(global-company-mode)

(setq yas-snippet-dirs
      (append '("~/.config/hax-config/emacs/snippets"
                "~/.config/hax-config/emacs/surround-snippets")))

(setq auto-completion-private-snippets-directory
      '("~/.config/hax-config/emacs/snippets"))

(setq abbrev-file-name "~/.config/hax-config/emacs/abbrev_defs")

;; https://github.com/lewang/flx#gc-optimization
(setq gc-cons-threshold 20000000)

(setq-default evil-insert-state-cursor 'bar)
(setq mode-require-final-newline t)
(setq-default cursor-type 'bar)
(setq-default evil-escape-delay 0.2)
(setq-default sp-escape-quotes-after-insert nil)
