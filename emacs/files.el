(setq undo-tree-auto-save-history nil)
(setq undo-tree-enable-undo-in-region nil)

(defun hax/make-backup-file-name (file)
  (let ((dirname (f-join
                  hax/file::emacs-backup-dir
                  (format-time-string "%y/%m/%d"))))
    (if (not (file-exists-p dirname)) (make-directory dirname t))
    (f-join dirname (file-name-nondirectory file))))

;; https://www.emacswiki.org/emacs/BackupFiles


(defvar hax/file::cache-dir "~/.cache/hax")
(defvar hax/file::emacs-backup-dir (f-join hax/file::cache-dir "emacs-backups"))
(defvar hax/file::emacs-trash-dir (f-join hax/file::cache-dir "emacs-trash"))
(defvar hax/file::backup-file-size-limit (* 5 1024 1024))

(setq
 backup-directory-alist
 `(("" . ,hax/file::emacs-backup-dir)))

;; TODO check if max number of backups is being respected and it is
;; set to some reasonable number (I've seen backup file number ~56~
;; somewhere in direcory, don't thing I need that many)

;; TODO make messages optional - they really clutter everything make
;; some variable along the lines of
;; (hax/config::backup-file-message-actions) and check whether or not
;; it is nill

;; http://pragmaticemacs.com/emacs/auto-save-and-backup-every-save/
(defun hax/backup-on-save ()
  "Backup files every time they are saved.

Files are backed up to `' in subdirectories
\"per-session\" once per Emacs session, and \"per-save\" every
time a file is saved.

Files larger than `hax/file::backup-file-size-limit' are not
backed up."

  (let ((buffer-backed-up nil))
    (if (<= (buffer-size) hax/file::backup-file-size-limit)
        (progn
          (message "Made per save backup of %s" (buffer-name))
          (backup-buffer))
      (message "WARNING: File %s too large to backup" (buffer-name)))))


(setq
 make-backup-files t
 make-backup-file-name-function 'hax/make-backup-file-name
 backup-by-copying t     ; don't clobber symlinks
 kept-new-versions 20    ; keep 10 latest versions
 kept-old-versions 0     ; don't bother with old versions
 delete-old-versions t   ; don't ask about deleting old versions
 version-control t       ; number backups
 vc-make-backup-files t) ; backup version controlled files



;; add to save hook
;; (add-hook 'before-save-hook 'hax/backup-on-save)
;; (remove-hook 'before-save-hook 'hax/backup-on-save)
