;; FIXME Does not work, haven't really searched why
(font-lock-add-keywords 'ess-mode '(("d" 1 font-lock-warning-face)))

(defun font-lock-restart ()
  (interactive)
  (setq font-lock-mode-major-mode nil)
  (font-lock-fontify-buffer))

;; Make folds play nice with hybrid line numbering
;;(setq display-line-numbers 'visual)

;; https://stackoverflow.com/questions/22817120/how-can-i-save-evil-mode-vim-style-macros-to-my-init-el/22820324
(fset 'pad-curly-brace-with-spaces "vi{s ")

;;TODO implement
(defun shell-command-on-buffer (command)
  " NOTE: Not implemented yet! Run shell command on buffer. Two
modes is supported; (1) pipe buffer into stdin and replace it
aftewards with content from stdout; (2) save buffer in temporary
file, run command on that file and replace buffer with content of
the file"
  (interactive)
  (shell-command-on-region (point-min) (point-max) command t t))

(defun shell-command-on-buffer-file (command-string)
  "Execute command on current buffer file. `command-string' is
  `format' string with one argument - absolute file name"
  (message "Shell-command-on-buffer-file %s" (buffer-file-name))
  (shell-command (format command-string (buffer-file-name))))


(defun hax/help-mode-hook ()
  (map! :nv "l" 'backward-char)
  (map! :nv ";" 'forward-char)
  )

(add-hook 'help-mode-hook 'hax/help-mode-hook)

(defun sort-lines-nocase ()
  (interactive)
  (let ((sort-fold-case t))
    (call-interactively 'sort-lines)))

(setq ibuffer-default-sorting-mode 'major-mode)


(defun ibuffer-previous-line ()
  (interactive) (previous-line)
  (if (<= (line-number-at-pos) 2)
      (goto-line (- (count-lines (point-min) (point-max)) 2))))

(defun ibuffer-next-line ()
  (interactive) (next-line)
  (if (>=
       (line-number-at-pos)
       (- (count-lines (point-min) (point-max)) 1))
      (goto-line 3)))


(setq display-line-numbers 'visual)


(require 'json-error)

;; (defun json-pretty-print-buffer () (interactive))

(defun hax/json-mode-hook()
  (interactive)
  (add-hook 'before-save-hook 'json-pretty-print-buffer t t)
  (json-error-mode-enter))

;;; TODO write append-to-list
;;; TODO Add wrapper for adding extension-mode mapping more easily
(add-to-list 'auto-mode-alist '("\\.json_private" . json-mode))
(add-to-list 'auto-mode-alist '("\\.json.msth" . json-mode))
(add-to-list 'auto-mode-alist '("\\.gp" . gnuplot-mode))
(add-to-list 'auto-mode-alist '("\\.qstodo" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.qsnote" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.qsevent" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.qslink" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.pri" . qt-pro-mode))
(add-to-list 'auto-mode-alist '("\\.pro" . qt-pro-mode))

(add-to-list 'auto-mode-alist '("\\.splog" . prolog-mode))
(add-to-list 'auto-mode-alist '("\\.gplog" . prolog-mode))


(add-to-list 'auto-mode-alist '("\\.lex" . fundamental-mode))
(add-to-list 'auto-mode-alist '("\\.y" . fundamental-mode))
(add-to-list 'auto-mode-alist '("\\.yml" . yaml-mode))
(add-to-list 'auto-mode-alist '("\\.yaml" . yaml-mode))

(add-hook 'json-mode-hook 'hax/json-mode-hook)




;;; TODO check for current file extension and invoke corresponding
;;; debugger

;;; TODO for makefiles: get list of targets in the current file and
;;; provide helm prompt for quicker selection. Target that I'm
;;; currently on should be on the top.
(SPC-map! "arm" 'realgud:remake)


(spacemacs/toggle-visual-line-navigation-on)

(defun hax/hook::text-mode-hook ()
  (interactive)
  (setq display-line-numbers-width 4))

(add-hook 'text-mode-hook 'hax/hook::text-mode-hook)
(require 'column-marker)

;; (defun set-column-highlights ()
;;   (interactive)
;;   (funcall-interactively 'column-marker-1 75)
;;   (funcall-interactively 'column-marker-2 80)
;;   (funcall-interactively 'column-marker-3 90))
;; (remove-hook 'prog-mode-hook 'set-column-highlights)

(defun hax/hook::prog-mode-hook()
  (interactive)
  (subword-mode +1)
  (map! :nv "<S-k>" nil)
  (setq display-line-numbers-width 4))

(add-hook 'prog-mode-hook 'hax/hook::prog-mode-hook)

(yas-global-mode +1)
(yas-reload-all)

(exec-path-from-shell-initialize)
(exec-path-from-shell-copy-env "PATH")

;; TODO How to set value for specific mode?
(setq company-minimum-prefix-length 3)
(setq company-idle-delay 0.4)
(global-visual-line-mode t)

(setq-default evil-escape-key-sequence "><")

(defun get-home-dir() (expand-file-name "~"))

(defvar hax/binutils-dir "")
(setq hax/binutils-dir
      (concat (get-home-dir) "/.config/hax-software/bin"))

(defvar
  hax/internal::exec-path
  (list
   (f-join hax/file::workspace-dir "hax-nim/scripts")))

(defun get-full-util-path (utilname)
  "Return full path to the `utilname'"
  (let ((path (f-join hax/binutils-dir utilname)))
    (if (f-exists-p path) (f-join path utilname)
      (error
       (format
        "No such file %s in directory %s" utilname hax/binutils-dir)))))

(cl-defun exec-util (util-name arguments &key
                               (join-mode :posix)
                               (output-buffer "*Messages*")
                               (infile nil)
                               (clean-buffer t)
                               (ansi-colorize-buffer t)
                               )
  "Search for file with name `util-name' in
`hax/internal::exec-path' and execute it with arguments
`arguments'. `arguments' can be one either list or string. If
this is list all arguments will be joined using whitespace \" \"
and supplied as argument to executable. If list's element is nil
it will be converted into empty string. If it is another list
then two elements will be joined using either \"=\" or \":\" and
converted to string. Which separator is chosed depends on
optional argument `join-mode'. It can be either `:posix' (join
using \"=\") or `:nim' (join using \":\")

- `output-buffer': redirect stdout of the utility into given
  buffer. Supplying `nil' will discard result. This value is
  directly passed into `call-process-shell-command' as optional
  `BUFFER' argument. See it's documentation for more.

- `infile': supply file as input for program. This value is
  directly passed into `call-process-shell-command' as optional
  `INFILE' argument. See it's documentation for more.

- `clean-buffer': If \"t\" and buffer-name is not equal `string='
  to `*Messages*' run `(delete-region (point-min) (point-max))'
  on output buffer before running program.

- `ansi-colorize-buffer': after running executable apply
  `ansi-color-apply-on-region' on whole resulting buffer.
"
  (print arguments)
  (let ((fullpath (get-full-util-path util-name))
        (args-join
         (cond
          ((not arguments) "")
          ((stringp arguments) arguments)
          ((listp arguments)
           (mapconcat
            'identity
            (-map (lambda (argpair)
                    (cond ((not argpair) "")
                          ((stringp argpair) argpair)
                          ((listp argpair)
                           (if (= (length argpair) 1) (nth 0 argpair)
                             (concat
                              (nth 0 argpair)
                              (cond ((eq join-mode :posix) "=")
                                    ((eq join-mode :nim) ":"))
                              (nth 1 argpair))))))
                  arguments)
            " ")))))
    (when fullpath
      (when (and clean-buffer
                 (not (string= output-buffer "*Messages*")))
        (with-current-buffer (get-buffer-create output-buffer)
          (delete-region (point-min) (point-max))))
      ;; (print args-join)
      (call-process-shell-command
       (concat fullpath " " args-join)
       infile
       output-buffer)
      (when ansi-colorize-buffer
        (with-current-buffer output-buffer
          (ansi-color-apply-on-region (point-min) (point-max)))))))

;; (exec-util "circuit.nim" '("input.tmp.m4" "output.tmp.png"))

(defun binutil-to-string (command)
  (interactive)
  (shell-command-to-string-no-newline
   (concat hax/binutils-dir "/" command)))

(setq tuareg-match-patterns-aligned t)


(setq langtool-default-language "en-US")
(setq langtool-language-tool-jar
      (f-join hax/file::software-dir
              "langtool/languagetool-commandline.jar"))

(cl-defun reindent-to-the-line-above ()
  "Change indentation of the selected region to match line above"
  (interactive)
  (indent-to (line-above-indentation (point))))

(cl-defun newline-and-indent-same-level (&optional
                                         (indent-addition nil)
                                         (goto-eol nil))
  "Insert a newline, then indent to the same column as the current line."
  (interactive)
  (when goto-eol (goto-char (line-end-position)))
  (let ((col (save-excursion
               (back-to-indentation)
               (current-column))))
    (newline)
    (indent-to-column (if indent-addition
                          (+ indent-addition col)
                        col))))


(cl-defun new-comment-line-indent-same-level (&optional
                                              (trigger-after nil)
                                              )
  "Start new in nim comment with the same indentation as current
one"
  (interactive)
  (newline-and-indent-same-level nil t)
  (when trigger-after (funcall trigger-after)))


(cl-defun newline-and-indent-next-level (&optional
                                         (indent-addition nil)
                                         (goto-eol t)
                                         (toggle-evil-insert t))
  "Insert a newline, then indent to the same column as the next line."
  (interactive)
  (when toggle-evil-insert (evil-insert-state))
  (when goto-eol (goto-char (line-end-position)))
  (newline)
  (let ((col (save-excursion
               (forward-line)
               (back-to-indentation)
               (current-column))))

    (indent-to-column (if indent-addition
                          (+ indent-addition col)
                        col))))


(add-hook 'scad-mode-hook 'common-typos-highlight-mode)

(add-hook
 'reb-mode-hook
 (lambda () (E-mmap! 'reb-mode-map "C-c C-q" 'reb-quit)))


;; (cl-defun hax/org::global-skip-function ()
;;   )

;; (setq org-agenda-skip-function-global nil)

;; (E-mmap! elvish-mode-map "<C-return>" 'newline-and-indent-same-level)

(setq eshell-prompt-function (lambda nil (concat " > \n")))
(setq eshell-prompt-regexp " > \n")

(defun hax/hook::eshell-mode-hook ()
  "Initialize shortcuts for eshell mode"
  (interactive)
  (setenv "IN_ESHELL" "true")
  (map!
   :map eshell-mode-map
   :n "dd" #'kill-whole-line))

(add-hook 'eshell-mode-hook 'hax/hook::eshell-mode-hook)

;; (map!
;;  :map telega-msg-button-map
;;  :nv "k" 'evil-previous-line
;;  )


(setq telega-chat-fill-column 60)
(setq telega-chat-use-markdown-version 1)
(setq telega-chat-input-prompt "****\n")

(defvar hax/internal::previous-input-method nil)
(setq hax/internal::previous-input-method 'default-input-method)

;; (defvar hax/internal::repeated-backtick-press-count nil)
;; (setq hax/internal::repeated-back)

;; WIP TODO
(defun toggle-default-input-on-backtick ()
  (interactive)
  (message "hello")
  (print current-input-method)
  (print hax/internal::previous-input-method)
  (print default-input-method)
  (if (not current-input-method) ;; no changes to input method were
      ;; done so we are only using this as function for inserting
      ;; backtick.
      (progn (insert "`") (message "inserting regular quote"))
    (cond
     (; Was typing in default mode and used backtick, no need to switch.
      (and (eq default-input-method current-input-method)
           (eq hax/internal::previous-input-method default-input-method))
      ;; ===
      (insert "`")
      (message "inserting backtick as in regular mode")
      )
     (;; Typing inside backticks but manually switched to different
      ;; input method
      (and (not (eq default-input-method current-input-method))
           (not (eq hax/internal::previous-input-method default-input-method)))
      (insert "`")
      (message "inserting backtick as passthroug")
      )
     (;; Now typing in changed input mode but need to switch to default
      ;; one for typing some code
      (and (not (eq default-input-method current-input-method))
           (eq default-input-method hax/internal::previous-input-method))
      ;; ===
      (message "inserting quote, changing layout back")
      (setq hax/internal::previous-input-method current-input-method)
      (set-input-method default-input-method)
      (insert "`")
      )
     (;; Currently typing in default input method and need to switch
      ;; back for previous one
      (and (eq default-input-method current-input-method)
           (not (eq default-input-method hax/internal::previous-input-method)))
      ;; ===
      (message "switching to default input method")
      (setq hax/internal::previous-input-method current-input-method)
      (insert "`")
      )
     ))
  )

;; (load "telega-autosend.el")
(load-config "telega-autosend.el")

(defun hax/hook::telega-mode ()
  (interactive)
  (setq telega-chat-use-markdown-formatting t)
  (spacemacs/toggle-relative-line-numbers-on)
  (map! (:map telega-msg-button-map
          "k" nil
          "l" nil))

  (map!
   :map telega-chat-mode-map

   :desc "insert-shell-command"
   :n ",ic" (lambda
              ()
              (interactive)
              (insert-shell-command :formatting-mode 'markdown))
   )

  (define-key
    telega-chat-mode-map (kbd "RET") 'electric-newline-and-maybe-indent)
  (define-key
    telega-chat-mode-map (kbd "<C-return>") 'telega-chatbuf-input-send)
  (define-key
    telega-chat-mode-map
    (kbd "<f7>")
    (lambda (text)
      (interactive "scode: ")
      ;; (goto-char (line-end-position))
      (insert (concat "`" (s-trim text) "` "))))

  ;; (map! (:map telega-chat-mode-map
  ;;         "RET" 'electric-newline-and-maybe-indent))
  (message "telega mode hook")
  )

;; IDEA simply add proxy to list and dump list value to configuration
;; (serialize into string). When loading deserialize value.

;; (:server "<SERVER>" :port <PORT> :enable <BOOL> :type <PROXY-TYPE>)
;; PROXY-TYPE is:
;; (:@type "proxyTypeMtproto" :secret <SECRET-STRING>)

(defun telega-tme-open-proxy (_type _proxy)
  "Open the PROXY."
  ;; TYPE is "socks" or "proxy"
  ;; :server, :port, :user, :pass, :secret
  (add-to-list
   'telega-proxies
   (list :server (plist-get _proxy :server)
         :port (plist-get _proxy :port)
         :enable t
         :type (list
                :@type "proxyTypeMtproto"
                :secret (plist-get _proxy :secret)
                ))
   )
  (print "added proxy")
  (print telega-proxies)
  )

;; TODO add note that this file has been automatically generated from
;; variable
(defun dump-variable-to-file (var filename)
  "simplistic dumping of variables in VARLIST to a file FILENAME"
  (save-excursion
    (let ((buf (find-file-noselect filename)))
      (set-buffer buf)
      (erase-buffer)
      (insert (prin1-to-string var))
      (save-buffer)
      (kill-buffer))))

(defun save-telega.el-proxy-configuration ()
  "Write content of the `telega-proxies' into file located in
  `hax/file::local-config-dir'"
  (dump-variable-to-file
   telega-proxies
   (f-join hax/file::local-config-dir "telega-proxy-configuration.el")))

;;(load-telega.el-proxy-configuration)

(defun load-telega.el-proxy-configuration ()
  "Update value of `telega-proxies' by loading file located in
  `hax/file::local-config-dir'. Also see
  `save-telega.el-proxy-configuration'"
  (let ((path (f-join hax/file::local-config-dir
                      "telega-proxy-configuration.el")))
    (setq telega-proxies
          (car (read-from-string (get-file-contents path))))))

;; (load "~/.config/hax-local/telega-proxy-configuration.el")
(setq telega-proxies nil)

;; IDEA several shortcuts for communicating: simply enter command,
;; enter command and add some text around it (for example something
;; like 'enter command `<command>` and show me output')

(cl-defun escape-string (string &key (replace-newlines t))
  "replace control characters and escape quotes inside of the
string"
  (s-replace
   "\"" "\\\""
   (s-replace "\n" "\\n" string)))

(defun trim-newlines (string)
  "Remove newlines at the end and start of the string"
  (replace-regexp-in-string "^\n\\|\n$" "" string))

(defun get-clipboard-content-as-plaintext ()
  "Really, who could need this fucking functionality, it is not
  like I want to copy-paste *text* in emacs. I think I'm more
  interested in getting some vomit-inducing degenerative piece of
  crap for face styling, isn't it?"
  (with-temp-buffer
    (message-to-buffer (buffer-name) "%s" (simpleclip-paste))
    (get-buffer-content)))



(cl-defun insert-shell-command
    (&key (formatting-mode 'markdown)
          (add-permission t)
          (run-permission 'user)
          (language nil)
          )
  "Insert shell command from clipboard into current buffer, add
  permission sign (comment at the end of the line)"
  (let* (
         (clipboard-str (s-trim (trim-newlines
                                 (get-clipboard-content-as-plaintext))))
         (multiline (s-contains? "\n" clipboard-str))
         (delimiter-chars
          (cond ((eq formatting-mode 'markdown)
                 (if multiline
                     '("```\n" "\n```")
                   '("`" "`")))
                ((eq formatting-mode 'org-mode)
                 (if multiline
                     `((concat "#+begin_src sh" (if language language "") "\n")
                       "\n#+end_src")
                   '("`" "`")))
                ((eq formatting-mode 'code) '("\"" "\""))
                ((t (error "unknown formatting mode" formatting-mode)))))
         (res-str
          (cond ((eq formatting-mode 'code)
                 (escape-string clipboard-str))
                (t (s-replace "" "" clipboard-str))))
         (to-insert (progn
                      (message "clipboard string is %s" clipboard-str)
                      (message "res string is %s" res-str)
                      (concat " " (car delimiter-chars)
                              res-str
                              (cadr delimiter-chars) " "))))
    (insert to-insert)))


(map!
 :map emacs-lisp-mode-map
 :desc "insert-shell-command"
 :n ",ic" (lambda
            ()
            (interactive)
            (insert-shell-command :formatting-mode 'code))
 )

(add-hook 'telega-chat-mode-hook 'hax/hook::telega-mode)
(add-hook 'telega-load-hook
          (lambda ()
            (telega-notifications-mode nil)
            ))

(setq telega-notifications-mode nil)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;  org-mode latex  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;#= org-mode latex preview
(defun buffer-background-red ()
  (interactive)
  (setq buffer-face-mode-face `(:background "red" :foreground "red"))
  (buffer-face-mode 1))

(setq revert-without-query '(".*"))


(cl-defun indent-selection (start end n)
  (interactive "r\nP")
  (indent-code-rigidly start end (if n n -4))
  (setq deactivate-mark nil)
  )

(defun get-line-above (pos)
  "Get content of the line above `pos'. If `pos' is `nil' then
use `(point)' as position value"
  (save-excursion
    (when pos (goto-char pos))
    (forward-line -1)
    (thing-at-point 'line)))

(defun line-above-indentation (pos)
  "Get indentation of the line above `pos'. If `pos' is `nil' then
use `(point)' as position value"
  (save-excursion
    (when pos (goto-char pos))
    (forward-line -1)
    (current-indentation)))

(defun indent-relative-to-line-above (begin end relative-indent)
  "Indent selection to have indentation relative to the line
  above the selection"
  (let* ((above-indent (line-above-indentation begin))
         (curr-indent (save-excursion (goto-char begin) (current-indentation)))
         (compensating-indent (- (- curr-indent above-indent relative-indent))))
    ;; (message "current: %d\nabove: %d\nrelative: %d\ncompensation: %d"
    ;;          curr-indent
    ;;          above-indent
    ;;          relative-indent
    ;;          compensating-indent
    ;;          )
    (indent-selection begin end compensating-indent)))

;; TODO Work on current line if no region is selected
;; https://www.emacswiki.org/emacs/WholeLineOrRegion

(map!
 :nv "M-|"
 (lambda
   (beg end n)
   (interactive "r\nP")
   (indent-relative-to-line-above beg end 2))

 :nv "M-\\"
 (lambda
   (beg end n)
   (interactive "r\nP")
   (indent-relative-to-line-above beg end 0))

 :nv "M->" (lambda
             (beg end n)
             (interactive "r\nP")
             (indent-selection beg end 2))

 :nv "M-<" (lambda
             (beg end n)
             (interactive "r\nP")
             (indent-selection beg end -2))
 )

(defun message-to-buffer (target-buffer format-string &rest items)
  (with-current-buffer target-buffer
    (save-excursion
      (goto-char (point-max))
      (insert (apply 'format
                     (append (list (concat format-string "\n"))
                             items))))))

(define-derived-mode m4circuit-mode
  m4-mode
  "m4circuit"
  "Major more for m4 circuits")


;;;;;;;;;;;;;
(defun hax/test::test-transparency (color transparency)
  "Test 'transparent' color generation"
  (message-to-buffer
   "*scratch*"
   "|original color rgb: %s\nhex %s\ntransparent %s"
   (color-values color)
   (rgb-to-hex (color-values color))
   (rgb-to-hex (generate-transparent color))))

(defun open-file-in-clipboard ()
  "If system clipboard contains path to file open it"
  (interactive)
  (let ((path (s-trim (simpleclip-get-contents))))
    (if (file-exists-p path) (find-file path)
      (message "file at %s does not exist" path))))

;;;;;;;;;;;;;;;;;;  insert separator in emacs lisp mode  ;;;;;;;;;;;;;;;;;;


;; TODO function to refresh separator (fix padding characters if
;; center string has changed)
(cl-defun pad-string-centered
    (string target-width &key
            (padding-char " ")
            (inner-padding "="))
  ;; TODO add support for padding with multi-character strings
  "Pad string with equal (+-1) number of characters from left and
  right side"
  (let* ((str-len (length string))
         (empty-space (- target-width str-len))
         (left-pad (floor empty-space 2))
         (right-pad (- empty-space left-pad)))
    (concat (if inner-padding ;; If inner padding is present

                ;; Pad with single `pad-char',fill everything else
                ;; with `inner-padding'
                (concat padding-char
                        (s-repeat (- left-pad 1) inner-padding))
              ;; Otherwise just fill everything with `pad-char'
              (s-repeat left-pad padding-char))
            string
            (if inner-padding
                (concat (s-repeat (- right-pad 1) inner-padding)
                        padding-char)
              (s-repeat right-pad padding-char)))))

(cl-defun insert-comment-separator-medium
    (sep-name &optional (inner-padding "-"))
  "Insert medium separator spanning from beginning of the line
until column with the same number as `whitespace-line-column'"
  ;; TODO account for indentation of the line cursor is currently on
  ;; and use this for calculating total comment width.
  (interactive "sseparator string: ")
  (insert (pad-string-centered
           (if (eq (length sep-name) 0) ""
             (concat "  " sep-name "  "))
           whitespace-line-column
           :padding-char (substring comment-start 0 1)
           :inner-padding inner-padding)))

(cl-defun insert-comment-separator-large
    (sep-name &optional (inner-padding "="))
  "Insert large comment separator"
  (interactive "sseparator string: ")
  (insert-comment-separator-medium "" inner-padding)
  (insert "\n")
  (insert-comment-separator-medium sep-name inner-padding)
  (insert "\n")
  (insert-comment-separator-medium "" inner-padding))

(defun insert-comment-separator-medium-noinner (sep-name)
  (interactive "sseparator string: ")
  (insert-comment-separator-medium sep-name nil))

(map!
 :map emacs-lisp-mode-map
 :desc "Insert separator comment"
 :n ",is" 'insert-comment-separator-medium-noinner)

(defun hax/hook::lua-mode-hook ()
  (map!
   :map lua-mode-map :n ",is" 'insert-comment-separator-medium-noinner))

(add-hook 'lua-mode-hook 'hax/hook::lua-mode-hook)


(defun hax/hook::shell-mode-hook ()
  (map!
   :map shell-mode-map :n ",is" 'insert-comment-separator-medium-noinner
   ;; :map fish-mode-map :n ",is" 'insert-comment-separator-medium-noinner
   ))

(add-hook 'shell-mode-hook 'hax/hook::shell-mode-hook)
(add-hook 'fish-mode-hook 'hax/hook::shell-mode-hook)

;;;;;;;;;;;;;;;;;;  fix double-slash in helm-find-file  ;;;;;;;;;;;;;;;;;;;


(defun advice-unadvice (sym)
  "Remove all advices from symbol SYM."
  (interactive "aFunction symbol: ")
  (advice-mapc (lambda (advice _props) (advice-remove sym advice)) sym))



;; (advice-add 'helm-reduce-file-name
;;             :filter-args (lambda (arg level) (interactive)
;;                            (list (s-replace "//" "/" (car arg)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;  ivy configuration  ;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq ivy-re-builders-alist '((t . ivy--regex-fuzzy)))

;; (define-key ivy-minibuffer-map (kbd "<ESC>") 'minibuffer-keyboard-quit)
;; (define-key swiper-map (kbd "<ESC>") 'minibuffer-keyboard-quit)

;; (with-eval-after-load 'evil
;;   (define-key ivy-minibuffer-map [escape] 'minibuffer-keyboard-quit))

;;;;;;;;;;;;;;;;;;;;;;;;  relative line numbering  ;;;;;;;;;;;;;;;;;;;;;;;;

;; (setq linum-relative-current-symbol "")
;; (setq display-line-numbers-type 'visual)
;; (setq linum-relative-backend 'display-line-numbers-mode)
;;(setq display-line-numbers-width-start 4)
(setq display-line-numbers-width 4)

;;;;;;;;;;;;;;;;;;;;  org html export for inline code  ;;;;;;;;;;;;;;;;;;;;

(require 'ox)
(require 'ox-html)

(defun example-org-html-code (code contents info)
  (message "exporting from buffer: %s" (buffer-name))
  (message "buffer content:\n%s" (buffer-substring (point-min) (point-max)))
  (message "block arguments: %s"
           (org-element-property
            :parameters
            (save-excursion
              (goto-char (+ (org-element-property :begin code) 10))
              (org-element-at-point))))
  (concat "!!! exported code !!!" contents))

(org-export-define-derived-backend 'example 'html
  :translate-alist '((src-block . example-org-html-code)))

(defun example-exporter
    (&optional async subtreep visible-only body-only ext-plist)
  (interactive)
  (print "Exporthting to hax-html")
  (org-export-to-buffer 'example "* Out buffer name *"))

;;;;;;;;;;;;;;;;;;;;;;;;;;  GNU make shortcuts  ;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun hax/hook::make-mode-hook ()
  (interactive)
  (E-mmap! makefile-mode-map "<C-return>" 'newline-and-indent-same-level)
  (E-mmap! makefile-mode-map
           "<C-M-return>"
           (lambda () (interactive) (insert-commented-pipe
                                     :indentation-string "\t"
                                     :force-indentation 1
                                     )))
  (map!
   :map makefile-mode-map
   :desc "yas/awk-heredoc"
   :n ",ia" (lambda () (interactive)
              (insert-snippet-by-name "makefile-mode/awk-heredoc"))
   :n ",is" 'insert-comment-separator-medium
   )
  ;; FIXME does not work (syntax highlighting for mode)
  ;; (setq font-lock-defaults
  ;;       `((,(rx (* "#")) . 'font-lock-doc-face)
  ;;         ,@(cdr font-lock-defaults)))
  )

;; (defconst makefile-hax-font-lock-keywords
;;   (makefile-make-font-lock-keywords
;;    makefile-var-use-regex
;;    makefile-gmake-statements
;;    t
;;    ;; "^\\(?: [ \t]*\\)?if\\(n\\)\\(?:def\\|eq\\)\\>"

;;    ;; '("[^$]\\(\\$[({][@%*][DF][})]\\)"
;;    ;;   1 'makefile-targets append)

;;    ;; ;; $(function ...) ${function ...}
;;    ;; '("[^$]\\$[({]\\([-a-zA-Z0-9_.]+\\s \\)"
;;    ;;   1 font-lock-function-name-face prepend)

;;    ;; ;; $(shell ...) ${shell ...}
;;    ;; '("[^$]\\$\\([({]\\)shell[ \t]+"
;;    ;;   makefile-match-function-end nil nil
;;    ;;   (1 'makefile-shell prepend t))
;;    )
;;   )


;; (define-derived-mode makefile-hax-mode makefile-mode "HAXmakefile"
;;   "An adapted `makefile-mode' that knows about gmake."
;;   (setq font-lock-defaults
;;         `((,(rx bol (* space) "##" space (* nonl) eol) . 'font-lock-doc-face)
;;           ,@(cdr font-lock-defaults))))


(defun current-indentation ()
  "Return number of indentation characters for current line"
  (save-excursion (back-to-indentation) (current-column)))

(cl-defun insert-commented-pipe (&key
                                 (indentation-string " ")
                                 force-indentation)
  "Insert shell pipe with comment for the command"
  (interactive)
  (let ((indent (s-repeat (if force-indentation
                              force-indentation
                            (current-indentation))
                          indentation-string)))
    (goto-char (line-end-position))
    (insert " | {\n")
    (insert (concat indent "  # "))
    (save-excursion
      (insert "\n")
      (insert (concat indent "  \n"))
      (insert (concat indent "}")))
    (evil-insert-state)))


(add-hook 'makefile-mode-hook 'hax/hook::make-mode-hook)

(setq auto-mode-alist
      (cons '(".mk" . makefile-gmake-mode) auto-mode-alist))


;;;;;;;;;;;;;;;;;;;  programmatically insert snippets  ;;;;;;;;;;;;;;;;;;;;
;; TODO use ivy to select available snippets

(defun get-hax-conf-snippet (snippet-name)
  "Return absolute path for snippet located in hax config
  directory `hax-config-dir'"
  (let ((path (f-join
               (f-expand hax-config-dir)
               "emacs/snippets"
               snippet-name)))
    (if (file-exists-p path) path
      (error
       (format
        "Attemp to get non-existent snippet.
Snippet name is %s
Full snippet path is %s" snippet-name path)))))

(get-hax-conf-snippet "makefile-mode/awk-heredoc")

(defun get-file-contents (file-path)
  "Return file contents as string"
  (with-temp-buffer
    (insert-file-contents file-path)
    (buffer-string)))

;; (yas--make-template &key KEY CONTENT NAME CONDITION EXPAND-ENV
;; LOAD-FILE SAVE-FILE KEYBINDING UUID MENU-BINDING-PAIR GROUP
;; PERM-GROUP TABLE)

;; (KEY TEMPLATE NAME CONDITION GROUP VARS LOAD-FILE KEYBINDING UUID)

(cl-defun parse-yas-snippet-template
    (input-string &key (name nil))
  "Parse yas snippet template string into `yas-template'
  structure"
  (let ((parsed (with-temp-buffer
                  (insert input-string)
                  (yas--parse-template))))
    (yas--make-template :table nil
                        :key (if name name (nth 0 parsed))
                        :content (nth 1 parsed)
                        :name (nth 1 parsed)
                        :expand-env (nth 5 parsed))))

(cl-defun define-yasnippet (key expansion &key (expand-env nil))
  (yas--make-template
   :table nil
   :key key
   :content expansion
   :name key
   :expand-env expand-env))

(cl-defun yas-snippet-to-list (snippet)
  "`yas-template' structure into list. Return list of the
  following form: `(KEY TEMPLATE NAME CONDITION GROUP EXPAND-ENV
  LOAD-FILE KEYBINDING UUID SAVE-FILE)'"
  (list
   (yas--template-key snippet)
   (yas--template-content snippet)
   (yas--template-name snippet)
   (yas--template-condition snippet)
   (yas--template-group snippet)
   (yas--template-expand-env snippet)
   (yas--template-load-file snippet)
   (yas--template-keybinding snippet)
   (yas--template-uuid snippet)
   (yas--template-save-file snippet)))

(cl-defun insert-snippet-from-file
    (path &key
          (start-position nil)
          (end-position nil)
          (expansion-mode 'fixed)
          (use-evil-insert t))
  "Expand snippet from file located at `path' [string].

Text between `start-position' [integer, position in buffer] and
`end-position' [integer, position in buffer] will be deleted
before inserting template.

`expansion-mode' [list of pairs] Controls indenting applied to a
recent snippet expansion. The following values are possible:

- fixed Indent the snippet to the current column;
- auto Indent each line of the snippet with indent-according-to-mode

`evil-normal-insert' [t/nil] before inserting snippet transition
into evil-insert state
"
  (let ((template (parse-yas-snippet-template
                   (if (f-exists-p path)
                       (get-file-contents path) path))))
    (when use-evil-insert (evil-insert-state))
    (yas-expand-snippet
     template nil nil
     '((yas-indent-line 'fixed)))))

(defun insert-snippet-by-name (snippet-name)
  "Insert snippet at point. Snippet is take from
`hax-config-dir/emacs/snippets/<snippet-name>' "
  (insert-snippet-from-file
   (get-file-contents
    (get-hax-conf-snippet snippet-name))))

(defun insert-snippet-from-string (snippet-string)
  (interactive)
  (let ((template (parse-yas-snippet-template snippet-string)))
    (yas-expand-snippet template nil nil
                        '((yas-indent-line 'fixed)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  telega  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setenv "LD_LIBRARY_PATH" "/usr/local/lib/")

;;;;;;;;;;;;;;;;  optimize saving evil macro as shortcuts  ;;;;;;;;;;;;;;;;

(fset 'wrap-to-org-link
      [?v ?e ?$ ?l ?s ?\] ?v escape ?$ ?A ?\[ ?l ?i ?n ?k escape ?A escape ?v ?B ?s ?\]])

(defun paste-last-evil-macro-as-function (name)
  "Insert past evil macro as function"
  (interactive "Smacro name: ")
  (name-last-kbd-macro name)
  (insert-kbd-macro name))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  sql  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq sql-mysql-login-params
      '((user :default "root")
        (database :default "testapp")
        (server :default "localhost")
        (port :default 3308)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  fence-edit  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'fence-edit)

(defun edit-nimdoc-popup ()
  "Edit nim documentation comments in separate buffer with
  rst-mode as major"
  (interactive)
  ;; TODO account for indentation of the documentation comments
  (let ((comm-start (save-excursion (re-search-backward "##\\[" nil t)))
        (comm-end (save-excursion (re-search-forward "\\]##" nil t)))
        )
    (message "Located documentation comment in [%d %d]" comm-start comm-end)
    (when (and comm-start comm-end)
      (fence-edit-code-region
       (+ comm-start 4)
       (- comm-end 3)
       "rst"
       ))))

;;;;;;;;;;;;;;;;;;;;;;;;;  c++ comment override  ;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun c++-additional-keyword-matcher (end)
  (let (pos (case-fold-search t))
    (while (and (setq pos
                      (re-search-forward
                       (rx "--- " (*? anything) " ---") end t))
                (null (nth 3 (syntax-ppss pos)))))
    pos))

(defun c++-add-additional-keyword-matcher ()
  (font-lock-add-keywords
   nil
   '((c++-additional-keyword-matcher 0 'font-lock-warning-face t))
   'append))

(add-hook 'c++-mode-hook  'c++-add-additional-keyword-matcher)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  old section  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (defvar 3dash-matcher
;;   (rx "// --- " (group (* not-newline)) " --- //"))

;; (defvar 3dash-keywords
;;   `((,3dash-matcher 1 'org-todo t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  new section  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar 3dash-keywords
  `(
    (,(rx "// --- " (group (*? not-newline)) " --- //") 1 'org-todo t)
    (,(rx "// --- " (group (*? not-newline)) " ---" line-end) 1 'org-done t)
    (,(rx "// -- " (group (*? not-newline)) line-end) 1 'powerline-active0 t)
    ))

(eval-when-compile
  (defvar 3dash-warning-mode nil))

(define-minor-mode 3dash-warning-mode
  ""
  :lighter " ---"
  (if 3dash-warning-mode
      (font-lock-add-keywords nil 3dash-keywords 'append)
    (font-lock-remove-keywords nil 3dash-keywords))
  (when font-lock-mode
    (font-lock-flush)))

;;;;;;;;;;;;;;;;;;;;;;;;  revese region for words  ;;;;;;;;;;;;;;;;;;;;;;;;

(defun reverse-region-characters (beg end)
  "Reverse characters between BEG and END."
  (interactive "r")
  (let ((region (buffer-substring beg end)))
    (delete-region beg end)
    (insert (nreverse region))))

;;;;;;;;;;;;;;;;;;;;  whtitespace mode configuration  ;;;;;;;;;;;;;;;;;;;;;

(progn
  ;; Make whitespace-mode with very basic background coloring for whitespaces.
  ;; http://ergoemacs.org/emacs/whitespace-mode.html
  (setq whitespace-style (quote
                          (face spaces tabs newline
                                space-mark tab-mark newline-mark )))

  ;; Make whitespace-mode and whitespace-newline-mode use “¶” for end of line char and “▷” for tab.
  (setq whitespace-display-mappings
        ;; all numbers are unicode codepoint in decimal. e.g. (insert-char 182 1)
        '(
          (space-mark 32 [183] [46]) ; SPACE 32 「 」, 183 MIDDLE DOT 「·」, 46 FULL STOP 「.」
          (newline-mark 10 [182 10]) ; LINE FEED,
          (tab-mark 9 [8677 9] [92 9]) ; tab
          )))

(defun infer-indentation-style ()
  "if our source file uses tabs, we use tabs, if spaces spaces,
   and if neither, we use the current indent-tabs-mode"
  (interactive)
  (let ((space-count (how-many "^  " (point-min) (point-max)))
        (tab-count (how-many "^\t" (point-min) (point-max))))
    (if (> space-count tab-count) (setq indent-tabs-mode nil))
    (if (> tab-count space-count) (setq indent-tabs-mode t))))

(defun use-indentation-tab ()
  "Use tabs for indentation"
  (interactive) (setq indent-tabs-mode t))

;;;;;;;;;;;;;;;;;;;;;;;  buffer-local keybindings  ;;;;;;;;;;;;;;;;;;;;;;;;

(define-minor-mode hax/mode::buffer-local-bind-mode
  "Minor mode to simulate buffer local keybindings."
  :init-value nil
  :lighter nil
  :keymap '()
  )

(defmacro bind-buffer-local (key function)
  "Bind key for buffer local mode"
  `(define-key hax/mode::buffer-local-bind-mode-map (kbd ,key) ,function))



(defun tab-insert-single-tab ()
  (interactive)
  (hax/mode::buffer-local-bind-mode 1)
  (bind-buffer-local "<tab>" (lambda() (interactive) (insert "\t")))
  )

;;;;;;;;;;;;;;;;;;;;;;;  spice-mode configuration  ;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'auto-mode-alist '("\\.net" . spice-mode))

;;;;;;;;;;;;;;;;;;;;;;;;  doxygen comment editing  ;;;;;;;;;;;;;;;;;;;;;;;;

(remove-hook 'poporg-edit-hook 'org-mode)
(add-hook 'poporg-edit-hook 'markdown-mode)

(defun poporg-org-mode()
  "Open documentation comment in `org-mode' buffer"
  (interactive)
  (remove-hook 'poporg-edit-hook 'markdown-mode)
  (remove-hook 'poporg-edit-hook 'rst-mode)
  (add-hook 'poporg-edit-hook 'org-mode)
  (poporg-dwim))

;; (global-set-key (kbd "C-c \\") 'poporg-dwim)
(global-set-key (kbd "<s-tab>") 'other-window)

;;;;;;;;;;;;;;;;;;;;;;;;;;;  blackletter font  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defface blackletter-base
  '((default
      :inherit default
      :family "Chomsky monospacified for JetBrains Mono"
      ))
  "Blackletter font"
  :group 'font-lock-faces)

(defface hax/face::blackletter-magenta
  '((t
     :inherit blackletter-base
     :foreground "magenta"
     ))
  "Magenta blackletter"
  :group 'hax/face::group-extra)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;  evil-collection  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(evil-collection-init)

;;;;;;;;;;;;;;;;;;;;;  interactive lisp development  ;;;;;;;;;;;;;;;;;;;;;;
(setq inferior-lisp-program "/bin/sbcl --noinform")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  erc  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun hax/hook::erc-mode-hook()
  (interactive)
  (E-mmap! erc-mode-map "<return>" (lambda () (interactive) (insert "\n")))
  (E-mmap! erc-mode-map "<C-return>" 'erc-send-current-line))

(add-hook 'erc-mode-hook 'hax/hook::erc-mode-hook)

;;;;;;;;;;;;;;;;;;;;;;;;  emacsclient frame fixup  ;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;  save buffer states between sessions  ;;;;;;;;;;;;;;;;;;
;; TODO TODO TODO FIXME fucking degenerative bullshit - how the fuck can I
;; prevent this little stinky piece of vomit from constantly nagging me for
;; pid-locked cached files? I have ONE (1) fucking emacs instance running -
;; can you just be so nice and just save your moronic cache

;; (desktop-save-mode 1)
;; (savehist-mode 1)
;; (desktop-read)
;; (setq desktop-restore-frames nil)

;; (defun save-current-desktop ()
;;   (interactive)
;;   ;; Don't call desktop-save-in-desktop-dir, as it prints a message.
;;   (if (eq (desktop-owner) (emacs-pid))
;;       (desktop-save desktop-dirname)))

;; ;; remove desktop after it's been read
;; (add-hook 'desktop-after-read-hook
;;           '(lambda ()
;;              ;; desktop-remove clears desktop-dirname
;;              (setq desktop-dirname-tmp desktop-dirname)
;;              (desktop-remove)
;;              (setq desktop-dirname desktop-dirname-tmp)))

;; (add-hook 'auto-save-hook 'save-current-desktop)

;; (defun exists-saved-session ()
;;   (file-exists-p (concat desktop-dirname "/" desktop-base-file-name)))

;; (defun restore-save-desktop-noprompt ()
;;   "Restore saved desktop session without prompting"
;;   (if (exists-saved-session) (session-restore)
;;     (warn "No saved session found - desktop not restored")))



;; (add-hook 'after-init-hook 'restore-save-desktop-noprompt)

;;;;;;;;;;;;;;;;;;;;  running emacs as sytemd service  ;;;;;;;;;;;;;;;;;;;;
(defun server-shutdown ()
  "Save buffers, Quit, and Shutdown (kill) server"
  (interactive)
  ;; Force to always overwrite desktop regardless of modification time
  (save-buffers-kill-emacs))

;;;;;;;;;;;;;;;;;;;;;;;;;  server theme loading  ;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun disable-all-custom-themes ()
  "Disable all custom themes"
  (interactive)
  (while custom-enabled-themes
    (disable-theme (car custom-enabled-themes))))

(defun prompt-select-theme ()
  "Interactively select theme from list of installed themes"
  (list
   (intern (completing-read "Load custom theme: "
                            (mapcar 'symbol-name
                                    (custom-available-themes))))
   nil nil))

(defun reselect-theme ()
  "Unload current theme and interactively select new one"
  (interactive)
  (let ((theme (prompt-select-theme)))
    (print theme)
    (disable-all-custom-themes)
    (load-theme (car theme) t)))

(SPC-map! "Tr" 'reselect-theme)

(when nil
  (defvar hax/theme 'cyberpunk)
  (defvar hax/theme-window-loaded nil)
  (defvar hax/theme-terminal-loaded nil)

  (defun hax/hook::after-frame-hook (frame)
    (interactive)
    (select-frame frame)
    (let ((font-name "JetBrains Mono-12"))
      (setq default-frame-alist '((font . font-name)
                                  (vertical-scroll-bars . nil)
                                  (vertical-scroll-bar . nil)))

      (set-face-attribute 'default nil :font font-name)
      (set-face-attribute 'default nil :height 100)
      (set-frame-font font-name nil t))

    ;; (if (window-system frame)
    ;;     (unless hax/theme-window-loaded
    ;;       (if hax/theme-terminal-loaded
    ;;           (enable-theme hax/theme)
    ;;         (load-theme hax/theme t))
    ;;       (setq hax/theme-window-loaded t))
    ;;   (unless hax/theme-terminal-loaded
    ;;     (if hax/theme-window-loaded
    ;;         (enable-theme hax/theme)
    ;;       (load-theme hax/theme t))
    ;;     (setq hax/theme-terminal-loaded t)))
    )

  (add-hook 'after-make-frame-functions 'hax/hook::after-frame-hook))


;;;;;;;;;;;;;;;;;  save undo history when closing buffer  ;;;;;;;;;;;;;;;;;
(global-undo-fu-session-mode)


;;;;;;;;;;;;;;;;;  origami fold all nodes 2+ levels deep  ;;;;;;;;;;;;;;;;;


;; (progn
;;   (origami-open-all-nodes (get-buffer "tHalgorithm.nim"))
;;   (origami-close-all-nodes (get-buffer "tHalgorithm.nim")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  IRC  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq erc-nick '("haxscramper"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;  autoinsert bash  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(eval-after-load 'autoinsert
  '(define-auto-insert
     '("\\.sh\\'" . "bash auto-insert")
     '("bash auto-insert"
       "#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
set -o nounset
set -o errexit"
       )
     )
  )

;;;;;;;;;;;;;;;;;  wrap selection in controls sequences  ;;;;;;;;;;;;;;;;;;
(defun wrap-region (begin end delim-left delim-right)
  "Wrap `[begin, end]' region in `delim-left' and `delim-right'"
  (save-excursion
    (goto-char end)
    (insert delim-right))
  (save-excursion
    (goto-char begin)
    (insert delim-left)))

(setq ansi-foreground-color-codes
      '(
        ("Default" .        "\\e[39m")
        ("Black" .          "\\e[30m")
        ("Red" .            "\\e[31m")
        ("Green" .          "\\e[32m")
        ("Yellow" .         "\\e[33m")
        ("Blue" .           "\\e[34m")
        ("Magenta" .        "\\e[35m")
        ("Cyan" .           "\\e[36m")
        ("Light gray" .     "\\e[37m")
        ("Dark gray" .      "\\e[90m")
        ("Light red" .      "\\e[91m")
        ("Light green" .    "\\e[92m")
        ("Light yellow" .   "\\e[93m")
        ("Light blue" .     "\\e[94m")
        ("Light magenta" .  "\\e[95m")
        ("Light cyan" .     "\\e[96m")
        ("White" .          "\\e[97m")))

(setq ansi-background-color-codes
      '(
        ("Default" "\\e[49m" )
        ("Black" . "\\e[40m")
        ("Red" . "\\e[41m")
        ("Green" . "\\e[42m")
        ("Yellow" . "\\e[43m")
        ("Blue" . "\\e[44m")
        ("Magenta" . "\\e[45m")
        ("Cyan" . "\\e[46m")
        ("Light gray" . "\\e[47m")
        ("Dark gray" . "\\e[100m")
        ("Light red" . "\\e[101m")
        ("Light green" . "\\e[102m")
        ("Light yellow" . "\\e[103m")
        ("Light blue" . "\\e[104m")
        ("Light magenta" . "\\e[105m")
        ("Light cyan" . "\\e[106m")
        ("White" . "\\e[107m")))

(defun get-alist-keys (alist) (mapcar (lambda (pair) (car pair)) alist))

(defun string-alist-get (key list)
  (cdr (assoc key list #'string-equal)))

(defun ivy-select-alist (prompt alist)
  (string-alist-get
   (ivy-read prompt (get-alist-keys alist)) alist))

(cl-defun wrap-region-select-delims
    (begin end delim-list &key
           (default-left "")
           (default-right "")
           (left-prompt "Left delimiter")
           (right-prompt "Right delimiter"))

  (let ((selected-left
         (if (not (eq default-left nil))
             default-left
           (ivy-select-alist left-prompt delim-list)))
        (selected-right
         (if default-right default-right
           (ivy-select-alist right-prompt delim-list))))
    (wrap-region begin end selected-left selected-right)))

(defun wrap-region-ansi-foreground (begin end)
  (interactive "r")
  (wrap-region-select-delims
   begin end ansi-foreground-color-codes
   :default-left nil
   :default-right "\\e[39m"
   :left-prompt "Foreground color"))


(defun wrap-region-ansi-background (begin end)
  (interactive "r")
  (wrap-region-select-delims
   begin end ansi-background-color-codes
   :default-left nil
   :default-right "\\e[49m"
   :left-prompt "Background color"))

(setq ansi-text-properties
      '(
        ("Bright" . ("\\e[1m" "\\e[21m"))
        ("Dim" . ("\\e[2m" "\\e[22m"))
        ("Italic" . ("\\e[3m" "\\e[23m"))
        ("Underlined" . ("\\e[4m" "\\e[24m"))
        ("Blink" . ("\\e[5m" "\\e[25m"))
        ("Reverse" . ("\\e[7m" "\\e[27m"))
        ("Hidden" . ("\\e[8m" "\\e[28m"))))

(defun wrap-region-ansi-attributes (begin end)
  (interactive "r")
  (let ((attribs (ivy-select-alist "Attributes:" ansi-text-properties)))
    (wrap-region
     begin end
     (car attribs)
     (cadr attribs))))

;; asfas dfas df

;;;;;;;;;;;;;;;;;;;;;;;;;  Show images in eshell  ;;;;;;;;;;;;;;;;;;;;;;;;;
;; CREDIT: https://emacs.stackexchange.com/questions/3432/display-images-in-eshell-with-iimage-mode
(defun hax/iimage-mode-refresh--eshell/cat (orig-fun &rest args)
  "Display image when using cat on it."
  (let ((image-path (cons default-directory iimage-mode-image-search-path)))
    (dolist (arg args)
      (let ((imagep nil)
            file)
        (with-silent-modifications
          (save-excursion
            (dolist (pair iimage-mode-image-regex-alist)
              (when (and (not imagep)
                         (string-match (car pair) arg)
                         (setq file (match-string (cdr pair) arg))
                         (setq file (locate-file file image-path)))
                (setq imagep t)
                (add-text-properties 0 (length arg)
                                     `(display ,(create-image file)
                                               modification-hooks
                                               (iimage-modification-hook))
                                     arg)
                (eshell-buffered-print arg)
                (eshell-flush)))))
        (when (not imagep)
          (apply orig-fun (list arg)))))
    (eshell-flush)))

(advice-add 'eshell/cat :around #'hax/iimage-mode-refresh--eshell/cat)

;;;;;;;;;;;;;;;  Custom operator highlighting overlay mode  ;;;;;;;;;;;;;;;

(defvar hax/internal::custom-op-any-regex
  '(|
    "="     "+"     "-"     "*"     "/"     "<"     ">"
    "@"     "$"     "~"     "&"     "%"     "|"
    "!"     "?"     "^"     "."     ":"     "\\"))

(when t
  (defvar
    hax/mode::operator-font-highlights
    `(;; `OP10' Built-in operators
      (,(rx (and
             (any "^" "$") ;; Custom
             (+ (eval hax/internal::custom-op-any-regex))))
       . 'hax/face::boxed::blue-fringes)
      (,(rx (| "^" "$")) . 'hax/face::boxed::blue-fringes) ;; Built-in

      ;; `OP9'
      (,(rx (and ;; Custom
             (any "/" "\\" "*" "%")
             (+ (eval hax/internal::custom-op-any-regex))))
       . 'hax/face::boxed::blue-fringes)
      (,(rx (| "*" "/" " div " " mod " " shl " " shr " "%")) .
       'hax/face::boxed::blue-fringes) ;; Built-in

      ;; `OP8'
      (,(rx (and
             (any "+" "-" "~" "|")
             (+ (eval hax/internal::custom-op-any-regex))))
       . 'hax/face::boxed::green-bold-boxed)
      (,(rx (| "+" "-")) . 'hax/face::boxed::green-bold-boxed)

      ;; `OP7'
      (,(rx (and
             (any "&")
             (+ (eval hax/internal::custom-op-any-regex))))
       . 'hax/face::boxed::mangenta-with-pink-box)
      (,(rx (| "&")) . 'hax/face::boxed::mangenta-with-pink-box)

      ;; `OP6'
      (,(rx (and
             (any ".")
             (+ (eval hax/internal::custom-op-any-regex))))
       . 'hax/face::boxed::green-bold-boxed)
      (,(rx (| "..")) . 'hax/face::boxed::green-bold-boxed)

      ;; `OP5'
      (,(rx (and
             (any "=" "<" ">" "!")
             (+ (eval hax/internal::custom-op-any-regex))))
       . 'hax/face::simple::bold-green)
      (,(rx (| "==" "=<" "<" ">=" ">" "!="
               " in " " notin " " is " " isnot " " not " " of " " as "))
       . 'hax/face::simple::bold-dark-green)

      ;; `OP4'
      (,(rx (| " and ")) . 'hax/face::simple::bold-orange)

      ;; `OP3'
      (,(rx (| " or " " xor ")) . 'hax/face::bold-red)

      ;; `OP2'
      (,(rx (and
             (any "@" ":" "?")
             (+ (eval hax/internal::custom-op-any-regex))))
       . 'hax/face::bold-red)

      ;; `OP1'
      (,(rx (and
             (any "<" ">" "!" "=" "~" "?")
             (* (eval hax/internal::custom-op-any-regex))
             (any "=")))
       . 'hax/face::simple::bold-light-blue)

      ;; `OP0'
      (,(rx
         (and
          (* (eval hax/internal::custom-op-any-regex))
          (| "->" "~>" "=>")))
       . 'hax/face::simple::bold-dark-green)
      ))

  (defvar hax/mode::operator-font nil)
  (define-minor-mode hax/mode::operator-font
    "Font lock overlay for languages with custom operators allowed.

Originally made for nim, but should provide sensible highlighting
for all languages"
    :lighter "--"
    (if hax/mode::operator-font
        (font-lock-add-keywords nil hax/mode::operator-font-highlights 'append)
      (font-lock-remove-keywords nil hax/mode::operator-font-highlights))
    (if font-lock-mode (font-lock-flush) (font-lock-mode))))

;;;;;;;;;;;;;;;;;;;;  Separator comments highlighting  ;;;;;;;;;;;;;;;;;;;;
(when t
  (defvar nim-separator-comments-keywords
    `(
      (,(rx "#" (>= 4 "=")
            (group (*? not-newline))
            (>= 4 "=") "#") 1 'hax/face::boxed::orange-bold-boxed t)

      (,(rx "# ~~~~ "
            (group (*? not-newline))
            " ~~~~ #") 1 'hax/face::boxed::green-bold-boxed t)

      (,(rx "#" (>= 4 "*")
            (group (*? not-newline))
            (>= 4 "*") "#") 1 'hax/face::boxed::red-bold-boxed t)))

  (eval-when-compile
    (defvar nim-separator-comments-rx-highlight-mode nil))

  (define-minor-mode nim-separator-comments-rx-highlight-mode
    ""
    :lighter " ---"
    (if nim-separator-comments-rx-highlight-mode
        (font-lock-add-keywords
         nil nim-separator-comments-keywords 'append)
      (font-lock-remove-keywords nil nim-separator-comments-keywords))
    (when font-lock-mode
      (font-lock-flush))))


(defvar nim-doc-comments-keywords
  `(
    (,(rx (group "# @@@ " (*? not-newline) " @@@ #"))
     1 'org-block-begin-line t)))

(eval-when-compile
  (defvar nim-doc-comments-rx-highlight-mode nil))

(define-minor-mode nim-doc-comments-rx-highlight-mode
  ""
  :lighter " ---"
  (if nim-doc-comments-rx-highlight-mode
      (font-lock-add-keywords
       nil nim-doc-comments-keywords 'append)
    (font-lock-remove-keywords nil nim-doc-comments-keywords))
  (when font-lock-mode
    (font-lock-flush)))

;;;;;;;;;;;;;;;;;;;;;;;;;  Force nimble-rebuild  ;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro make-hax-script-callback (script-file)
  `(lambda () (interactive)
     (shell-command (f-join (get-home-dir)
                            ".config/hax-config/scripts" ,script-file))))

(defmacro make-hax-doc-line-callback(callback-insert)
  "Insert new comment line with function argument documentation"
  `(lambda () (interactive)
     (new-comment-line-indent-same-level
      (make-snippet-callback ,callback-insert))))




(global-set-key (kbd "<s-pause>") (make-hax-script-callback "entr-space.sh"))
(global-set-key (kbd "<s-Scroll_Lock>") (make-hax-script-callback "kitty-repeat.sh"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;  Ivy deafult input  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; https://oremacs.com/2016/01/06/ivy-flx/
(setq ivy-initial-inputs-alist nil)

;;;;;;;;;;;;;;;;  Draw underline comment on selected text  ;;;;;;;;;;;;;;;;

(defun column-at-point (pos) (save-excursion (goto-char pos) (current-column)))
;; (defun line-at-point (pos) (save-excursion (goto-char pos) ()))
(defun comment-line-p ()
  "Check if current line starts with comment"
  (save-excursion
    (beginning-of-line)
    (looking-at-p comment-start)))

(defun end-of-buffer-p () (eq (point) (point-max)))

(defun next-comment-line-p ()
  (save-excursion
    (forward-line)
    (comment-line-p)))

(defun current-line-len ()
  "Get lenght of current line"
  (- (line-end-position) (line-beginning-position)))

(defun buffer-line-count () (line-number-at-pos (point-max)))

(defun set-at-coords (line column string)
  "Set character at `line':`column' extending buffer if necessary"
  (save-excursion

    ;; (if (> (+ line 1) (line-number-at-pos (point-max)))
    ;;     (progn
    ;;       (goto-char (point-max))
    ;;       (dolist (l (number-sequence (line-number-at-pos (point-max)) (+ line 1)))
    ;;         (insert "\n")))
    ;;   (goto-line line))
    (goto-line line)
    (when (> column (current-line-len))
      (message "Inserting additional")
      (goto-char (line-end-position))
      (insert (s-repeat (- column (current-line-len)) " ")))
    (move-to-column column)
    (delete-char (length string))
    (insert string)))


(defun draw-underline (begin end char)
  "Draw underine for text in `begin'-`end' range using `char'"
  (dolist (col (number-sequence (column-at-point begin)
                                (column-at-point (- end 1))))
    (set-at-coords (+ (line-number-at-pos begin) 1) col char)))

(defun find-next-noncomment-line ()
  (save-excursion
    (while (and (comment-line-p) (not (end-of-buffer-p)))
      (forward-line))
    (line-number-at-pos)
    ))


(cl-defun start-insert-underline-comment
    (begin end &key
           (comment-string comment-start)
           (underline-char "^"))
  ;; TODO simplify writing multiline annotations
  "
Easily insert things like that:

some text you want to comment on
;    ^^^^             ^^^^^^^
;    |                |
;    |                Your comment
;    |
;    More awesome comments
"

  (interactive "r")
  (let ((padding (s-repeat (- (column-at-point begin) (length comment-string)) " ")))
    (if (not (next-comment-line-p)) ;; Insert underline directly
        (progn
          (goto-char (line-end-position))
          (insert "\n" comment-string
                  padding
                  (s-repeat (- end begin) underline-char))
          (insert "\n" comment-string padding "|")
          (insert "\n" comment-string padding)
          (evil-insert-state))
      ;; Draw underline and search for non-comment line
      (draw-underline begin end underline-char)
      (forward-line)
      (let ((nextline (find-next-noncomment-line)))
        (dolist (line (number-sequence
                       (+ (line-number-at-pos begin) 2)
                       nextline))
          (set-at-coords line (column-at-point begin) "|")
          (set-at-coords line 0 comment-start))
        (goto-line nextline))
      (move-end-of-line nil)
      (insert "\n" comment-string padding)
      (evil-insert-state))))

;;;;;;;;;;;;;;;;  Insert commonly used unicode characters  ;;;;;;;;;;;;;;;;

(defvar common-unicode-chars-alist
  '(("alpha small" . "α")
    ("beta small" . "β")
    ("delta small" . "δ")
    ("epsilon small" . "ε")
    ("theta" . "θ")
    ("lambda small" . "λ")
    ("u greek" . "μ")
    ("pi small greek" . "π")
    ("phi small" . "φ")
    ("psi small" . "ψ")
    ("omega greek" . "Ω")
    ("bullet | fat dot" . "•")
    ("circled X" . "⮾")
    ("circled bulled" . "⦿")
    ("medium circle" . "⚫")
    ("large circle" . "⬤")
    ("epsilon large" . "Ɛ")
    ("up down arrow" . "↕")
    ("left right arrow" . "↔")
    ))

(defun select-common-unicode-char ()
  "Interactively select and insert character from
`common-unicode-chars-alist'"
  (interactive)
  (insert
   (ivy-select-alist "Letter name: " common-unicode-chars-alist)))

;;;;;;;;;;;;;;;;;;;;;;;;  Org-mode gitbook export  ;;;;;;;;;;;;;;;;;;;;;;;;
(require 'org-gitbook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  Autoinsert  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun hax/emacs-lisp-autoinsert ()
  (interactive)
  (insert "Hello world")
  )

(defun declare-ext-autoinsert (ext callback)
  "Associate auto-insert callback `callback' with file extension
`ext'. After file is created use `on-file-callback'"
  (let ((ext-regex (concat "\\." ext)))
    (if (assoc ext-regex auto-insert-alist)
        (setf (cdr (assoc ext-regex auto-insert-alist)) callback)
      (add-to-list 'auto-insert-alist (cons ext-regex callback)))))

;; (add-to-list 'auto-insert-alist '("\\.el" nil))
;; (assoc "\\.el" auto-insert-alist)

;; (message "%s" auto-insert-alist)
;; (-mapcat
;;  (lambda (dir)
;;    (let ((s-dir (concat dir "/" "emacs-lisp-mode" ;; mode-name
;;                         )))
;;      (message s-dir)
;;      (if (f-exists-p s-dir)
;;          (f-files s-dir)
;;        nil)
;;      ))
;;  ;; '("cunt" "hitler" "hello" "fuckoff")
;;  (yas-snippet-dirs)
;;  )

(sequencep yas-snippet-dirs)

(when t
  (defun yas-select-mode-snippets (mode-name accept-pred)
    "Interactively select snippet file for mode described as
  `mode-name'. Snippets are taken from `yas-snippet-dirs' and
  filtered using `accept-pred'"
    (let* ((snippet-files
            (-filter accept-pred
                     (-mapcat
                      (lambda (dir)
                        (let ((s-dir (concat dir "/" mode-name)))
                          (if (f-exists-p s-dir)
                              (f-files s-dir)
                            nil)))
                      (yas-snippet-dirs))))
           (files-alist (-map (lambda (file)
                                (cons (f-base file) file))
                              snippet-files))
           )
      (cond
       ((= 1 (length files-alist)) (cdar files-alist))
       ((= 0 (length files-alist)) nil)
       (ivy-select-alist "template" files-alist))
      ;; (if (> 2 (length files-alist))
      ;;     (cdar files-alist)
      ;;   (ivy-select-alist "template" files-alist))
      ))

  (defun yas-insert-mode-snippets (mode-name accept-pred)
    (insert-snippet-from-file (yas-select-mode-snippets
                               mode-name
                               accept-pred)))

  (with-temp-buffer
    (yas-minor-mode)
    (yas-insert-mode-snippets
     "emacs-lisp-mode"
     (lambda (file) (s-starts-with-p "template" (f-base file))))
    ;; (message "[[%s]]" (buffer-substring (point-min) (point-max)))
    ))

;; (yas-select-mode-snippets
;;  "emacs-lisp-mode"
;;  (lambda (file) (s-starts-with-p
;;                  "template" (f-base file))))

(when t
  (defmacro declare-ext-template-insert (ext mode-name-str)
    `(declare-ext-autoinsert
      ,ext (lambda ()
             (message "---%s---" ,mode-name-str)
             (yas-insert-mode-snippets
              ,mode-name-str
              (lambda (file) (s-starts-with-p
                              "template" (f-base file)))))))

  )

(declare-ext-template-insert "el" "emacs-lisp-mode")
;; (eval-after-load 'autoinsert
;;   (declare-ext-template-insert "el" "emacs-lisp-mode")
;;   )
;; (declare-ext-autoinsert
;;  "el" (lambda ()
;;         (yas-expand-snippet "
;; sfadf asdfa $1
;; ")
;;         ))

(defun chmod-+x-current-file ()
  (interactive) (chmod (buffer-file-name) #o755))


;; (insert-snippet-from-file "/home/test/.config/hax-config/emacs/snippets/emacs-lisp-mode/template-default")

;;;;;;;;;;;;;;;;;;;;;  Picture-mode better shortcuts  ;;;;;;;;;;;;;;;;;;;;;

;; (eval-after-load "picture-mode.el"
;;   (map!
;;    :mode picture-mode
;;    "<C-left>" 'picture-movement-left
;;    "<C-right>" 'picture-movement-right
;;    "<C-down>" 'picture-movement-down
;;    "<C-up>" 'picture-movement-up
;;    :v "x" 'kill-rectangle
;;    :v "p" 'yank-rectangle))

;;;;;;;;;;;;;;;;;;;;;;;;;  Ansi color for buffer  ;;;;;;;;;;;;;;;;;;;;;;;;;
(defun display-ansi-colors ()
  (interactive)
  (ansi-color-apply-on-region (point-min) (point-max)))

(setq max-mini-window-height 1)
(setq window-min-height 6)


;; ;;;;;;;;;;;;;;;;;  Sane bindings for rectangle commands  ;;;;;;;;;;;;;;;;;;
;; (map!
;;  :desc "Copy rectangle"
;;  :v "C-x r y" 'copy-rectangle-as-kill
;;  :desc "Paste rectangle"
;;  :v "C-x r p" 'yank-rectangle
;;  )

;; (setq erc-prompt-for-nickserv-password nil)

;; (setq erc-autojoin-channels-alist
;;       '(("freenode.net" "#nim" "#nim-internals")))

;; (load "~/.ercpass")
