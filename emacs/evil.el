;;;#== Navigation
(define-evil-normal-visual-key "l" 'backward-char)
(define-evil-normal-visual-key ";" 'forward-char)
(define-evil-normal-visual-key "o" 'evil-insert)
(define-evil-normal-visual-key "h" 'evil-repeat-search)

(global-set-key [mouse-4] 'scroll-down-line)
(global-set-key [mouse-5] 'scroll-up-line)

;;;#== Undo/redo manipulation
(define-key evil-normal-state-map "u" 'undo-fu-only-undo)
(define-key evil-normal-state-map "r" 'undo-fu-only-redo)

;;;#== Quitting and writing files
(define-key evil-normal-state-map (kbd "ww") 'save-buffer)
(define-key evil-normal-state-map (kbd "w RET") 'save-buffer)


(spacemacs/set-leader-keys "j2" 'evil-avy-goto-char-2)
(spacemacs/set-leader-keys "j1" 'evil-avy-goto-word-1)
(spacemacs/set-leader-keys "j!" 'evil-avy-goto-subword-1)
(spacemacs/set-leader-keys "j0" 'evil-avy-goto-word-0)
(spacemacs/set-leader-keys "j)" 'evil-avy-goto-subword-0)

(map! :nv "h" 'evil-avy-goto-char-2)

(use-package undo-fu
  :config
  (define-key evil-normal-state-map "u" 'undo-fu-only-undo)
  (define-key evil-normal-state-map "r" 'undo-fu-only-redo))
