(evil-ex-define-cmd
 "W[write]"
 'save-all-buffers)

(evil-ex-define-cmd
 "Wkill[write buffers kill emacs]"
 'save-buffers-kill-emacs)

(evil-ex-define-cmd
 "WR[write reload-emacs]"
 (lambda()
   (interactive)
   (save-all-buffers)
   (dotspacemacs/sync-configuration-layers)))

(evil-ex-define-cmd
 "wq[pass]"
 (lambda()
   (interactive)
   (save-buffer)
   (kill-this-buffer)))

(evil-ex-define-cmd
 "q[pass]"
 (lambda()
   (interactive)
   (unless (buffer-modified-p) (kill-this-buffer))))

(evil-ex-define-cmd
 "q![uit]"
 (lambda()
   (interactive)
   (kill-this-buffer)))

(message "Done loading evil ex commands")
