;; TODO Make available only in visual/visual block mode
;;(spacemacs/declare-prefix "E" "edit")
;;(spacemacs/set-leader-keys "Emf" 'move-region-to-file)

(defun move-region-to-file()
  "Append current active region to the file. File is selected
  using helm completion."
  (interactive)
  (when (region-active-p)
    (let ((region-text (buffer-substring (region-beginning) (region-end)))
          (target-file (helm-find-files-1 (helm-current-directory))))
      (unless target-file
        (append-to-file (region-beginning) (region-end) target-file)
        (delete-region (region-beginning) (region-end))))))
