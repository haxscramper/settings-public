(defmacro SPC-prefix! (prefix name)
  "Declare spacemacs prefix. This is helper function"
  `(spacemacs/declare-prefix (kbd ,prefix) ,name))

(auto-fill-mode -1)
(electric-indent-mode -1)
(electric-indent-local-mode -1)
(load-theme 'moe-dark)
(setq vc-follow-symlinks t) ;; Follow vc symlinks
(setq yas-snippet-dirs
      (list
       (concat config-file-folder "snippets")))

;;;#=== Rule 75
(require 'whitespace)
(setq whitespace-line-column 75)
;; (setq-default whitespace-line-column 75 whitespace-style '(face lines-tail))
(setq whitespace-style '(face lines-tail))
(setq-default fill-column 75)


(defun delete-spacemeacs-leader-key (key-name)
  "Delete spacemacs leader key by settings it to nil"
  (spacemacs/set-leader-keys key-name nil))

(defun define-evil-normal-visual-key (kbd-combination target-function)
  "Define key for evil-normal-state-map and evil-visual-state-map"
  (define-key evil-normal-state-map (kbd kbd-combination) target-function)
  (define-key evil-visual-state-map (kbd kbd-combination) target-function))

(defun save-all-buffers ()
  (interactive)
  (save-some-buffers t))

(defun define-all-mode-key (kdb-combination target-function)
  (define-evil-normal-visual-key kdb-combination target-function)
  (global-set-key (kbd kdb-combination) target-function))

;; FIXME Does not generate correct string
(defun internal/random-base-64 (str-length)
  "Generate random base-64 string with with `str-length` characters."
  (let ((char-list "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwx yz0123456789+/")
        (result ""))
    (dotimes (_ str-length)
      (let ((i (% (abs (random)) (length char-list))))
        (setq result  (concat result (substring char-list i (+ i 1)))))) result))



(defun 100-random-chars (num)
  "Insert 100 random characters at (point)"
  (interactive "P")
  (if (equal num nil) (setq num 1))
  (insert (internal/random-base-64 (* 100 num))))
