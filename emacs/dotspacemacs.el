(setq evil-want-keybinding nil)
(setq package-archives '(("gnu" . "http://mirrors.163.com/elpa/gnu/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")))

;; TODO remove XXXX
(defvar hax-config-dir
  "~/.config/hax-config"
  "Path to root of the configuration")

(defvar config-file-folder
  (concat hax-config-dir "/emacs")
  "Path to emacs config folder")

(defvar hax/file::config-dir hax-config-dir
  "should be equal to `hax-config-dir'. Defied for naming
  consistency, prefer this over shortened version")

(defvar hax/file::local-config-dir
  "~/.config/hax-local"
  "Path to root of the local configuration directory")

(defvar hax/file::software-dir
  "~/.config/hax-software"
  "Directory with user-installed software that is not available
  through package manager. Things like `plantuml.jar' for
  example")

(defvar hax/file::workspace-dir "~/workspace"
  "Code worksace directory")

(defvar hax/file::defaultdirs-dir "~/defaultdirs"
  "Directory with images/music/documents etc.")

;; TODO_DOC write better documentation for file organization tooling

;; TODO add support for reading list of files
(defun load-config (file-name)
  (load (concat config-file-folder "/" file-name)))
;
; (defvar my/device-specific-config "~/.config/kamanji/emacs-config.el")
;
; (if (file-exists-p my/device-specific-config)
;     (load my/device-specific-config)
;   ((defvar
;      my/config/device-is-main-pc
;      nil)))

(setq custom-file (concat config-file-folder "/customize.el"))
(load custom-file)

;; TODO move all binding functions to `shortcuts.el' and load this
;; file earlier.
(defmacro E-gmap! (chord function)
  "Add binding to global keymap"
  `(unless (interactive-form ,function)
     (error "Attemp to bind non-interactive function"))
  `(global-set-key (kbd ,chord) ,function))

(defmacro E-mmap! (mode chord function)
  "Add binding to specific mode map. `chord' will be expanded
by `(kbd)'"
  `(unless (interactive-form ,function)
     (error "Attemp to bind non-interactive function"))
  `(define-key ,mode (kbd ,chord) ,function))

(defmacro EX-map! (command function)
  "Add binding to evil command mode"
  `(unless (interactive-form ,function)
     (error "Attemp to bind non-interactive function"))
  `(evil-ex-define-cmd ,command ,function))

(defmacro SPC-map! (chord function &rest bindings)
  "Shorthand for spacemacs/set-leader-keys"
  `(unless (interactive-form ,function)
     (error "Attemp to bind non-interactive function"))
  `(spacemacs/set-leader-keys ,chord ,function ,@bindings))

(defmacro SPC-map-minor! (mode chord function &rest bindings)
  "Shorthand for spacemacs/set-leader-keys"
  `(unless (interactive-form ,function)
     (error "Attemp to bind non-interactive function"))
  (spacemacs/set-leader-keys ,mode ,chord ,function ,@bindings))


(defmacro SPC-map-major! (mode chord function &rest bindings)
  "Shorthand for spacemacs/set-leader-keys"
  `(unless (interactive-form ,function)
     (error "Attemp to bind non-interactive function"))
  `(spacemacs/set-leader-keys-for-major-mode ,mode ,chord ,function ,@bindings))

(defun dotspacemacs/layers ()
  (load-config "spacemacs-layers.el"))
(defun dotspacemacs/init ()
  (load-config "spacemacs-init.el"))
(defun dotspacemacs/user-init ()
  (load-config "user-init.el"))
(defun dotspacemacs/user-config ()
  (load-config "user-config.el"))
