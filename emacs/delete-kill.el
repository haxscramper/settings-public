;;;; Instead of pushing deleted text to the register/kill ring I want
;;;; to add it to custom stack of deleted text. TODO Create own state
;;;; for deleting text that will be triggered when I press `d` key

;;;#= Helper functions

(defvar internal/delete-queue '())

(defun internal/push-text-to-delete-ring (text)
  "Add text to delete ring. If text contains only whitespaces
Ignore it."
  (let ((clean-text (replace-regexp-in-string "\n" " " text)))
    (unless (string-match-p "^\\s-*$" clean-text)
      (push text internal/delete-queue))))

(defun internal/delete-line-range-from-current
    (line-range &optional reverse)
  "Delete current line and n-1 lines above or below.
If arg is negative delete lines upwards, if positive delete
downwards. If line-range is (equal) to nil delete only current
line. Using nil, 1, and -1 as argument is functionally the same."
  (if (equal arg nil)
      (setq arg 1))
  (unless (equal reverse nil) (setq arg (* -1 arg)))
  (let ((range-start (progn
                       (if (> arg 0)
                           (beginning-of-line)
                         (end-of-line))
                       (point)))
        (range-end (progn
                     (if (> arg 0)
                         (progn
                           (forward-line (- arg 1))
                           (end-of-line))
                       (progn
                         (forward-line (+ arg 1))
                         (beginning-of-line)))
                     (point))))
    (setq range-end (+ range-end 1))
    (if (< arg 0) (setq range-start (+ range-start 1)))
    (internal/push-text-to-delete-ring (buffer-substring range-start range-end))
    (delete-region range-start range-end)))

;;;#= Line deletion
;;;# Delete up

(defun delete-line-upwards-no-push (arg)
  "Delete lines upwards *including* current line."
  (interactive "P")
  (internal/delete-line-range-from-current arg t))

(defun delete-line-above-no-push (arg)
  "Delete lines above current line, *excluding* current line"
  (interactive "P")
  (forward-line -1)
  (internal/delete-line-range-from-current arg t))

;;;#== Delete down

(defun delete-line-downwards-no-push (arg)
  "Delete lines downwards, *including* current line."
  (interactive "P")
  (internal/delete-line-range-from-current arg))

(defun delete-line-below-no-push (arg)
  "Delete lines below current line, *excluding* current line"
  (interactive "P")
  (forward-line 1)
  (internal/delete-line-range-from-current arg)
  (forward-char -1))

;;;#= Word deletion
(defun backward-delete-word-no-push (arg)
  "Delete characters backward until encountering the beginning of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (delete-region (point)
                 (progn (forward-word -1)
                        (point))))


(defun forward-delete-word-no-push (arg)
  "Delete characters backward until encountering the beginning of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (delete-region (point)
                 (progn (forward-word)
                        (point))))

;;;#== Range deletion
(defun delete-range-to-symbol-including-no-push (arg)
  "Delete all symbols between current point position and first
occurrence of symbol"
  (interactive "P")
  (if  (equal arg nil)
       (setq arg 1))
  (let ((range-start (point))
        (range-end   (evil-find-char)))
    (unless (equal range-end range-start)
            (progn
              (internal/push-text-to-delete-ring
               (buffer-substring range-start range-end))))))

(map! :nvi "<C-backspace>" 'backward-delete-word-no-push)
(global-set-key (kbd "<C-backspace>") 'backward-delete-word-no-push)


;;;#= Bindings
; (map!
;  :n "d"  nil
;  :n "dw" 'forward-delete-word-no-push
;  :n "db" 'backward-delete-word-no-push
;  :n "dd" 'delete-line-downwards-no-push
;  :n "dk" 'delete-line-upwards-no-push
;  :n "dj" 'delete-line-downwards-no-push
;  :n "dJ" 'delete-line-below-no-push
;  :n "dK" 'delete-line-above-no-push
;  :n "df" 'delete-range-to-symbol-including-no-push)

;; TODO map dd to delete region to delete ring and dc to kill region
;; (map! :v "d" 'kill-region)

;;;#== Copy-pasted from
;;; https://github.com/gilbertw1/bmacs/blob/master/bmacs.org#operators

(defun delete-region-push-delete (beg end)
  (delete-region beg end))


(evil-define-operator evil-delete-char-without-register (beg end type reg)
  "delete character without yanking unless in visual mode"
  :motion evil-forward-char
  (interactive "<R><y>")
  (delete-region-push-delete beg end))

(evil-define-operator evil-delete-backward-char-without-register (beg end type _)
  "delete backward character without yanking"
  :motion evil-backward-char
  (interactive "<R><y>")
  (delete-region-push-delete beg end))

(evil-define-operator evil-delete-without-register (beg end type _ _2)
  (interactive "<R><y>")
  (delete-region-push-delete beg end))

(evil-define-operator evil-delete-without-register-if-whitespace (beg end type reg yank-handler)
  (interactive "<R><y>")
  (delete-region beg end))

(evil-define-operator evil-delete-line-without-register (beg end type _ yank-handler)
    (interactive "<R><y>")
    (delete-region-push-delete beg end))

;; TODO Alow push deleted text to delete stack
(evil-define-operator evil-change-without-register (beg end type _ yank-handler)
  (interactive "<R><y>")
  (evil-change beg end type ?_ yank-handler))

(evil-define-operator evil-change-line-without-register (beg end type _ yank-handler)
  "Change to end of line without yanking."
  :motion evil-end-of-line
  (interactive "<R><y>")
  (evil-change beg end type ?_ yank-handler #'evil-delete-line))

(evil-define-command evil-paste-after-without-register
  (count &optional register yank-handler)
  "evil paste before without yanking"
  :suppress-operator t
  (interactive "P<x>")
  (if (evil-visual-state-p)
      (evil-visual-paste-without-register count register)
      (evil-paste-after count register yank-handler)))

(evil-define-command evil-paste-before-without-register (count &optional register yank-handler)
  "evil paste before without yanking"
  :suppress-operator t
  (interactive "P<x>")
  (if (evil-visual-state-p)
      (evil-visual-paste-without-register count register)
      (evil-paste-before count register yank-handler)))

(evil-define-command evil-visual-paste-without-register (count &optional register)
  "Paste over Visual selection."
  :suppress-operator t
  (interactive "P<x>")
  ;; evil-visual-paste is typically called from evil-paste-before or
  ;; evil-paste-after, but we have to mark that the paste was from
  ;; visual state
  (setq this-command 'evil-visual-paste)
  (let* ((text (if register
                   (evil-get-register register)
                 (current-kill 0)))
         (yank-handler (car-safe (get-text-property
                                  0 'yank-handler text)))
         new-kill
         paste-eob)
    (evil-with-undo
      (let* ((kill-ring (list (current-kill 0)))
             (kill-ring-yank-pointer kill-ring))
        (when (evil-visual-state-p)
          (evil-visual-rotate 'upper-left)
          ;; if we replace the last buffer line that does not end in a
          ;; newline, we use `evil-paste-after' because `evil-delete'
          ;; will move point to the line above
          (when (and (= evil-visual-end (point-max))
                     (/= (char-before (point-max)) ?\n))
            (setq paste-eob t))
          (evil-delete-without-register evil-visual-beginning evil-visual-end
                       (evil-visual-type))
          (when (and (eq yank-handler #'evil-yank-line-handler)
                     (not (eq (evil-visual-type) 'line))
                     (not (= evil-visual-end (point-max))))
            (insert "\n"))
          (evil-normal-state)
          (setq new-kill (current-kill 0))
          (current-kill 1))
        (if paste-eob
            (evil-paste-after count register)
          (evil-paste-before count register)))
      (kill-new new-kill)
      ;; mark the last paste as visual-paste
      (setq evil-last-paste
            (list (nth 0 evil-last-paste)
                  (nth 1 evil-last-paste)
                  (nth 2 evil-last-paste)
                  (nth 3 evil-last-paste)
                  (nth 4 evil-last-paste)
                  t)))))

;;; https://github.com/gilbertw1/bmacs/blob/master/bmacs.org#evil-mc
(dolist
    (commands '( ;;---
                (evil-change-without-register . ((:default
                                                  . evil-mc-execute-default-evil-change)))
                (evil-change-line-without-register . ((:default
                                                       . evil-mc-execute-default-evil-change-line)))
                (evil-delete-without-register . ((:default .
                                                           evil-mc-execute-default-evil-delete)))
                (evil-delete-without-register-if-whitespace . ((:default .
                                                                         evil-mc-execute-default-evil-delete)))
                (evil-delete-char-without-register . ((:default
                                                       . evil-mc-execute-default-evil-delete)))
                (evil-delete-backward-char-without-register . ((:default
                                                                . evil-mc-execute-default-evil-delete)))
                (evil-delete-line-without-register . ((:default
                                                       . evil-mc-execute-default-evil-delete)))
                (evil-paste-after-without-register . ((:default
                                                       . evil-mc-execute-default-evil-paste)))
                (evil-paste-before-without-register . ((:default
                                                        . evil-mc-execute-default-evil-paste)))))
  (push commands evil-mc-custom-known-commands))

(map!
 :n  "c"  #'evil-change-without-register
 :n  "C"  #'evil-change-line-without-register
 :n  "p"  #'evil-paste-after-without-register
 :n  "P"  #'evil-paste-before-without-register
 :n  "x"  #'evil-delete-char-without-register
 :n  "X"  #'evil-delete-backward-char-without-register
 :n  "d"  #'evil-delete-without-register)

(map! :v "x" 'kill-region)
(map! :v "d" 'delete-region)
