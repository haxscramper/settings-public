;;;;#= This file is grouped based on colors

;;;#= Green

(defface hax/face::simple::regular-green
  '((t :foreground "green"))
  "Regular green face"
  :group 'hax/face::group-misc)

(defface extended-green
  `((t
     :background ,(rgb-to-hex (generate-transparent "magenta" 0.2))
     :underline t
     :extend t))
  "Regular green face"
  :group 'hax/face::group-misc)

(defface hax/face::simple::bold-green
  '((t :foreground "green" :weight bold))
  "Bold green face"
  :group 'hax/face::group-misc)

(defface hax/face::simple::bold-dark-green
  '((t :foreground "DarkGreen" :weight bold))
  "Bold DarkGreen face"
  :group 'hax/face::group-misc)

(defface hax/face::underlined::green
  '((t :underline (:color "green")))
  "Underline green face"
  :group 'hax/face::group-misc)

(defface hax/face::boxed::green-bold-boxed
  '((t :inherit hax/face::simple::bold-green
       :box "green"
       ))
  "Bold green face in orange box"
  :group 'hax/face::group-other)

;;;#= Red

(defface hax/face::simple::bold-red
  '((t :foreground "red" :weight bold))
  "Bold red face"
  :group 'hax/face::group-misc)

(defface hax/face::boxed::red-bold-boxed
  '((t :inherit hax/face::simple::bold-red
       :box "red"
       ))
  "Bold red face in orange box"
  :group 'hax/face::group-other)


(defface hax/face::nuclear
  '((t :foreground "red"
       :background "orange"
       :weight ultra-bold
       :slant italic
       :box (:line-width 2 :color "green" :style released-button)
       ))
  "Nuclear red face"
  :group 'hax/face::group-misc)


;;;#= Blue

(defface hax/face::simple::regular-blue
  '((t :foreground "blue"))
  "Regular blue face"
  :group 'hax/face::group-misc)


(defface hax/face::underlined::blue
  '((t :underline (:color "blue")))
  "Underline green face"
  :group 'hax/face::group-misc)

(defface hax/face::simple::bold-light-blue
  '((t :foreground "DeepSkyBlue" :weight bold))
  "Bold lightblue (DeepSkyBlue) face"
  :group 'hax/face::group-misc)

;;;#= Orange

(defface hax/face::simple::bold-orange
  '((t :foreground "orange" :weight bold))
  "Bold orange face"
  :group 'hax/face::group-misc)

(defface hax/face::underlined::orange
  '((t :underline (:color "orange")))
  "Underline orange face"
  :group 'hax/face::group-misc)

(defface hax/face::boxed::orange-bold-boxed
  '((t :inherit hax/face::simple::bold-orange
       :box "orange"
       ))
  "Bold orange face in orange box"
  :group 'hax/face::group-other
  )

(defface hax/face::boxed::dim-yellow-bold-boxed
  '((t :box "#b1951d" :foreground "#b1951d" :weight bold))
  "Dim yellow face in orange box"
  :group 'hax/face::group-other
  )


;;;#= White

(defface hax/face::simple::regular-white
  '((t :foreground "white"))
  "Regular white face"
  :group 'hax/face::group-misc)

(defface hax/face::simple::bold-white
  '((t :foreground "white" :weight bold))
  "Bold white face"
  :group 'hax/face::group-misc)

;;;#= Syntax elements

;;#= Code

(defface hax/face::highlight::code
  '((t :inherit hax/face::simple::bold-light-blue))
  "Code face"
  :group 'hax/face::group-highlight)

;;;#= Tag

(defface hax/face::highlight::tag
  '((t :inherit hax/face::simple::bold-orange))
  "Simple tag face"
  :group 'hax/face::group-highlight)

;;;#= Extra

(defface hax/face::boxed::mangenta-with-pink-box
  `((t :background ,(rgb-to-hex (generate-transparent "magenta" 0.3))))
  "Bold orange face in thin orange box. Has the same width as
regular text"
  :group 'hax/face::group-extra)

(defface hax/face::boxed::blue-fringes
  `((t
     :background ,(rgb-to-hex (generate-transparent "blue" 0.4))
     :box "blue"))
  "Transparent blue background, blue fringes"
  :group 'hax/face::group-extra)

;;;;;;;;;;;;;;;;;;;;  modifications of existing faces  ;;;;;;;;;;;;;;;;;;;;


;; (defface font-lock-doc-face
;;   '((t :inherit hax/face::simple::bold-orange))
;;   "Simple tag face"
;;   :group 'hax/face::group-highlight)
