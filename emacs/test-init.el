;;; test-init.el --- test init file -*- lexical-binding: t; -*-

(set-face-attribute 'default nil
                    :family "Chomsky"
                    :height 150
                    :weight 'normal
                    :width 'normal)
